/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectionPhoto;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

//SelectionGridAdapter - adapter for selecting with multi select from photos stored on the device
public class SelectionGridAdapter extends BaseAdapter {


    //int for creating items id
    private static final int HEX_ID_CONSTANT = 0x001100;

    //ArrayList for use as data model
    private final ArrayList<SelectionPhoto> mSelectionPhotos;

    //reference to context
    private final Context mContext;


    //public constructor setting context and data model
    public SelectionGridAdapter(Context _context, ArrayList<SelectionPhoto> _selectionPhotos) {

        //store reference to context and data model
        this.mContext = _context;
        this.mSelectionPhotos = _selectionPhotos;
    }


    //getCount - return the count of items
    @Override
    public int getCount() {

        //verify data model is available
        if (mSelectionPhotos != null) {

            //return the size of the data model
            return mSelectionPhotos.size();

            //if data model is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the current position
    @Override
    public SelectionPhoto getItem(int position) {

        //verify the position is within the data model bounds
        if (mSelectionPhotos != null && position >= 0 && position < mSelectionPhotos.size()) {

            //return the item at the current position
            return mSelectionPhotos.get(position);

            //if position is not within bounds
        } else {

            //return new SelectionPhoto
            return new SelectionPhoto();
        }
    }


    //getItemId - return the id for the current item
    @Override
    public long getItemId(int position) {

        //verify the position is within the bounds of the data model
        if (mSelectionPhotos != null && position >= 0 && position < mSelectionPhotos.size()) {

            //return sum of hex id and position
            return HEX_ID_CONSTANT + position;

            //if the position is not within the bounds
        } else {

            //return 0
            return 0;
        }
    }


    //getView - configure and return the view for the position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //reference variable for the view holder
        SelectionPhotoViewHolder selectionPhotoViewHolder;

        //check if the view is not available
        if(convertView == null) {

            //inflate view and set as convertView
            convertView = LayoutInflater.from(mContext).inflate(R.layout.selection_grid_image_item, parent, false);

            //create new view holder and set as tag for the convertView
            selectionPhotoViewHolder = new SelectionPhotoViewHolder(convertView);
            convertView.setTag(selectionPhotoViewHolder);

            //if convertView is available
        } else {

            //retrieve the view holder
            selectionPhotoViewHolder = (SelectionPhotoViewHolder) convertView.getTag();
        }

        //retrieve the SelectionPhoto at the current position
        SelectionPhoto photoAtPosition = getItem(position);

        //try
        try {

            //load the photo into the image view with glide
            Glide.with(mContext)
                    .load(photoAtPosition.getImageFile()).into(selectionPhotoViewHolder.mSelectionImageView);

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //check if the photo is selected
        if (photoAtPosition.getIsSelected()) {

            //set the overlay to visible
            selectionPhotoViewHolder.mIsSelectedOverlay.setVisibility(View.VISIBLE);

            //if not selected
        } else {

            //set the overlay to invisible
            selectionPhotoViewHolder.mIsSelectedOverlay.setVisibility(View.INVISIBLE);
        }

        //return the configured view
        return convertView;
    }


    //SelectionPhotoViewHolder - view holder for SelectionGridAdapter
    private static class SelectionPhotoViewHolder {

        //references to image view and overlay
        final SquareGridImageView mSelectionImageView;
        final FrameLayout mIsSelectedOverlay;

        //constructor
        SelectionPhotoViewHolder(View _view) {

            //retrieve and store references to image view and overlay
            this.mSelectionImageView = _view.findViewById(R.id.selection_image_view_grid_item);
            this.mIsSelectedOverlay = _view.findViewById(R.id.selection_grid_item_overlay);
        }
    }
}