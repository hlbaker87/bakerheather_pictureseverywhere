/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


import java.io.File;


//MapPhotoFile - class for creating objects to store the information of a photo with location data
public class MapPhotoFile {

    //member variables
    private final File mFile;
    private final double mLatitude;
    private final double mLongitude;

    //getters
    public File getFile() {
        return mFile;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }


    //public constructor storing file and location in member variables
    public MapPhotoFile(File _file, double _lat, double _lng) {

        //set file, lat and lng as member variable values
        this.mFile = _file;
        this.mLatitude = _lat;
        this.mLongitude = _lng;
    }
}
