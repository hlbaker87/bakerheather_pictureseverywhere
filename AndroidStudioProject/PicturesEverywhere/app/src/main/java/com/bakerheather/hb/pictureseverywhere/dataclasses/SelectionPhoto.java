/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


import java.io.File;
import java.io.Serializable;


//SelectionPhoto - class for creating objects to store the data of a photo for multiselection
public class SelectionPhoto implements Serializable {

    //member variables
    private final File mImageFile;
    private Boolean mIsSelected = false;


    //getters
    public Boolean getIsSelected() {
        return mIsSelected;
    }

    public File getImageFile() {
        return mImageFile;
    }


    //setter
    public void setIsSelected(Boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }


    //public constructor setting received file as value of member variable
    public SelectionPhoto(File _imageFile) {

        //store received file as value of member variable
        this.mImageFile = _imageFile;
    }

    //public constructor setting new file as value of member variable
    public SelectionPhoto() {

        //store new file as value of member variable
        this.mImageFile = new File("");
    }

}