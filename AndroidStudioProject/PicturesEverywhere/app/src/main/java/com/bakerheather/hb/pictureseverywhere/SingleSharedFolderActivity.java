/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */


package com.bakerheather.hb.pictureseverywhere;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.dataclasses.Contributor;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectRemovalPhoto;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.fragments.SingleSharedFolderFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.DatabasePhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.FolderPhotoListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedPhotoDownloadListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.MenuUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.PhotoUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.ProfileUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;


//SingleSharedFolderActivity - displays the photos of a single shared folder and provides options
// for user
public class SingleSharedFolderActivity extends AppCompatActivity implements DatabasePhotoClickedListener,
        FolderPhotoListener, SharedPhotoDownloadListener {

    //strings for extra identifiers
    public static final String EXTRA_IS_SHARED_PHOTO = "EXTRA_IS_SHARED_PHOTO";
    public static final String EXTRA_SELECTION_PHOTOS = "EXTRA_SELECTION_PHOTOS";

    //request code ints
    private static final int RC_ADD_PHOTOS = 0x01000;
    private static final int RC_REMOVE_PHOTOS = 0x0101;
    public static final int RC_FOLDER_DELETED = 0x000100;

    //reference to selected shared folder
    private SharedFolder mSharedFolder;

    //references to Firebase objects
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseStorage mStorage;

    //ArrayList for contributors
    private ArrayList<Contributor> mContributors = new ArrayList<>();

    //ArrayList for folder photo references
    private final ArrayList<StorageReference> mDatabasePhotos = new ArrayList<>();

    //boolean to determine if the user is subscribed
    private boolean mUserIsSubscribed = false;


    //onCreate - firebase references setup event listeners and UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_shared_folder);

        //retrieve references to database and storage
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();
        mStorage = FirebaseStorage.getInstance();

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set the orientation to portrait
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_single_shared_folder);
        setSupportActionBar(myToolbar);

        //retrieve the intent and verify available
        Intent intent = getIntent();

        if (intent != null) {

            //retrieve the extras ans verify available
            Bundle extras = intent.getExtras();

            if (extras != null) {

                //retrieve the selected shared folder and boolean for if the user is subscribed
                mSharedFolder = (SharedFolder) extras.getSerializable(SharedFoldersActivity.EXTRA_SHARED_FOLDER);
                mUserIsSubscribed = extras.getBoolean(MainActivity.EXTRA_USER_IS_SUBSCRIBED, false);

                //verify the folder is available
                if (mSharedFolder != null) {

                    //add fragment to view with fragment manager
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_single_shared_folder_fragment_container, SingleSharedFolderFragment.newInstance(mSharedFolder), SingleSharedFolderFragment.TAG)
                            .commit();

                    //verify database is available and retrieve reference
                    if (mDatabase != null) {

                        DatabaseReference databaseReference = mDatabase.getReference();

                        //verify reference is available
                        if (databaseReference != null) {

                            //create reference to the photos of the folder and set listener
                            DatabaseReference folderPhotosReference = databaseReference
                                    .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                    .child(mSharedFolder.getFolderIdentifier())
                                    .child(SharedFoldersActivity.KEY_PHOTOS);
                            folderPhotosReference.addChildEventListener(mPhotosChildEventListener);

                            //create reference to contributors of the folder and set listener
                            DatabaseReference folderContributorsReference = databaseReference
                                    .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                    .child(mSharedFolder.getFolderIdentifier())
                                    .child(SharedFoldersActivity.KEY_CONTRIBUTORS);
                            folderContributorsReference.addChildEventListener(mContributorChildEventListener);
                        }
                    }
                }
            }
        }
    }


    //onDestroy - remove database event listeners
    @Override
    protected void onDestroy() {
        super.onDestroy();

        //verify FirebaseDatabase has been referenced
        if (mDatabase != null) {

            //retrieve reference to database
            DatabaseReference databaseReference = mDatabase.getReference();

            //verify reference is available
            if (databaseReference != null) {

                //create reference for current folder photos
                DatabaseReference folderPhotosReference = databaseReference
                        .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                        .child(mSharedFolder.getFolderIdentifier())
                        .child(SharedFoldersActivity.KEY_PHOTOS);

                //remove the event listener
                folderPhotosReference.removeEventListener(mPhotosChildEventListener);

                //create reference for the contributors
                DatabaseReference folderContributorsReference = databaseReference
                        .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                        .child(mSharedFolder.getFolderIdentifier())
                        .child(SharedFoldersActivity.KEY_CONTRIBUTORS);

                //remove the event listener
                folderContributorsReference.removeEventListener(mContributorChildEventListener);
            }
        }
    }


    //onActivityResult - verify request code and result and reload folder data model
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check for add photos request code
        if (requestCode == RC_ADD_PHOTOS) {

            //verify result is ok
            if (resultCode == RESULT_OK) {

                //reload folder data model
                getFolderPhotos();
            }

            //check for remove photos request code
        } else if (requestCode == RC_REMOVE_PHOTOS) {

            //verify result is ok
            if (resultCode == RESULT_OK) {

                //reload folder data model
                getFolderPhotos();
            }
        }
    }


    //onCreateOptionsMenu - inflate menu from resources, load fragment, hide menu items if not owner
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_single_shared_folder_activity, menu);

        //verify shared folder has been referenced
        if (mSharedFolder != null) {

            //verify current user id is available and check if does not match owner
            if (mAuth != null && mAuth.getCurrentUser() != null && !mAuth.getCurrentUser().getUid().equals(mSharedFolder.getOwner())) {

                //hide menu items for folder owner only
                MenuUtils.hideMenuItem(menu, R.id.action_add_a_friend);
                MenuUtils.hideMenuItem(menu, R.id.action_remove_a_friend);
                MenuUtils.hideMenuItem(menu, R.id.action_remove_pictures);
            }
        }

        //return true
        return true;
    }


    //onOptionsItemSelected - verify the selected options item and take action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the id of the selected menu item
        switch (item.getItemId()) {

            //capture photo
            case R.id.action_capture_photo:

                //set the result to capture a new photo and finish the activity
                setResult(MainActivity.RESULT_CAPTURE_NEW_PHOTO);
                finish();

                break;

            //add pictures
            case R.id.action_add_pictures:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

                    //create intent for SelectPhotosActivity
                    Intent addPicturesIntent = new Intent(SingleSharedFolderActivity.this, SelectPhotosActivity.class);

                    //add shared folder as extra
                    addPicturesIntent.putExtra(SharedFoldersActivity.EXTRA_SHARED_FOLDER, mSharedFolder);

                    //start the activity
                    startActivityForResult(addPicturesIntent, RC_ADD_PHOTOS);

                    //if no internet
                } else {

                    //display toast to inform user internet is required to add
                    Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //remove pictures
            case R.id.action_remove_pictures:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

                    //verify photos are available to delete
                    if (mDatabasePhotos.size() != 0) {

                        //create intent for SelectRemovalPhotosActivity
                        Intent removePicturesIntent = new Intent(SingleSharedFolderActivity.this, SelectRemovalPhotosActivity.class);

                        //add extras for shared folder and SelectRemovalPhoto objects for all folder photos
                        removePicturesIntent.putExtra(SharedFoldersActivity.EXTRA_SHARED_FOLDER, mSharedFolder);
                        removePicturesIntent.putExtra(EXTRA_SELECTION_PHOTOS, getSelectRemovalPhotos());

                        //start the activity
                        startActivityForResult(removePicturesIntent, RC_REMOVE_PHOTOS);

                        //if folder is empty
                    } else {

                        //display toast to inform user there are no photos to remove
                        Toast.makeText(SingleSharedFolderActivity.this, R.string.no_photos_to_remove_shared_folder, Toast.LENGTH_SHORT).show();
                    }

                    //if no internet
                } else {

                    //display toast to inform user internet is required to remove
                    Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //add a friend
            case R.id.action_add_a_friend:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

                    //display alert to add a contributor
                    addAFriend();

                    //if no internet
                } else {

                    //display toast to inform user internet is required to add a friend
                    Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //remove a friend
            case R.id.action_remove_a_friend:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

                    //display alert to remove a contributor
                    deleteAFriend();

                    //if no internet
                } else {

                    //display toast to inform user internet is required to remove a friend
                    Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //delete folder
            case R.id.action_delete_folder:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

                    //display alert to delete the folder or current user as a contributor
                    deleteFolder();

                    //if no internet
                } else {

                    //display toast to inform user internet is required to delete a folder
                    Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;
        }

        //return super implementation
        return super.onOptionsItemSelected(item);
    }


    //deleteFolder - display alert for user to confirm deletion, upon delete, verify if user is
    // owner and delete folder or remove current user as a contributor if not owner
    private void deleteFolder() {

        //create alert builder for user to confirm folder delete
        AlertDialog.Builder deleteFolderAlertBuilder = new AlertDialog.Builder(SingleSharedFolderActivity.this);

        //set title and message and prevent outside click to cancel
        deleteFolderAlertBuilder.setTitle(R.string.delete_folder);
        deleteFolderAlertBuilder.setCancelable(false);
        deleteFolderAlertBuilder.setMessage(R.string.message_alert_delete_folder);

        //set positive to delete the folder
        deleteFolderAlertBuilder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //verify instances of database and storage for Firebase have been referenced
                if (mDatabase != null && mStorage != null) {

                    //retrieve references to database and storage
                    DatabaseReference databaseReference = mDatabase.getReference();
                    StorageReference storageReference = mStorage.getReference();

                    //verify database reference, Firebase auth, and current user are not null
                    if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                        //check if current user is owner of folder
                        if (mSharedFolder.getOwner().equals(mAuth.getCurrentUser().getUid())) {

                            //delete shared folder from database
                            databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                    .child(mSharedFolder.getFolderIdentifier()).removeValue();

                            //delete all shared folder photos from storage
                            storageReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                    .child(mSharedFolder.getFolderIdentifier()).delete();

                            //if current user is not the owner
                        } else {

                            //remove user from contributors list
                            final DatabaseReference contributorsReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                    .child(mSharedFolder.getFolderIdentifier())
                                    .child(SharedFoldersActivity.KEY_CONTRIBUTORS);

                            //create query to find current user in contributors list
                            Query query = contributorsReference.orderByValue().equalTo(mAuth.getCurrentUser().getUid());

                            //add new listener for single event
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    /*
                                    * Warning suppressed due to intentional and tested implementation.
                                    * */
                                    @SuppressWarnings("unchecked")
                                    HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                                    //verify HashMap is available
                                    if (valueMap != null) {

                                        //loop over the keys in the map
                                        for (String key : valueMap.keySet()) {

                                            //remove any values that match the UID of the current user
                                            contributorsReference.child(key).removeValue();
                                        }
                                    }
                                }

                                //required override
                                @Override
                                public void onCancelled(DatabaseError databaseError) {}
                            });
                        }

                        //display toast to inform user the folder was deleted
                        Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.folder_deleted), Toast.LENGTH_SHORT).show();

                        //finish the activity
                        finish();
                    }
                }
            }
        });

        //set negative button to cancel
        deleteFolderAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //display the alert
        deleteFolderAlertBuilder.create().show();
    }


    //deleteAFriend - retrieve the email addresses of the contributors and display alert for user to
    // select contributor to remove
    private void deleteAFriend() {

        //create new array list for contributors
        final ArrayList<Contributor> contributors = new ArrayList<>();

        //verify at least the owner and another contributor exist
        if (mSharedFolder.getContributorUserIds().size() > 1) {

            //verify database is available and get reference
            if (mDatabase != null) {

                DatabaseReference databaseReference = mDatabase.getReference();

                //verify reference is available
                if (databaseReference != null) {

                    //loop over the contributors ids
                    for (String uid : mSharedFolder.getContributorUserIds()) {

                        //create query for user with uid of loop
                        Query query = databaseReference.child(SignInActivity.KEY_USERS).orderByKey().equalTo(uid);

                        //add single event listener to query
                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                //verify snapshot is available
                                if (dataSnapshot != null) {

                                    /*
                                    * Warning suppressed due to intentional and tested implementation.
                                    * */
                                    @SuppressWarnings("unchecked")
                                    HashMap<String, String> contributorMap = (HashMap<String, String>) dataSnapshot.getValue();

                                    //verify the HashMap is available
                                    if (contributorMap != null) {

                                        //loop over the keys in the hash map
                                        for (String key : contributorMap.keySet()) {

                                            //verify the current user of the loop is not the current user (owner)
                                            if (mAuth != null && mAuth.getCurrentUser() != null && !key.equals(mAuth.getCurrentUser().getUid())) {

                                                //add the contributor to the list of contributors
                                                contributors.add(new Contributor(key, contributorMap.get(key)));
                                            }
                                        }

                                        //verify there is at least one contributor that is not the owner
                                        if (contributors.size() >= mSharedFolder.getContributorUserIds().size() - 1) {

                                            //display the list of contributors for removal selection
                                            displayContributors(contributors);
                                        }
                                    }
                                }
                            }

                            //required override
                            @Override
                            public void onCancelled(DatabaseError databaseError) {}
                        });
                    }
                }
            }

            //if there is not at least one non-owner contributor
        } else {

            //display a toast to inform user there are no contributors to remove
            Toast.makeText(SingleSharedFolderActivity.this, R.string.no_contributors_to_remove, Toast.LENGTH_SHORT).show();
        }
    }


    //displayContributors - display the contributors in alert allowing the owner to select for removal
    private void displayContributors(ArrayList<Contributor> _contributors) {

        //store the contributors
        mContributors = _contributors;

        //create ArrayList for emails
        final ArrayList<String> emails = new ArrayList<>();

        //loop over contributors
        for (Contributor contributor: _contributors) {

            //add email to ArrayList of emails
            emails.add(contributor.getEmail());
        }

        //create Alert builder  and set the title
        AlertDialog.Builder deleteAFriendAlertBuilder = new AlertDialog.Builder(SingleSharedFolderActivity.this);
        deleteAFriendAlertBuilder.setTitle(R.string.remove_contributor);

        //inflate and set the view with spinner
        /*
        * Suppressed due to intentional passing of null as root view
        * */
        @SuppressLint("InflateParams")
        View createSharedFolderLayout = SingleSharedFolderActivity.this.getLayoutInflater().inflate(R.layout.alert_remove_a_friend, null);
        deleteAFriendAlertBuilder.setView(createSharedFolderLayout);

        //retrieve the spinner from the view
        final Spinner removeSpinner = createSharedFolderLayout.findViewById(R.id.remove_a_friend_spinner);

        //create and set adapter for the spinner
        removeSpinner.setAdapter(new ArrayAdapter<>(SingleSharedFolderActivity.this, android.R.layout.simple_list_item_1, emails));

        //set negative button to cancel
        deleteAFriendAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //set positive button to remove the selected
        deleteAFriendAlertBuilder.setPositiveButton(R.string.remove, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //retrieve the selected position of the spinner
                int selected = removeSpinner.getSelectedItemPosition();

                //verify the database is available
                if (mDatabase != null) {

                    //retrieve a reference to the database and verify available
                    DatabaseReference databaseReference = mDatabase.getReference();

                    if (databaseReference != null) {

                        //create a reference to the contributors of the folder
                        final DatabaseReference contributorsReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                .child(mSharedFolder.getFolderIdentifier())
                                .child(SharedFoldersActivity.KEY_CONTRIBUTORS);

                        //set up query for the selected contributors id
                        Query query = contributorsReference.orderByValue().equalTo(mContributors.get(selected).getUID());

                        //add new single event listener for the query
                        query.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                /*
                                * Warning suppressed due to intentional and tested implementation.
                                * */
                                @SuppressWarnings("unchecked")
                                HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                                //verify the retrieved map is available
                                if (valueMap != null) {

                                    //loop over the contributors by key
                                    for (String key : valueMap.keySet()) {

                                        //remove the contributor
                                        contributorsReference.child(key).removeValue();
                                    }
                                }
                            }

                            //required override
                            @Override
                            public void onCancelled(DatabaseError databaseError) {}
                        });
                    }
                }

                //display toast to inform user the contributor has been removed
                Toast.makeText(SingleSharedFolderActivity.this, R.string.contributor_removed, Toast.LENGTH_SHORT).show();
            }
        });

        //display the alert
        deleteAFriendAlertBuilder.create().show();
    }


    //addAFriend - display alert, verify entered email is signed up with PicturesEverywhere and add as contributor for folder
    private void addAFriend() {

        //create alert builder to add a friend
        AlertDialog.Builder addAFriendAlertBuilder = new AlertDialog.Builder(SingleSharedFolderActivity.this);

        //set the alert title
        addAFriendAlertBuilder.setTitle(R.string.add_a_contributor);

        //inflate layout for email address entry and set as view for alert
        /*
        * Warning suppressed due to intentional and tested implementation.
        * */
        @SuppressLint("InflateParams")
        View createSharedFolderLayout = SingleSharedFolderActivity.this.getLayoutInflater().inflate(R.layout.alert_add_a_friend,null);
        addAFriendAlertBuilder.setView(createSharedFolderLayout);

        //retrieve input EditText from layout
        final EditText emailInput = createSharedFolderLayout.findViewById(R.id.add_a_friend_edit_text);

        //set negative button for cancel
        addAFriendAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //set positive button for add
        addAFriendAlertBuilder.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //check if no text or empty string was entered
                if (emailInput.getText().toString().trim().equals("")) {

                    //display toast to inform user an email must be entered
                    Toast.makeText(SingleSharedFolderActivity.this, R.string.enter_email_add_contributor, Toast.LENGTH_SHORT).show();

                    //check if invalid email was entered
                } else if (!ProfileUtils.isValidEmail(emailInput.getText().toString().trim())) {

                    //display toast to inform user the email entered was invalid
                    Toast.makeText(SingleSharedFolderActivity.this, R.string.invalid_email, Toast.LENGTH_SHORT).show();

                    //verify valid email was input
                } else if (ProfileUtils.isValidEmail(emailInput.getText().toString().trim())) {

                    //verify database instance and auth user is available
                    if (mDatabase != null && mAuth != null && mAuth.getCurrentUser() != null) {

                        //retrieve reference to database and verify exists
                        final DatabaseReference databaseReference = mDatabase.getReference();

                        if (databaseReference != null) {

                            //create reference to users in database
                            final DatabaseReference usersReference = databaseReference.child(SignInActivity.KEY_USERS);

                            //create query for user with entered email
                            Query query = usersReference.orderByValue().equalTo(emailInput.getText().toString().trim());

                            //add new event listener for single value
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    //verify snapshot was received
                                    if (dataSnapshot != null) {

                                        /*
                                        * Warning suppressed due to intentional and tested implementation.
                                        * */
                                        @SuppressWarnings("unchecked")
                                        HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                                        //verify hash map is available and is not empty
                                        if (valueMap != null && !valueMap.isEmpty()) {

                                            //loop over keys with values matching email entered
                                            for (String key : valueMap.keySet()) {

                                                //verify user did not enter self
                                                if (!key.equals(mAuth.getCurrentUser().getUid())) {

                                                    //add UID for email as contributor for folder
                                                    databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                                                            .child(mSharedFolder.getFolderIdentifier())
                                                            .child(SharedFoldersActivity.KEY_CONTRIBUTORS)
                                                            .push()
                                                            .setValue(key);

                                                    //display toast to inform user the contributor has been added
                                                    Toast.makeText(SingleSharedFolderActivity.this, R.string.contributor_added, Toast.LENGTH_SHORT).show();

                                                    //if user tried to add themselves
                                                } else {

                                                    //display toast to inform user no need to add self
                                                    Toast.makeText(SingleSharedFolderActivity.this, R.string.user_added_self_as_contributor, Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                            //if map is not available or map is empty
                                        } else {

                                            //display toast to inform user the entered email is not registered
                                            Toast.makeText(SingleSharedFolderActivity.this, emailInput.getText().toString().trim() + " " + getString(R.string.not_signed_up_suffix), Toast.LENGTH_SHORT).show();
                                        }

                                        //if no snapshot received
                                    } else {

                                        //display toast to inform user the entered email is not registered
                                        Toast.makeText(SingleSharedFolderActivity.this, emailInput.getText().toString().trim() + " " + getString(R.string.not_signed_up_suffix), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                //required override
                                @Override
                                public void onCancelled(DatabaseError databaseError) {}
                            });
                        }
                    }
                }
            }
        });

        //display alert
        addAFriendAlertBuilder.create().show();
    }


    //photoClicked - retrieve clicked photo, create temp file and set this as listener for finished
    // download and file creation
    @Override
    public void photoClicked(int _clickedIndex) {

        //check for internet
        if (InternetUtils.getIsNetworkAvailable(SingleSharedFolderActivity.this)) {

            //start download and temp file creation and pass activity as listener for completion to display
            // photo in single photo activity
            PhotoUtils.getTempFileForFolderPhoto(SingleSharedFolderActivity.this, mDatabasePhotos.get(_clickedIndex), this);

            //if no internet
        } else {

            //display toast to inform user internet is required for folder
            Toast.makeText(SingleSharedFolderActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
        }
   }


    //displaySharedPhoto - create new intent and start activity to display photo for temp file
    // received as parameter
    @Override
    public void displaySharedPhoto(File _tempPhotoFile) {

        //create a new intent for SinglePhotoActivity
        Intent singlePhotoIntent = new Intent(SingleSharedFolderActivity.this, SinglePhotoActivity.class);

        //add extras of temp file and boolean to inform activity of displaying photo from shared folder
        singlePhotoIntent.putExtra(MainActivity.EXTRA_PHOTO_FILE, _tempPhotoFile);
        singlePhotoIntent.putExtra(EXTRA_IS_SHARED_PHOTO, true);
        singlePhotoIntent.putExtra(MainActivity.EXTRA_USER_IS_SUBSCRIBED, mUserIsSubscribed);

        //start the photo activity
        startActivity(singlePhotoIntent);
    }


    //getFolderPhotos - display current data model in fragment
    @Override
    public void getFolderPhotos() {

        //retrieve the fragment
        SingleSharedFolderFragment singleSharedFolderFragment = (SingleSharedFolderFragment) getFragmentManager()
                .findFragmentByTag(SingleSharedFolderFragment.TAG);

        //verify the fragment is available
        if (singleSharedFolderFragment != null) {

            //pass current data model to fragment as parameter
            singleSharedFolderFragment.folderDataChanged(mDatabasePhotos);
        }
    }


    //getSelectRemovalPhotos - create and return ArrayList of serializable SelectRemovalPhotos allowing
    // user to multi-select the photos in the folder for deletion
    private ArrayList<SelectRemovalPhoto> getSelectRemovalPhotos() {

        //create new ArrayList
        ArrayList<SelectRemovalPhoto> selectionPhotos = new ArrayList<>();

        //loop over folder photo StorageReferences
        for (StorageReference storageReference: mDatabasePhotos) {

            //create new SelectRemovalPhoto object for current reference and add to ArrayList
            selectionPhotos.add(new SelectRemovalPhoto(storageReference.getPath()));
        }

        //return the populated ArrayList
        return selectionPhotos;
    }


    //mPhotosChildEventListener - ChildEventListener for firebase database to listen for changes in photos in the current folder
    private final ChildEventListener mPhotosChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            //verify the snapshot is available
            if (dataSnapshot != null) {

                //retrieve the path of the photo
                String path = (String) dataSnapshot.getValue();

                //verify the path is available
                if (path != null) {

                    //create storage path for the image
                    StorageReference storageReference = mStorage.getReference()
                            .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                            .child(mSharedFolder.getFolderIdentifier())
                            .child(path);

                    //add the reference to the array list of photos
                    mDatabasePhotos.add(storageReference);

                    //reload the photos in UI
                    getFolderPhotos();
                }
            }
        }

        //required override
        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            //verify snapshot is available
            if (dataSnapshot != null) {

                //retrieve the path of the photo and verify available
                String path = (String) dataSnapshot.getValue();

                if (path != null) {

                    //create storage reference for the photo
                    StorageReference storageReference = mStorage.getReference()
                            .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                            .child(mSharedFolder.getFolderIdentifier())
                            .child(path);

                    //remove the photo from the ArrayList
                    mDatabasePhotos.remove(storageReference);

                    //reload photos in UI
                    getFolderPhotos();
                }
            }
        }

        //required overrides
        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };


    //mContributorChildEventListener - ChildEventListener for firebase database to listen for changes in contributors in the current folder
    private final ChildEventListener mContributorChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            //verify snapshot is available
            if (dataSnapshot != null) {

                //retrieve the user id
                String uid = (String) dataSnapshot.getValue();

                //verify the user id is available
                if (uid != null) {

                    //add to the list of contributors
                    mSharedFolder.addContributorID(uid);
                }
            }
        }

        //required override
        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {

            //verify the snapshot is available
            if (dataSnapshot != null) {

                //retrieve the user id
                String uid = (String) dataSnapshot.getValue();

                //verify the id is available
                if (uid != null) {

                    //remove the user from the contributors list
                    mSharedFolder.removeContributorID(uid);
                }
            }
        }

        //required overrides
        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
}