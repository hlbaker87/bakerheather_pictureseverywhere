/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.SharedFoldersActivity;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarPhoto;
import com.bakerheather.hb.pictureseverywhere.dataclasses.MapPhotoFile;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.interfaces.CalendarMonthPhotosListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.DownloadBackupListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.ExportCompletedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.MapPhotosListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedPhotoDownloadListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedPhotosAddedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.StorageTotalListener;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.lang.GeoLocation;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.GpsDirectory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;


//PhotoUtils - utility class for use with photos throughout the app
public class PhotoUtils {

    //references to interfaces and context
    private static CalendarMonthPhotosListener mCalendarMonthListener;
    private static MapPhotosListener mMapPhotosListener;
    private static ExportCompletedListener mExportCompletedListener;
    private static Context mContext;

    //temporary file name
    private static final String TEMP_FILE_NAME = "displayFolderPhoto";
    private static final String TEMP_FILE_SUFFIX = ".jpeg";

    //keys for database
    private static final String KEY_USERS = "USERS";
    private static final String KEY_PHOTOS = "PHOTOS";
    private static final String KEY_TOTAL_STORAGE_BYTES = "TOTAL_STORAGE_BYTES";

    //ten gb bytes
    private static final long TEN_GB = 1073741824;

    //numbers to verify upload and download numbers
    private static int mNumDownloads = 0;
    private static int mNumUploads = 0;

    //data model for calendar photos
    private static HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> mAllCalendarPhotos = new HashMap<>();


    //getCalendarMonths - retrieves calendar data model in background task
    public static void getCalendarMonths(Context _context, CalendarMonthPhotosListener _calendarMonthListener) {

        //retrieve the application context
        mContext = _context.getApplicationContext();
        mCalendarMonthListener = _calendarMonthListener;

        //clear the calendar
        mAllCalendarPhotos.clear();

        //create task and execute to retrieve photos
        CalendarMonthsTask calendarMonthsTask = new CalendarMonthsTask();
        calendarMonthsTask.execute();
    }


    //CalendarMonthsTask - async task to retrieve calendar photos
    private static class CalendarMonthsTask extends AsyncTask<Void, Void, ArrayList<CalendarMonth>> {

        //doInBackground - retrieve photos in the background
        protected ArrayList<CalendarMonth> doInBackground(Void... _void) {

            //retrieve MediaStore images for calendar
            mAllCalendarPhotos = MediaStoreUtils.getCalendarPhotos(mContext);

            //create and return calendar gallery data model
            return MediaStoreUtils.getCalendarMonths(mAllCalendarPhotos);

        }

        //onPostExecute - update UI with data model
        protected void onPostExecute(ArrayList<CalendarMonth> result) {

            //verify listener and data model
            if (mCalendarMonthListener != null && result != null) {

                //store the data model and display the result
                mCalendarMonthListener.storeAllMonths(mAllCalendarPhotos);
                mCalendarMonthListener.displayCalendarMonths(result);
            }
        }
    }


    //getMapPhotos - retrieve map photos in background thread
    public static void getMapPhotos(Context _context, MapPhotosListener _mapPhotosListener, Cursor _photosCursor) {

        //retrieve application context and store reference to listener
        mContext = _context.getApplicationContext();
        mMapPhotosListener = _mapPhotosListener;

        //create new task and execute
        MapPhotosTask mapPhotosTask = new MapPhotosTask();
        mapPhotosTask.execute(_photosCursor);
    }


    //MapPhotosTask - async task to update the map with photos that have location data
    private static class MapPhotosTask extends AsyncTask<Cursor, MapPhotoFile, Boolean> {

        //doInBackground - find photos with location data in background thread
        protected Boolean doInBackground(Cursor... params) {

            //retrieve cursor
            Cursor photoCursor = params[0];

            //verify cursor is available
            if (photoCursor != null) {

                //move cursor to the first photo
                photoCursor.moveToFirst();

                //verify at least one photo is avaialble
                if (photoCursor.getCount() > 0) {

                    //retrieve the path
                    String imagePath = photoCursor.getString(photoCursor.getColumnIndex(MediaStore.Images.Media.DATA));

                    //retrieve file for the photo
                    File imageFile = new File(imagePath);

                    //try
                    try {

                        //retrieve photo meta data
                        Metadata metadata = ImageMetadataReader.readMetadata(imageFile);

                        //retrieve the gps directories
                        Collection<GpsDirectory> gpsDirectories = metadata.getDirectoriesOfType(GpsDirectory.class);

                        //loop over directories
                        for (GpsDirectory gpsDirectory : gpsDirectories) {

                            //retrieve the geo location
                            GeoLocation geoLocation = gpsDirectory.getGeoLocation();

                            //verify location is available
                            if (geoLocation != null && !geoLocation.isZero()) {

                                //create MapPhotoFile
                                MapPhotoFile mapPhotoFile = new MapPhotoFile(imageFile, geoLocation.getLatitude(), geoLocation.getLongitude());

                                //public progress with the map photo
                                publishProgress(mapPhotoFile);

                                //break
                                break;
                            }
                        }

                        //catch exceptions and print the stack trace
                    } catch (IOException | ImageProcessingException e) {
                        e.printStackTrace();
                    }
                }

                //loop over cursor items
                while (photoCursor.moveToNext()) {

                    //retrieve the path
                    String imagePath = photoCursor.getString(photoCursor.getColumnIndex(MediaStore.Images.Media.DATA));

                    //retrieve file for the photo
                    File imageFile = new File(imagePath);

                    //try
                    try {

                        //retrieve photo meta data
                        Metadata metadata = ImageMetadataReader.readMetadata(imageFile);

                        //retrieve the gps directories
                        Collection<GpsDirectory> gpsDirectories = metadata.getDirectoriesOfType(GpsDirectory.class);

                        //loop over directories
                        for (GpsDirectory gpsDirectory : gpsDirectories) {

                            //retrieve the geo location
                            GeoLocation geoLocation = gpsDirectory.getGeoLocation();

                            //verify location is available
                            if (geoLocation != null && !geoLocation.isZero()) {

                                //create MapPhotoFile
                                MapPhotoFile mapPhotoFile = new MapPhotoFile(imageFile, geoLocation.getLatitude(), geoLocation.getLongitude());

                                //public progress with the map photo
                                publishProgress(mapPhotoFile);

                                //break
                                break;
                            }
                        }

                        //catch exceptions and print the stack trace
                    } catch (IOException | ImageProcessingException e) {
                        e.printStackTrace();
                    }
                }
            }

            //return true
            return true;
        }

        //onProgressUpdate - add marker for the photo
        @Override
        protected void onProgressUpdate(MapPhotoFile... values) {
            super.onProgressUpdate(values);

            //verify listener is available
            if (mMapPhotosListener != null) {

                //add marker to the map
                mMapPhotosListener.addMapMarker(values[0]);
            }
        }

        //required override
        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
        }
    }


    //storeByteArrayForFolderPhoto - store byte array for photo in shared folder within database
    public static void storeByteArrayForFolderPhoto(final SharedFolder _sharedFolder, File _file, String _uid, final SharedPhotosAddedListener _photosAddedListener) {

        //retrieve the bitmap and verify available
        Bitmap bitmap = MediaStoreUtils.getPhotoBitmapFromFile(_file);

        //rotate the photo
        bitmap = MediaStoreUtils.rotatePhoto(bitmap, MediaStoreUtils.getRotateDegrees(_file));

        if (bitmap != null) {

            //create a new output stream for a byte array
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            //compress the bitmap into the byte array stream
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

            //create byte array
            final byte[] bytes = byteArrayOutputStream.toByteArray();

            //retrieve database and storage
            FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

            //verify database available
            if (firebaseDatabase != null) {

                //retrieve path for the photo storage
                String path = getFolderPhotoStorageKey(_uid);

                //create reference for the folders photo and set the value
                DatabaseReference folderPhotoReference = firebaseDatabase.getReference()
                        .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                        .child(_sharedFolder.getFolderIdentifier())
                        .child(SharedFoldersActivity.KEY_PHOTOS)
                        .push();
                folderPhotoReference.setValue(path);

                //create storage reference for the photo in storage
                StorageReference storageReference = firebaseStorage.getReference()
                        .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                        .child(_sharedFolder.getFolderIdentifier())
                        .child(path);

                //create task and upload the photo bytes
                UploadTask putBytesTask = storageReference.putBytes(bytes);
                putBytesTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                        //add the size of the file to the users
                        addSizeOfNewFileForUser(bytes.length, _sharedFolder.getOwner());

                        //update UI for upload
                        _photosAddedListener.photoAdded();
                    }
                });
            }
        }
    }


    //getFolderPhotoStorageKey - create unique firebase key for current time and user
    private static String getFolderPhotoStorageKey(String _userId) {

        //create date formatter
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy_MM_DD_hh_mm_ss_SSS", Locale.US);

        //return the string storage path
        return _userId + "_" + dateFormat.format(Calendar.getInstance().getTime());
    }


    //getBitmapFromByteArray - create a bitmap from the received byte array
    public static void getTempFileForFolderPhoto(final Context _context, StorageReference _photoStorageReference, final SharedPhotoDownloadListener _sharedPhotoDownloadListener) {

        //set the max image size as 3mb
        final long maxImageSize = (1024 * 1024) * 3;

        //add success and failure listeners and get bytes for photo
        _photoStorageReference.getBytes(maxImageSize).addOnSuccessListener(new OnSuccessListener<byte[]>() {

            //onSuccess - create bitmap and store in temporary file, display with interface
            @Override
            public void onSuccess(byte[] bytes) {

                //create bitmap of the photo
                Bitmap photo = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                //verify bitmap was created
                if (photo != null) {

                    //try
                    try {

                        //create temporary file for the photo
                        File tempFolderFile = File.createTempFile(TEMP_FILE_NAME, TEMP_FILE_SUFFIX, _context.getCacheDir());

                        //create file output stream for the temporary file
                        FileOutputStream outStream = new FileOutputStream(tempFolderFile);

                        //compress the photo into the output stream
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

                        //close the stream
                        outStream.close();

                        //display the photo with interface received as parameter
                        _sharedPhotoDownloadListener.displaySharedPhoto(tempFolderFile);

                        //catch IO exceptions and print the stack trace
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).addOnFailureListener(new OnFailureListener() {

            //onFailure - print failure stack trace and display toast to inform user
            @Override
            public void onFailure(@NonNull Exception exception) {

                //display toast to inform user there was an error retrieving the photo
                Toast.makeText(_context, R.string.error_downloading_shared_photo, Toast.LENGTH_SHORT).show();

                //print the onFailure stack trace
                exception.printStackTrace();
            }
        });
    }


    //getTempFileForBackupPhoto - create temporary file for
    private static void getTempFileForBackupPhoto(final Context _context, StorageReference _photoStorageReference, final DownloadBackupListener _downloadBackupListener, final String _name) {

        //set the max image size as 3mb
        final long maxImageSize = (1024 * 1024) * 3;

        //add success and failure listeners and get bytes for photo
        _photoStorageReference.getBytes(maxImageSize).addOnSuccessListener(new OnSuccessListener<byte[]>() {

            //onSuccess - create bitmap and store in temporary file, display with interface
            @Override
            public void onSuccess(byte[] bytes) {

                //create bitmap of the photo
                Bitmap photo = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

                //verify bitmap was created
                if (photo != null) {

                    //try
                    try {

                        //create temporary file for the photo
                        File tempFolderFile = File.createTempFile(_name, "", _context.getCacheDir());

                        //create file output stream for the temporary file
                        FileOutputStream outStream = new FileOutputStream(tempFolderFile);

                        //compress the photo into the output stream
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, outStream);

                        //close the stream
                        outStream.close();

                        //store the photo in the media store
                        MediaStoreUtils.storeMediaStorePhoto(_context, tempFolderFile, _name);

                        //call method to signal activity the download is complete
                        _downloadBackupListener.downloadComplete();

                        //catch IO exceptions and print the stack trace
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        }).addOnFailureListener(new OnFailureListener() {

            //onFailure - print failure stack trace and display toast to inform user
            @Override
            public void onFailure(@NonNull Exception exception) {

                //display toast to inform user there was an error retrieving the photo
                Toast.makeText(_context, R.string.error_downloading_shared_photo, Toast.LENGTH_SHORT).show();

                //print the onFailure stack trace
                exception.printStackTrace();
            }
        });
    }


    //uploadAllPhotosToFirebase - backup photos to database
    public static void uploadAllPhotosToFirebase(Cursor _cursor, DownloadBackupListener _downloadBackupListener) {

        //retrieve instance of auth and verify available
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        if (firebaseAuth != null) {

            //verify current user is available
            if (firebaseAuth.getCurrentUser() != null) {

                //move the cursor to the first position
                _cursor.moveToFirst();

                //store count of photos
                mNumUploads = _cursor.getCount();

                //do
                do {

                    //verify photos exist
                    if (mNumUploads > 0) {

                        //retrieve the path of the image
                        String imagePath = _cursor.getString(_cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                        //create file for the path
                        File imageFile = new File(imagePath);

                        //store the photo
                        storeByteArrayForUserPhoto(firebaseAuth.getCurrentUser().getUid(), imageFile, _downloadBackupListener);
                    }

                    //loop over photos
                } while (_cursor.moveToNext());
            }
        }
    }


    //storeByteArrayForUserPhoto - store photo in database for backup
    private static void storeByteArrayForUserPhoto(final String _uid, final File _file, final DownloadBackupListener _downloadBackupListener) {

        //retrieve bitmap
        Bitmap bitmap = MediaStoreUtils.getPhotoBitmapFromFile(_file);

        //rotate the bitmap
        bitmap = MediaStoreUtils.rotatePhoto(bitmap, MediaStoreUtils.getRotateDegrees(_file));

        //verify photo available
        if (bitmap != null) {

            //create a new output stream for a byte array
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            //compress the bitmap into the byte array stream
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

            //create byte array
            final byte[] bytes = byteArrayOutputStream.toByteArray();

            //retrieve database and storage
            final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
            FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

            //verify database is available
            if (firebaseDatabase != null) {

                //create reference for file
                final StorageReference storageReference = firebaseStorage.getReference()
                        .child(KEY_USERS)
                        .child(_uid)
                        .child(_file.getName());

                //check if photo exists
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {

                        //decrement uploads
                        mNumUploads = mNumUploads - 1;

                        //check if no uploads are left
                        if (mNumUploads == 0) {

                            //update ui
                            _downloadBackupListener.fullUploadComplete();
                        }

                        //if photo does not already exist in database
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {

                        //create reference for photo path in databse
                        DatabaseReference photoReference = firebaseDatabase.getReference().child(KEY_PHOTOS).child(KEY_USERS).child(_uid)
                                .child(KEY_PHOTOS).push();

                        //store the file name
                        photoReference.setValue(_file.getName());

                        //create task and upload the photo
                        UploadTask putBytesTask = storageReference.putBytes(bytes);
                        putBytesTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {

                                //decrement uploads
                                mNumUploads = mNumUploads - 1;

                                //check if no uploads are left
                                if (mNumUploads == 0) {

                                    //update ui
                                    _downloadBackupListener.fullUploadComplete();
                                }

                                //add the size of the file to user's storage total
                                addSizeOfNewFileForUser(bytes.length, _uid);
                            }
                        });
                    }
                });
            }
        }
    }


    //getAllPhotosFromFirebase - retrieve all user's photos
    public static void getAllPhotosFromFirebase(final Context _context, final DownloadBackupListener _downloadBackupListener) {

        //retrieve auth and database
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        //verify database, auth, and current user are available
        if (firebaseDatabase != null && firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

            //create reference for the user's photos
            DatabaseReference photoReference = firebaseDatabase.getReference()
                    .child(KEY_PHOTOS).child(KEY_USERS).child(firebaseAuth.getCurrentUser()
                            .getUid()).child(KEY_PHOTOS);

            //retrieve the uid of the user
            final String uid = firebaseAuth.getCurrentUser()
                    .getUid();

            //add new single event listener
            photoReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    //verify snapshot is available
                    if (dataSnapshot != null) {

                         /*
                        * Warning suppressed due to intentional and tested implementation.
                        * */
                        @SuppressWarnings("unchecked")
                        HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                        //verify map is available
                        if (valueMap != null) {

                            //store the size
                            mNumDownloads = valueMap.size();

                            //loop over map
                            for (String key : valueMap.keySet()) {

                                //boolean for if photo already exists
                                boolean photoNotExist = true;

                                //retrieve cursor and move to the first position
                                Cursor cursor = MediaStoreUtils.getMediaStoreCursor(_context);
                                cursor.moveToFirst();

                                //verify at least 1 photo exists
                                if (cursor.getCount() > 0) {

                                    //do
                                    do {

                                        //retrieve path and create file
                                        String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                                        File imageFile = new File(imagePath);

                                        //check if name is already there
                                        if (valueMap.get(key).equals(imageFile.getName())) {

                                            //set boolean
                                            photoNotExist = false;
                                        }

                                        //loop over cursor
                                    } while (cursor.moveToNext());

                                    //if no photo
                                    if (photoNotExist) {

                                        //create storage reference
                                        StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                                                .child(KEY_USERS)
                                                .child(uid)
                                                .child(valueMap.get(key));

                                        //retrieve temp file for photo
                                        getTempFileForBackupPhoto(_context, storageReference, _downloadBackupListener, valueMap.get(key));
                                    }

                                    //if no photos exist
                                } else {

                                    //create storage reference
                                    StorageReference storageReference = FirebaseStorage.getInstance().getReference()
                                            .child(KEY_USERS)
                                            .child(uid)
                                            .child(valueMap.get(key));

                                    //retrieve temp file for photo
                                    getTempFileForBackupPhoto(_context, storageReference, _downloadBackupListener, valueMap.get(key));
                                }

                                //decrement downloads
                                mNumDownloads = mNumDownloads - 1;

                                //if no downloads left
                                if (mNumDownloads == 0) {

                                    //update ui
                                    _downloadBackupListener.fullDownloadComplete();
                                }
                            }
                        }
                    }
                }

                //required override
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    }


    //deletePhotoFromBackup - delete photo from backup
    public static void deletePhotoFromBackup(final File _file) {

        //retrieve auth, database, and storage and verify available
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        final FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        if (firebaseDatabase != null && firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

            //create reference for database and get the user's id
            DatabaseReference photoReference = firebaseDatabase.getReference()
                    .child(KEY_PHOTOS).child(KEY_USERS).child(firebaseAuth.getCurrentUser()
                            .getUid()).child(KEY_PHOTOS);

            final String uid = firebaseAuth.getCurrentUser()
                    .getUid();

            //create and add single event listener
            photoReference.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    //verify snapshot is available
                    if (dataSnapshot != null) {

                        /*
                        * Warning suppressed for tested implementation.
                        * */
                        @SuppressWarnings("unchecked")
                        HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                        //verify map is available
                        if (valueMap != null) {

                            //loop over map
                            for (String key : valueMap.keySet()) {

                                //check for name match
                                if (valueMap.get(key).equals(_file.getName())) {

                                    //create database and storage reference
                                    final DatabaseReference photoReference = firebaseDatabase.getReference()
                                            .child(KEY_PHOTOS).child(KEY_USERS).child(uid).child(KEY_PHOTOS).child(key);

                                    final StorageReference storageReference = firebaseStorage.getReference()
                                            .child(KEY_USERS)
                                            .child(uid)
                                            .child(valueMap.get(key));

                                    //retrieve meta data
                                    storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
                                        @Override
                                        public void onSuccess(StorageMetadata storageMetadata) {

                                            //verify data is available
                                            if (storageMetadata != null) {

                                                //delete photo and path
                                                photoReference.removeValue();
                                                storageReference.delete();

                                                //update the users's used storage
                                                removeSizeOfFileForUser(storageMetadata.getSizeBytes(), firebaseAuth.getCurrentUser().getUid());
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }

                //required override
                @Override
                public void onCancelled(DatabaseError databaseError) {}
            });
        }
    }


    //exportPhotosToSD - export photos to sd card in backgroudn thread
    public static void exportPhotosToSD (Context _context, ExportCompletedListener _exportCompletedListener) {

        //store context and listener
        mContext = _context;
        mExportCompletedListener = _exportCompletedListener;

        //create task and execute
        ExportTask exportTask = new ExportTask();
        exportTask.execute();
    }


    //exportAllPhotos - export all photos to the sd card
    private static void exportAllPhotos (Context _context) {

        //verify storage permission is granted
        if (ActivityCompat.checkSelfPermission(_context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

            //retrieve the storage state
            String state = Environment.getExternalStorageState();

            //check for mounted state
            if (Environment.MEDIA_MOUNTED.equals(state)) {

                //create ArrayList of files
                ArrayList<File> files = MediaStoreUtils.getAllPhotos(_context);

                //try
                try {

                    //get directories
                    File[] externalStorageDirectory = _context.getExternalMediaDirs();

                    //loop over directory
                    for (File dirFile : externalStorageDirectory) {

                        //find SD card
                        if (!dirFile.getAbsolutePath().toLowerCase().contains("emulated")) {

                            //retrieve the parent file
                            File directory = dirFile.getParentFile();

                            //get the uri
                            Uri parentUri = MediaStoreUtils.getFileUri(directory, _context);

                            //grant permissions
                            _context.grantUriPermission(_context.getPackageName(), parentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            _context.getContentResolver().takePersistableUriPermission(parentUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                            //create file for directory
                            File picturesEverywhereDir = new File(directory.getAbsoluteFile(), _context.getPackageName());

                            //check if does not exists
                            if (!picturesEverywhereDir.exists()) {

                                //create the directory
                                boolean peDirectoryCreated = picturesEverywhereDir.mkdir();

                                //if dir was created
                                if (peDirectoryCreated) {

                                    //log folder created
                                    Log.wtf("Created", picturesEverywhereDir.getPath());
                                }
                            }

                            //get the uri
                            Uri uri = MediaStoreUtils.getFileUri(picturesEverywhereDir, _context);

                            //get permissions
                            _context.grantUriPermission(_context.getPackageName(), uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            _context.getContentResolver().takePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                            //verify directory can be read
                            boolean ok = picturesEverywhereDir.canRead();

                            //check if can read
                            if (ok) {

                                //loop over files
                                for (File file : files) {

                                    //get the bitmap and verify available
                                    Bitmap bitmap = MediaStoreUtils.getPhotoBitmapFromFile(file);

                                    if (bitmap != null) {

                                        //retrieve the file and check if does not exist
                                        File curFile = new File(picturesEverywhereDir, file.getName());

                                        if (!curFile.exists()) {

                                            //create the file
                                            boolean photoFileCreated = curFile.createNewFile();

                                            //check if created
                                            if (photoFileCreated) {

                                                //log file created
                                                Log.wtf("Created", curFile.getPath());
                                            }
                                        }

                                        //verify the file exists
                                        if (curFile.exists()) {

                                            //get the uri
                                            Uri curUri = MediaStoreUtils.getFileUri(curFile, _context);

                                            //get permissions
                                            _context.grantUriPermission(_context.getPackageName(), curUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                            _context.getContentResolver().takePersistableUriPermission(curUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                                            //verify the file can write
                                            if (curFile.canWrite()) {

                                                //create output stream
                                                FileOutputStream fileOutputStream = new FileOutputStream(curFile);

                                                //compress the photo into the output stream
                                                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

                                                //close the stream
                                                fileOutputStream.close();
                                            }
                                        }
                                    }
                                }
                            }

                            //break
                            break;
                        }
                    }

                    //catch any exceptions and print the stack trace
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //ExportTask - async task to export photos to the sd card
    private static class ExportTask extends AsyncTask<Void, Void, Void> {

        //doInBackground - export photos in the background
        protected Void doInBackground(Void... _void) {

            //verify context and listener available
            if (mContext != null && mExportCompletedListener != null) {

                //export the photos
                exportAllPhotos(mContext);
            }

            //return null
            return null;
        }

        //onPostExecute - clear resources and update UI
        protected void onPostExecute(Void result) {

            //verify listener is availabe
            if (mExportCompletedListener != null) {

                //notify the UI
                mExportCompletedListener.exportCompleted();

                //clear resources
                mExportCompletedListener = null;
                mContext = null;
            }
        }
    }


    //addSizeOfNewFileForUser - increment the user's storage total by the received amount
    private static void addSizeOfNewFileForUser(final long _numBytes, String _uid) {

        //get an instance of Firebase auth and database
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        //verify database is available
        if (firebaseDatabase != null) {

            //verify auth and current user are available
            if (firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

                //get reference and reference to user storage total
                DatabaseReference databaseReference = firebaseDatabase.getReference();
                final DatabaseReference userReference = databaseReference.child(KEY_TOTAL_STORAGE_BYTES).child(KEY_USERS).child(_uid).child(KEY_TOTAL_STORAGE_BYTES);

                //create and add single event listener
                userReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //verify snapshot is available
                        if (dataSnapshot != null) {

                            //get the sum of user's bytes used
                            Number totalBytesNum = (Number) dataSnapshot.getValue();

                            //check if number is available
                            if (totalBytesNum != null) {

                                //cast to long
                                long totalBytes = (long) totalBytesNum;

                                //increment the value in the database
                                userReference.setValue(totalBytes + _numBytes);

                                //if number not available
                            } else {

                                //create number
                                userReference.setValue(_numBytes);
                            }

                            //if no data
                        } else {

                            //create number
                            userReference.setValue(_numBytes);
                        }
                    }

                    //required override
                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        }
    }


    //removeSizeOfFileForUser - decrement user's stored data bytes in database
    private static void removeSizeOfFileForUser(final long _numBytes, String _uid) {

        //get instance of auth and database
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        //verify database available
        if (firebaseDatabase != null) {

            //verify auth and current user available
            if (firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

                //get database references
                DatabaseReference databaseReference = firebaseDatabase.getReference();
                final DatabaseReference userReference = databaseReference.child(KEY_TOTAL_STORAGE_BYTES).child(KEY_USERS).child(_uid).child(KEY_TOTAL_STORAGE_BYTES);

                //add single event listener
                userReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        //verify snapshot is available
                        if (dataSnapshot != null) {

                            //get the number of bytes
                            Number totalBytesNum = (Number) dataSnapshot.getValue();

                            //verify number is available
                            if (totalBytesNum != null) {

                                //cast as long
                                long totalBytes = (long) totalBytesNum;

                                //decrement the value
                                userReference.setValue(totalBytes - _numBytes);
                            }
                        }
                    }

                    //required override
                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });
            }
        }
    }


    //getSubscribersTotalBytes - retrieve used data for current user
    public static void getSubscribersTotalBytes(final StorageTotalListener _storageTotalListener, String _uid) {

        //get auth and database instances
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        //verify database available
        if (firebaseDatabase != null) {

            //get database references
            DatabaseReference databaseReference = firebaseDatabase.getReference();

            //verify ref is available
            if (databaseReference != null) {

                //verify auth and current user available
                if (firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

                    //get references for user data
                    DatabaseReference userReference = databaseReference.child(KEY_TOTAL_STORAGE_BYTES).child(KEY_USERS).child(_uid).child(KEY_TOTAL_STORAGE_BYTES);

                    //add new single event listener
                    final ValueEventListener sharedFoldersEventListener = new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            //verify snapshot is available
                            if (dataSnapshot != null) {

                                //get the number of bytes
                                Number totalBytesNum = (Number) dataSnapshot.getValue();

                                //verify number available
                                if (totalBytesNum != null) {

                                    //cast as long
                                    long totalBytes = (long) totalBytesNum;

                                    //check if storage over 10gb
                                    if (totalBytes > TEN_GB) {

                                        //update UI
                                        _storageTotalListener.storageTotaled(false);

                                        //if under 10gb
                                    } else {

                                        //update UI
                                        _storageTotalListener.storageTotaled(true);
                                    }

                                    //if number is null
                                } else {

                                    //update UI
                                    _storageTotalListener.storageTotaled(true);
                                }

                                //if snapshot not available
                            } else {

                                //update the UI
                                _storageTotalListener.storageTotaled(true);
                            }
                        }

                        //required override
                        @Override
                        public void onCancelled(DatabaseError databaseError) {}
                    };

                    //add the listener
                    userReference.addListenerForSingleValueEvent(sharedFoldersEventListener);
                }
            }
        }
    }
}