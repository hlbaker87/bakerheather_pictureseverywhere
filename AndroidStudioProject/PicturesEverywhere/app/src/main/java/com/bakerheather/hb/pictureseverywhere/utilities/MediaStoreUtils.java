/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarPhoto;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectionPhoto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;


//MediaStoreUtils - utility class for use with the media store and photos
public class MediaStoreUtils {

    //string for file provider authority
    private static final String FILE_PROVIDER_AUTHORITY = "com.bakerheather.hb.pictures_everywhere_authority_file_provider";

    //prefix and suffix for photo files
    private static final String TEMP_FILE_NAME = "effectphoto";
    private static final String TEMP_FILE_SUFFIX = ".jpeg";


    //getMediaStoreCursor - retrieves and returns a cursor with the photos from the device media store
    public static android.database.Cursor getMediaStoreCursor(Context _context) {

        //create a projection for MediaStore data
        String[] projection = {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA};

        //create string to order the results descending to show the newest images first
        String orderBy = MediaStore.Images.Media._ID + " DESC";

        //retrieve cursor of media store images
        Cursor cursor = _context.getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                orderBy);

        //verify cursor is available
        if (cursor != null) {

            //verify cursor contains at least one item
            if (cursor.getCount() > 0) {

                //move the cursor to the first position
                cursor.moveToFirst();

                //do
                do {

                    //retrieve the path of the first photo
                    String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

                    //retrieve file for the image
                    File file = new File(imagePath);

                    //check if data exists for the photo
                    if (file.length() <= 0) {

                        //delete the blank image from media store directory
                        deleteMediaStorePhoto(_context, file);
                    }

                    //continue while move to next
                } while (cursor.moveToNext());

                //close the cursor
                cursor.close();
            }
        }

        //retrieve and return the media store cursor
        return _context.getContentResolver().query( MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                orderBy);
    }


    //getLastPhotoCaptured - retrieves and returns a file for the most recent photo captured and
    // stored in the device media store
    public static File getLastPhotoCaptured(Cursor _cursor) {

        //move the cursor to the first position
        _cursor.moveToFirst();

        //retrieve the path of the first photo
        String imagePath = _cursor.getString(_cursor.getColumnIndex(MediaStore.Images.Media.DATA));

        //create and return a file for the most recent photo path
        return new File(imagePath);
    }


    //getEditingBitmapFromFile - returns sample size bitmap from the received file
    public static Bitmap getEditingBitmapFromFile(File _imageFile){

        //try
        try {

            //check if the file is available
            if (_imageFile.exists()) {

                //create new bitmap options and set the sample size to 2
                BitmapFactory.Options imageOptions = new BitmapFactory.Options();
                imageOptions.inSampleSize = 2;

                //return the sample size bitmap for the received file
                return BitmapFactory.decodeFile(_imageFile.getAbsolutePath(), imageOptions);
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return null
        return null;
    }


    //getPhotoBitmapFromFile - returns the bitmap stored at the received file
    public static Bitmap getPhotoBitmapFromFile(File _imageFile) {

        //try
        try {

            //verify the file exists
            if (_imageFile.exists()) {

                //return the bitmap stored at the file
                return BitmapFactory.decodeFile(_imageFile.getAbsolutePath());
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return null
        return null;
    }


    //getAllPhotosForMultiselect - returns an ArrayList of SelectionPhotos with all photos stored
    // on the device media store
    public static ArrayList<SelectionPhoto> getAllPhotosForMultiselect(Context _context) {

        //create a new array list to store the photos
        ArrayList<SelectionPhoto> selectionPhotos = new ArrayList<>();

        //retrieve a cursor of the media store photos
        android.database.Cursor photosCursor = getMediaStoreCursor(_context);

        //move the cursor to the first position
        photosCursor.moveToFirst();

        //verify the cursor contains at least one photo
        if (photosCursor.getCount() > 0) {

            //retrieve the first image path and create a file
            String imagePath = photosCursor.getString(photosCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File imageFile = new File(imagePath);

            //create a new SelectionPhoto for the file and add to the array list
            selectionPhotos.add(new SelectionPhoto(imageFile));
        }

        //loop over the cursor items
        while (photosCursor.moveToNext()) {

            //retrieve the image path and create a new file
            String imagePath = photosCursor.getString(photosCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File imageFile = new File(imagePath);

            //create a new SelectionPhoto for the file and add to the array list
            selectionPhotos.add(new SelectionPhoto(imageFile));
        }

        //return the ArrayList of SelectionPhotos
        return selectionPhotos;
    }


    //getAllPhotos - creates and returns an ArrayList of Files for all photos stored in the device
    // media store
    public static ArrayList<File> getAllPhotos(Context _context) {

        //create new array list of files and retrieve the media store cursor
        ArrayList<File> photos = new ArrayList<>();
        android.database.Cursor photosCursor = getMediaStoreCursor(_context);

        //move the cursor to the first position
        photosCursor.moveToFirst();

        //verify the cursor contains at least on photo
        if (photosCursor.getCount() > 0) {

            //retrieve the path, create a file, and add it to the array list
            String imagePath = photosCursor.getString(photosCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File imageFile = new File(imagePath);

            //verify file exists
            if (imageFile.exists()) {

                //add the file
                photos.add(imageFile);
            }
        }

        //loop over the cursor items
        while (photosCursor.moveToNext()) {

            //retrieve the path, create a file, and add it to the array list
            String imagePath = photosCursor.getString(photosCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File imageFile = new File(imagePath);
            photos.add(imageFile);
        }

        //return the array list of files
        return photos;
    }


    //getRotateDegrees - reads the exif data of a photo and returns the rotation degrees as an int
    public static int getRotateDegrees(File _file){

        //try
        try {

            //retrieve ExifInterface
            ExifInterface exif = new ExifInterface(_file.getAbsolutePath());

            //retrieve photo orientation
            int exifRotation =  exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);


            Log.wtf("ROTATION INT", String.valueOf(exifRotation));

            //switch on the orientation
            switch (exifRotation) {

                //270 degrees
                case ExifInterface.ORIENTATION_ROTATE_270:
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    //return 270
                    return 270;

                //180 degrees
                case ExifInterface.ORIENTATION_ROTATE_180:
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    //return 180
                    return 180;

                //90 degrees
                case ExifInterface.ORIENTATION_ROTATE_90:
                case ExifInterface.ORIENTATION_TRANSPOSE:

                    //return 90
                    return 90;

                    //default
                default:

                    //return 0
                    return 0;
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return 0
        return 0;
    }


    //rotatePhoto - rotate the photo for the degrees received
    public static Bitmap rotatePhoto(Bitmap _bitmap, int _rotationDegrees) {

        //create a new matrix and set the rotation
        Matrix matrix = new Matrix();
        matrix.postRotate(_rotationDegrees);

        //return bitmap rotated with the matrix
        return Bitmap.createBitmap(_bitmap, 0, 0, _bitmap.getWidth(), _bitmap.getHeight(), matrix, true);
    }


    //getCalendarPhotos - retrieve date data from all photos and categorize by date
    public static HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> getCalendarPhotos (Context _context) {

        //retrieve an instance of the calendar
        Calendar calendar = Calendar.getInstance();

        //create data model for photos
        HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> calendarPhotos = new HashMap<>();

        //retrieve all photo files
        ArrayList<File> photos = getAllPhotos(_context);

        //loop over photo files
        for (File file: photos) {

            //try
            try {

                //retrieve ExifInterface and date of photo
                ExifInterface exifData = new ExifInterface(file.getAbsolutePath());
                String dateString = exifData.getAttribute(ExifInterface.TAG_DATETIME);

                //verify the date is available
                if (dateString != null) {

                    //create formatter and format the date
                    DateFormat format = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss", Locale.ENGLISH);
                    Date date = format.parse(dateString);

                    //set the date of the calendar
                    calendar.setTime(date);

                    //create CalendarPhoto
                    CalendarPhoto calendarPhoto = new CalendarPhoto(file, date);

                    //retrieve year month and day
                    String year = String.valueOf(calendar.get(Calendar.YEAR));
                    String month = String.valueOf(calendar.get(Calendar.MONTH));
                    String day = String.valueOf(calendar.get(Calendar.DATE));

                    //retrieve the map for the year
                    HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>> yearHashMap = calendarPhotos.get(year);

                    //check if year is available
                    if (yearHashMap != null) {

                        //retrieve the map for motn
                        HashMap<String, ArrayList<CalendarPhoto>> monthHashMap = yearHashMap.get(month);

                        //verify month is available
                        if (monthHashMap != null) {

                            //retrieve the day map
                            ArrayList<CalendarPhoto> dateArray = monthHashMap.get(day);

                            //verify date map is available
                            if (dateArray != null) {

                                //add the photo to the data model
                                dateArray.add(calendarPhoto);

                                //if date map not available
                            } else {

                                //create new ArrayList for the date
                                ArrayList<CalendarPhoto> datePhotos = new ArrayList<>();

                                //add the photo and add to data model
                                datePhotos.add(calendarPhoto);
                                monthHashMap.put(day, datePhotos);
                            }

                            //if month map not available
                        } else {

                            //create map for the month and retrieve the date
                            HashMap<String, ArrayList<CalendarPhoto>> newMonthMap = new HashMap<>();
                            ArrayList<CalendarPhoto> datePhotos = new ArrayList<>();

                            //add the photo
                            datePhotos.add(calendarPhoto);

                            //add data to data model
                            newMonthMap.put(day, datePhotos);
                            yearHashMap.put(month, newMonthMap);
                        }

                        //if year not available
                    } else {

                        //create year, month, and date
                        HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>> newYearMap = new HashMap<>();
                        HashMap<String, ArrayList<CalendarPhoto>> newMonthMap = new HashMap<>();
                        ArrayList<CalendarPhoto> datePhotos = new ArrayList<>();

                        //add the photo
                        datePhotos.add(calendarPhoto);

                        //add to data model
                        newMonthMap.put(day, datePhotos);
                        newYearMap.put(month, newMonthMap);
                        calendarPhotos.put(year, newYearMap);
                    }
                }

                //catch exceptions and print the stack trace
            } catch (IOException | ParseException e) {
                e.printStackTrace();
            }
        }

        //return the data model
       return calendarPhotos;
    }


    //getCalendarMonths - create and return ordered ArrayList for months with photos
    public static ArrayList<CalendarMonth> getCalendarMonths (HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> _calendarPhotos) {

        //create new ArrayList for months
        ArrayList<CalendarMonth> calendarMonths = new ArrayList<>();

        //loop over they years
        for (String year: _calendarPhotos.keySet()) {

            //retrieve the year
            HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>> yearEntries = _calendarPhotos.get(year);

            //loop over the months
            for (String month: yearEntries.keySet()) {

                //get the month
                HashMap<String, ArrayList<CalendarPhoto>> monthEntries = yearEntries.get(month);

                //create array list for the dates
                ArrayList<Date> datesWithPhotos = new ArrayList<>();

                //loop over the days
                for (String day: monthEntries.keySet()) {

                    //get the day
                    ArrayList<CalendarPhoto> dayPhotos = monthEntries.get(day);

                    //check if date has been added
                    if (!datesWithPhotos.contains(dayPhotos.get(0).getDate())) {

                        //add to dates list
                        datesWithPhotos.add(dayPhotos.get(0).getDate());
                    }
                }

                //sort the dates
                Collections.sort(datesWithPhotos, new Comparator<Date>() {
                    @Override
                    public int compare(Date o1, Date o2) {
                        return o2.compareTo(o1);
                    }
                });

                //create new month and add to data model
                CalendarMonth newCalendarMonth = new CalendarMonth(datesWithPhotos);
                calendarMonths.add(newCalendarMonth);
            }
        }

        //sotr the months
        Collections.sort(calendarMonths, new Comparator<CalendarMonth>() {
            @Override
            public int compare(CalendarMonth o1, CalendarMonth o2) {
                return o2.getMonthDates().get(0).compareTo(o1.getMonthDates().get(0));
            }
        });

        //return data model
        return calendarMonths;
    }


    //getDateFolderPhotos - return list of files for a date
    public static ArrayList<File> getDateFolderPhotos(Date _date, HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> _calendarPhotos) {

        //retrieve an instance of the calendar and set the time
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(_date);

        //get the year, month, and date
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH));
        String day = String.valueOf(calendar.get(Calendar.DATE));

        //verify photo files are available
        if (_calendarPhotos != null) {

            //retrieve the year and verify available
            HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>> yearMap = _calendarPhotos.get(year);

            if (yearMap != null) {

                //retrieve the month and verify available
                HashMap<String, ArrayList<CalendarPhoto>> monthMap = yearMap.get(month);

                if (monthMap != null) {

                    //retrieve the day and verify available
                    ArrayList<CalendarPhoto> dayArray = monthMap.get(day);

                    if (dayArray != null) {

                        //retrieve the photos for the date
                        ArrayList<File> dateFiles = new ArrayList<>();

                        //loop over the photos
                        for (CalendarPhoto photo : dayArray) {

                            //add the file to the list
                            dateFiles.add(photo.getImageFile());
                        }

                        //return list of files
                        return dateFiles;
                    }
                }
            }
        }

        //return null
        return null;
    }


    //storeMediaStorePhoto - store a photo in the device media store
    public static void storeMediaStorePhoto(Context _context, File _tempFile, String _name) {

        //retrieve bitmap from file
        Bitmap bitmap = getPhotoBitmapFromFile(_tempFile);

        //try
        try {

            //verify the bitmap is available
            if (bitmap != null) {

                //retrieve instance of the calendar
                Calendar calendar = Calendar.getInstance();

                //create ContentValues and put date information
                ContentValues contentValues = new ContentValues();
                contentValues.put(MediaStore.Images.Media.DATE_ADDED, calendar.getTimeInMillis());
                contentValues.put(MediaStore.Images.Media.DATE_TAKEN, calendar.getTimeInMillis());

                //retrieve the name of the photo
                if (_name != null) {

                    //add to the content values
                    contentValues.put(MediaStore.Images.Media.DISPLAY_NAME, _name);
                }

                //retrieve ContentResolver
                ContentResolver contentResolver = _context.getContentResolver();

                //retrieve Uri for the file to insert
                Uri contentUri = contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);

                //verify Uri is available
                if (contentUri != null) {

                    //open an output stream
                    OutputStream outputStream = contentResolver.openOutputStream(contentUri);

                    //try
                    try {

                        //compress the bitmap with the output stream
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                        //finally
                    } finally {

                        //check if output stream is available
                        if (outputStream != null) {

                            //close the stream
                            outputStream.close();
                        }
                    }
                }
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //deleteMediaStorePhoto - delete the received photo file from the MediaStore
    public static void deleteMediaStorePhoto(Context _context, File _photoFile) {

        //retrieve content resolver
        ContentResolver contentResolver = _context.getContentResolver();

        //delete the photo from the MediaStore
        contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                MediaStore.MediaColumns.DATA + "='" + _photoFile.getPath() + "'", null);
    }


    //getFileForNewPhoto - create and return a new temporary file for camera to capture new photo
    public static File getFileForNewPhoto() {

        //try
        try {

            //create file for external directory
            File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DCIM), "Camera");

            //verify directory is available or create
            if (storageDir.exists() || storageDir.mkdir()) {

                //return the file
                return File.createTempFile(TEMP_FILE_NAME, TEMP_FILE_SUFFIX, storageDir);
            }

            //catch any IO exceptions and print the stack trace
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return null
        return null;
    }


    //saveExifData - save the current date and location data for photo as EXIF data
    public static void saveExifData(File _file, Location _curUserLocation, File _tempFile) {

        //try
        try {

            //create exif interface for temp file
            ExifInterface tempExif = new ExifInterface(_tempFile.getAbsolutePath());

            //create exif interface for the file
            ExifInterface exif = new ExifInterface(_file.getAbsolutePath());
            exif.saveAttributes();

            //set the orientation and save
            exif.setAttribute(android.support.media.ExifInterface.TAG_ORIENTATION, String.valueOf(tempExif.getAttributeInt(android.support.media.ExifInterface.TAG_ORIENTATION, 0)));
            exif.saveAttributes();

            //check if the user location is available
            if (_curUserLocation != null) {

                //convert the location and set exif attributes
                exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LATITUDE, convertToExifLocationString(_curUserLocation.getLatitude()));
                exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LONGITUDE, convertToExifLocationString(_curUserLocation.getLongitude()));

                //check if the longitude is greater than 0
                if (_curUserLocation.getLongitude() > 0) {

                    //set ref for east
                    exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LONGITUDE_REF, "E");

                    //if less than 0
                } else {

                    //set ref for west
                    exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
                }

                //check if the longitude is greater than 0
                if (_curUserLocation.getLatitude() > 0) {

                    //set ref for north
                    exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LATITUDE_REF, "N");

                    //if less than 0
                } else {

                    //set ref for south
                    exif.setAttribute(android.support.media.ExifInterface.TAG_GPS_LATITUDE_REF, "S");
                }

                //save the location attributes
                exif.saveAttributes();
            }

            //create date formatter for exif compatibility
            SimpleDateFormat exifFormat = new SimpleDateFormat("yyyy:MM:dd hh:mm:ss", Locale.ENGLISH);

            //set the date attribute for the current time
            exif.setAttribute(android.support.media.ExifInterface.TAG_DATETIME, exifFormat.format(new Date(System.currentTimeMillis())));

            //save the date attributes
            exif.saveAttributes();

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //convertToExifLocationString - convert a coordinate to acceptable EXIF format
    private static String convertToExifLocationString(double _coordinate){

        //get the absolute of the coordinate
        _coordinate = Math.abs(_coordinate);

        //retrieve the degrees
        final int degrees = (int)_coordinate;

        //multiply by 60 and remove the degrees
        _coordinate = _coordinate * 60;
        _coordinate = _coordinate - (degrees * 60.0d);

        //retrieve the minutes
        final int minutes = (int)_coordinate;

        //multiply by 60 and remove the minutes
        _coordinate = _coordinate * 60;
        _coordinate = _coordinate - (minutes * 60.0d);

        //retrieve the seconds
        final int seconds = (int)(_coordinate * 1000.0d);

        //return the concatenated location
        return degrees + "/1," + minutes + "/1," + seconds + "/1000,";
    }


    //getFileUri - return the Uri for received file
    public static Uri getFileUri(File _file, Context _context) {

        //verify file is available
        if (_file != null) {

            //verify file can be read
            if (_file.canRead()) {

                //return the Uri for the file
                return FileProvider.getUriForFile(_context, FILE_PROVIDER_AUTHORITY, _file);
            }
        }

        //return null
        return null;
    }


    //storePhotoToTempFile - return temporary file for photo
    public static File storePhotoToTempFile(Context _context, Bitmap _photo) {

        //try
        try {

            //create temporary file
            File tempEffectFile = File.createTempFile(TEMP_FILE_NAME, TEMP_FILE_SUFFIX, _context.getCacheDir());

            //create output stream and compress the photo
            FileOutputStream fileOutputStream = new FileOutputStream(tempEffectFile);
            _photo.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

            //close the stream
            fileOutputStream.close();

            //return the file
            return tempEffectFile;

            //catch any exceptions and print the stack trace
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return null
        return null;
    }
}