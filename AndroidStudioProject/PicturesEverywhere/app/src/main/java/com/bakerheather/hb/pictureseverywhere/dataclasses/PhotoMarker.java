/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;

import com.google.android.gms.maps.model.Marker;

import java.io.File;

//PhotoMarker - class for creating objects to store the data of a map marker created for a photo
public class PhotoMarker {

    //member variables
    private final Marker mMarker;
    private final File mPhotoFile;

    //getters
    public Marker getMarker() {
        return mMarker;
    }

    public File getPhotoFile() {
        return mPhotoFile;
    }


    //public constructor storing Marker and File in member variables
    public PhotoMarker(Marker _marker, File _photoFile) {

        //store Marker and File for photo in member variables
        this.mMarker = _marker;
        this.mPhotoFile = _photoFile;
    }
}
