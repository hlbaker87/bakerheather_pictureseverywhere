/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;

//DatabasePhotoGridAdapter - adapter for displaying photos stored in shared folders
public class DatabasePhotoGridAdapter extends BaseAdapter {


    //id for use in creating item ids
    private static final int HEX_ID_CONSTANT = 0x0010;

    //ArrayList of references for photos
    private final ArrayList<StorageReference> mDatabasePhotos;

    //reference to context
    private final Context mContext;


    //public constructor setting data model and context references
    public DatabasePhotoGridAdapter(Context _context, ArrayList<StorageReference> _databasePhotos) {

        //store context and data model references
        this.mContext = _context;
        this.mDatabasePhotos = _databasePhotos;
    }


    //getCount - return the count of items
    @Override
    public int getCount() {

        //verify data model is available
        if (mDatabasePhotos != null) {

            //return the size of the data model
            return mDatabasePhotos.size();

            //if data model is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the current position
    @Override
    public StorageReference getItem(int position) {

        //verify the position is within the bounds of the data model
        if (mDatabasePhotos != null && position >= 0 && position < mDatabasePhotos.size()) {

            //return the item at the position
            return mDatabasePhotos.get(position);

            //if position is not available
        } else {

            //return generic storage reference
            return FirebaseStorage.getInstance().getReference();
        }
    }


    //getItemId - return the id for the current item
    @Override
    public long getItemId(int position) {

        //verify the position is within the bounds of the data model
        if (mDatabasePhotos != null && position >= 0 && position < mDatabasePhotos.size()) {

            //return the id constant and position sum
            return HEX_ID_CONSTANT + position;

            //if position is not within bounds
        } else {

            //return 0
            return 0;
        }
    }


    //getView - configure and return the view for the current position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //reference for view holder
        DatabasePhotoViewHolder databasePhotoViewHolder;

        //check if convertView is not available
        if(convertView == null) {

            //inflate layout and store as convert view
            convertView = LayoutInflater.from(mContext).inflate(R.layout.gallery_grid_image_item, parent, false);

            //create and store reference to view holder
            databasePhotoViewHolder = new DatabasePhotoViewHolder(convertView);
            convertView.setTag(databasePhotoViewHolder);

            //if convertView is available
        } else {

            //retrieve the view holder stored as tag
           databasePhotoViewHolder = (DatabasePhotoViewHolder) convertView.getTag();
        }

        //store view holder as final
        final DatabasePhotoViewHolder photoHolder = databasePhotoViewHolder;

        //retrieve the photo storage reference for the position
        StorageReference photoAtPosition = getItem(position);

        //create on success listener for photo
        OnSuccessListener onSuccessListener = new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                //try
                try {

                    //load the photo into the image view with gilde
                    Glide.with(mContext).load(uri).into(photoHolder.mImageView);

                    //catch any exceptions and print the stack trace
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //get download url and set on success listener
        /*
        * Warning suppressed due to intentional and tested implementation.
        * */
        //noinspection unchecked
        photoAtPosition.getDownloadUrl().addOnSuccessListener(onSuccessListener);

        //return the configured view
        return convertView;
    }


    //DatabasePhotoViewHolder - view holder class
    private static class DatabasePhotoViewHolder {

        //reference for SquareGridImageView
        final SquareGridImageView mImageView;

        //constructor
        DatabasePhotoViewHolder(View _view) {

            //retrieve and store reference to the SquareGridImageView
            this.mImageView = _view.findViewById(R.id.image_view_grid_item);
        }
    }
}