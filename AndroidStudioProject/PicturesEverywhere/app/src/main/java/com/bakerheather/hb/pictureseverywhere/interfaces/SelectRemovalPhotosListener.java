/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectRemovalPhoto;

import java.util.ArrayList;


//SelectRemovalPhotosListener - interface for selecting photos for removal from shared folder
public interface SelectRemovalPhotosListener {

    //methods to cancel the selection and complete selection to remove SelectRemovalPhotos from the
    // folder
    void cancelClicked();
    void doneClicked(ArrayList<SelectRemovalPhoto> _selectedPhotos);

    /*
    * Suppressed due to clarity of code with inverted boolean
    * */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean getDoneClicked();
}
