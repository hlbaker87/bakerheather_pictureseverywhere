/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.DatabasePhotoGridAdapter;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.interfaces.DatabasePhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.FolderPhotoListener;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;


//SingleSharedFolderFragment - fragment to display the photos of a single selected shared folder
public class SingleSharedFolderFragment extends Fragment {

    //fragment tag
    public static final String TAG = "FRAGMENT_SINGLE_SHARED_FOLDER";

    //string for argument key
    private static final String ARG_SHARED_FOLDER = "ARG_SHARED_FOLDER";

    //reference to PhotoClickedListener
    private DatabasePhotoClickedListener mDatabasePhotoClickedListener;
    private FolderPhotoListener mFolderPhotoListener;

    //reference to adapter
    private DatabasePhotoGridAdapter mDatabasePhotoGridAdapter;

    //data model
    private final ArrayList<StorageReference> mDatabasePhotos = new ArrayList<>();


    //public constructor
    public SingleSharedFolderFragment() {}


    //newInstance - create and return new instance of fragment
    public static SingleSharedFolderFragment newInstance(SharedFolder _sharedFolder) {

        //create new bundle and add folder
        Bundle args = new Bundle();
        args.putSerializable(ARG_SHARED_FOLDER, _sharedFolder);

        //create new fragment and set arguments
        SingleSharedFolderFragment fragment = new SingleSharedFolderFragment();
        fragment.setArguments(args);

        //return the fragment
        return fragment;
    }


    //onAttach - verify context implements interfaces, cast and store reference
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify context implements interface
        if (context instanceof DatabasePhotoClickedListener) {

            //cast context and store reference in member variable
            mDatabasePhotoClickedListener = (DatabasePhotoClickedListener) context;
        }

        //verify context implements interface
        if (context instanceof FolderPhotoListener) {

            //cast context and store reference in member variable
            mFolderPhotoListener = (FolderPhotoListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_single_shared_folder, container, false);
    }


    //onActivityCreated - set up UI
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify the view is available
        if (getView() != null) {

            //get arguments and verify available
            Bundle args = getArguments();

            if (args != null) {

                //get the shared folder and verify available
                SharedFolder sharedFolder = (SharedFolder) args.getSerializable(ARG_SHARED_FOLDER);

                if (sharedFolder != null) {

                    //get title view and set text
                    TextView titleTextView = getView().findViewById(R.id.single_shared_folder_title_text_view);
                    titleTextView.setText(sharedFolder.getTitle());

                    //get grid view and set empty state
                    GridView gridView = getView().findViewById(R.id.single_shared_folder_grid_grid_view);
                    gridView.setEmptyView(getView().findViewById(R.id.fragment_single_shared_folder_empty_list_state));

                    //create and set adapter and set on click listener
                    mDatabasePhotoGridAdapter = new DatabasePhotoGridAdapter(getActivity(), mDatabasePhotos);
                    gridView.setAdapter(mDatabasePhotoGridAdapter);
                    gridView.setOnItemClickListener(mGridPhotoClickedListener);

                    //verify interface is available
                    if (mFolderPhotoListener != null) {

                        //get the photos
                        mFolderPhotoListener.getFolderPhotos();
                    }

                    //if shared folder is not available
                } else {

                    //finsih the activity
                    getActivity().finish();
                }
            }
        }
    }


    //mGridPhotoClickedListener - click listener for grid opens clicked photo
    private final GridView.OnItemClickListener mGridPhotoClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify the listener is available
            if (mDatabasePhotoClickedListener != null) {

                //send clicked position to activity
                mDatabasePhotoClickedListener.photoClicked(position);
            }
        }
    };


    //folderDataChanged - refresh data model with received data
    public void folderDataChanged(ArrayList<StorageReference> _folderPhotos) {

        //reset data model to received data
        mDatabasePhotos.clear();
        mDatabasePhotos.addAll(_folderPhotos);

        //verify adapter is available
        if (mDatabasePhotoGridAdapter != null) {

            //notify the adapter of change
            mDatabasePhotoGridAdapter.notifyDataSetChanged();
        }
    }
}