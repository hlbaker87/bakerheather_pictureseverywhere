/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//GalleryListListener - interface to display the selected gallery on tablet from ListView selection
public interface GalleryListListener {

    //method to display the gallery selected by index
    void displayGallery(int _galleryIndex);
}
