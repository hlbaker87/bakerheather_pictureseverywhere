/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;


//SquareFrameLayout - FrameLayout displaying with equal width and height
public class SquareFrameLayout extends FrameLayout {

    //constructors
    public SquareFrameLayout(Context context) {
        super(context);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /*
    * Warning suppressed due to intentional passing of width to height measure to create square view
    * */
    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //pass width as height and width to super to create square view
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}