/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.fragments.SelectPhotosFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.SelectPhotosListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedPhotosAddedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.StorageTotalListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.PhotoUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.io.File;
import java.util.ArrayList;


//SelectPhotosActivity - displays all photos on the device and allows for multi select to add to the
// selected shared folder
public class SelectPhotosActivity extends AppCompatActivity implements SelectPhotosListener,
        SharedPhotosAddedListener, StorageTotalListener {


    //reference for the current shared folder
    private SharedFolder mSelectedFolder;

    //references to Firebase objects
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    //ints for how many photos will be and have been added
    private int mNumPhotosAdded = 0;
    private int mNumPhotosToAdd = 0;

    //ArrayList to store the selected photos
    private ArrayList<File> mSelectedPhotos = new ArrayList<>();

    //boolean to determine if done has been clicked
    private boolean mDoneClicked = false;


    //onCreate - retrieve references and set up the fragment for UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photos);

        //retrieve references to database and storage
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set orientation to portrait
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve the intent and verify available
        Intent intent = getIntent();

        if (intent != null) {

            //retrieve the extras and verify available
            Bundle extras = intent.getExtras();

            if (extras != null) {

                //retrieve the current shared folder and verify available
                mSelectedFolder = (SharedFolder) extras.getSerializable(SharedFoldersActivity.EXTRA_SHARED_FOLDER);

                if (mSelectedFolder != null) {

                    //add fragment to view with fragment manager
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_select_photos_fragment_container, SelectPhotosFragment.newInstance(), SelectPhotosFragment.TAG)
                            .commit();
                }
            }
        }
    }


    //cancelClicked - finish the activity
    @Override
    public void cancelClicked() {

        //finish the activity
        finish();
    }


    //doneClicked - add the selected photos to the current shared folder
    @Override
    public void doneClicked(ArrayList<File> _selectedPhotos) {

        //check for first time done is clicked
        if (!mDoneClicked) {

            //set that done has been clicked
            mDoneClicked = true;

            //check for internet
            if (InternetUtils.getIsNetworkAvailable(SelectPhotosActivity.this)) {

                //verify photos were selected to be added
                if (_selectedPhotos.size() > 0) {

                    //store the number of selected photos
                    mNumPhotosToAdd = _selectedPhotos.size();

                    //store the selected photos
                    mSelectedPhotos = _selectedPhotos;

                    //retrieve the storage space currently used by the host
                    PhotoUtils.getSubscribersTotalBytes(this, mSelectedFolder.getOwner());
                }

                //if no internet
            } else {

                //display toast to inform user internet is required to remove photos
                Toast.makeText(SelectPhotosActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
            }
        }
    }


    //photoAdded - called when a photo has been added to the database, notify user and finish activity
    // when all photos have been added
    @Override
    public void photoAdded() {

        //increment the number of photos that have been added
        mNumPhotosAdded += 1;

        //check if all photos have been added
        if (mNumPhotosToAdd <= mNumPhotosAdded) {

            //reset ints
            mNumPhotosToAdd = 0;
            mNumPhotosAdded = 0;

            //display toast to inform user the photos have been added
            Toast.makeText(SelectPhotosActivity.this, R.string.photos_added, Toast.LENGTH_SHORT).show();

            //hide the progress bar
            hideProgressBar();

            //set the result as ok and finish the activity
            setResult(RESULT_OK);
            finish();
        }
    }


    //getDoneClicked - returns if the user has yet clicked the done button
    @Override
    public boolean getDoneClicked() {

        //return boolean of if done has been clicked
        return mDoneClicked;
    }


    //showProgressBar - show and animate the progress bar
    private void showProgressBar() {

        //retrieve the fragment and verify available
        SelectPhotosFragment selectPhotosFragment = (SelectPhotosFragment) getFragmentManager().findFragmentByTag(SelectPhotosFragment.TAG);

        if (selectPhotosFragment != null) {

            //show and animate the progress bar
            selectPhotosFragment.showProgressBar();
        }
    }


    //hideProgressBar - hide the progress bar
    private void hideProgressBar() {

        //retrieve the fragment and verify available
        SelectPhotosFragment selectPhotosFragment = (SelectPhotosFragment) getFragmentManager().findFragmentByTag(SelectPhotosFragment.TAG);

        if (selectPhotosFragment != null) {

            //hide the progress bar
            selectPhotosFragment.hideProgressBar();
        }
    }


    //storageTotaled - called when the total storage of the user has been totaled and adds the
    // selected photos if space is available
    @Override
    public void storageTotaled(boolean _spaceIsAvailable) {

        //check if space is available
        if (_spaceIsAvailable) {

            //verify auth and database are available
            if (mAuth != null && mDatabase != null) {

                //retrieve current user and verify available
                FirebaseUser user = mAuth.getCurrentUser();

                if (user != null) {

                    //loop over the selected photos
                    for (File file: mSelectedPhotos) {

                        //store the photo
                        PhotoUtils.storeByteArrayForFolderPhoto(mSelectedFolder, file, user.getUid(), this);

                        //show the progress bar
                        showProgressBar();
                    }
                }
            }

            //if no space is available
        } else {

            //display toast to inform user there is not space to add photos
            Toast.makeText(SelectPhotosActivity.this, R.string.no_space_for_folder, Toast.LENGTH_SHORT).show();
        }
    }
}