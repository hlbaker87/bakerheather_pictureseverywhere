/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.bakerheather.hb.pictureseverywhere.SharedFoldersActivity;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

//SharedFolderService - service to notify user of new shared folder access
public class SharedFolderService extends Service {

    //string for broadcast action
    public static final String ACTION_NEW_FOLDER_ACCESS = "com.bakerheather.hb.pictureseverywhere.newsharedfolderaccessbroadcast";

    //references to firebase auth and database
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    //ArrayList to store
    private final ArrayList<SharedFolder> mSharedFolders = new ArrayList<>();


    //SharedFolderServiceBinder - binder for the service
    public class SharedFolderServiceBinder extends Binder {

        //getService - returns the service
        @SuppressWarnings("unused")
        public SharedFolderService getService(){

            //return the service
            return SharedFolderService.this;
        }
    }


    //onBind -
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        //retrieve instances of Firebase auth and database
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();

        //verify auth, database, and current user are available
        if (mAuth != null && mDatabase != null && mAuth.getCurrentUser() != null) {

            //retrieve reference to the database
            DatabaseReference databaseReference = mDatabase.getReference();

            //verify reference is available
            if (databaseReference != null) {

                //create reference to shared folders in the database and add listener for single event
                DatabaseReference sharedFoldersReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS);
                sharedFoldersReference.addListenerForSingleValueEvent(mSharedFoldersEventListener);
            }
        }

        //return the binder for the service
        return new SharedFolderServiceBinder();
    }


    //onDestroy - remove the database listener for shared folders
    @Override
    public void onDestroy() {
        super.onDestroy();

        //verify auth and database are available
        if (mAuth != null && mDatabase != null) {

            //retrieve reference to the database
            DatabaseReference databaseReference = mDatabase.getReference();

            //verify reference is available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference for shared folders and remove the child event listener
                DatabaseReference sharedFoldersReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS);
                sharedFoldersReference.removeEventListener(mSharedFoldersChildEventListener);
            }
        }
    }


    //mSharedFoldersEventListener - listener for shared folders from firebase database
    private final ValueEventListener mSharedFoldersEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            //verify snapshot is available
            if (dataSnapshot != null) {

                //clear the data model
                mSharedFolders.clear();

                //verify auth and current user are available
                if ( mAuth != null && mAuth.getCurrentUser() != null) {

                    //verify children of snapshot exist
                    if (dataSnapshot.getChildren() != null) {

                        //loop over the children of the snapshot
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                            //retrieve the folder id for the current index of loop
                            String folderId = snapshot.getKey();

                            //retrieve the folder map for the current folder
                            /*
                            * Warning suppressed due to intentional and tested implementation.
                            * */
                            @SuppressWarnings("unchecked")
                            HashMap<String, Object> folderMap = (HashMap<String, Object>) snapshot.getValue();

                            //verify the folder map is available
                            if (folderMap != null) {

                                //retrieve the owner and title of the filder
                                String owner = (String) folderMap.get(SharedFoldersActivity.KEY_OWNER);
                                String title = (String) folderMap.get(SharedFoldersActivity.KEY_TITLE);

                                //retrieve HashMap of all the contributors
                                /*
                                * Warning suppressed due to intentional and tested implementation.
                                * */
                                @SuppressWarnings("unchecked")
                                HashMap<String, String> contributorMap = (HashMap<String, String>) folderMap.get(SharedFoldersActivity.KEY_CONTRIBUTORS);

                                //create boolean to determine if the current user is authorized
                                boolean userIsContributor = false;

                                //verify the contributor map is available
                                if (contributorMap != null) {

                                    //create ArrayList to store all contributors
                                    ArrayList<String> contributors = new ArrayList<>();

                                    //loop over the contributors by key
                                    for (String key : contributorMap.keySet()) {

                                        //add the current contributor to the ArrayList
                                        contributors.add(contributorMap.get(key));

                                        //check if the current contributor is the current user
                                        if (contributorMap.get(key).equals(mAuth.getCurrentUser().getUid())) {

                                            //set boolean to true - current user is authorized
                                            userIsContributor = true;
                                        }
                                    }

                                    //check if the current user is authorized
                                    if (userIsContributor) {

                                        //retrieve a hash map of all the photo paths for the folder
                                        /*
                                        * Warning suppressed due to intentional and tested implementation.
                                        * */
                                        @SuppressWarnings("unchecked")
                                        HashMap<String, String> photoPathMap = (HashMap<String, String>) folderMap.get(SharedFoldersActivity.KEY_PHOTOS);

                                        //create new ArrayList to store the photo paths
                                        ArrayList<String> photoPaths = new ArrayList<>();

                                        //verify the map of paths is available
                                        if (photoPathMap != null) {

                                            //loop over the photo paths by key
                                            for (String photoKey : photoPathMap.keySet()) {

                                                //add the current path to the ArrayList
                                                photoPaths.add(photoPathMap.get(photoKey));
                                            }
                                        }

                                        //create a new shared folder for the current data
                                        SharedFolder sharedFolder = new SharedFolder(folderId, owner, title, photoPaths);

                                        //add the contributors to the shared folder
                                        for (String contributor: contributors) {

                                            sharedFolder.addContributorID(contributor);
                                        }

                                        //add the shared folder to member array list
                                        mSharedFolders.add(sharedFolder);
                                    }
                                }
                            }
                        }

                        //create reference to the database
                        DatabaseReference databaseReference = mDatabase.getReference();

                        //verify reference, auth and current user is available
                        if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                            //create reference to shared folders in database and add child listener for new children
                            DatabaseReference sharedFoldersReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS);
                            sharedFoldersReference.addChildEventListener(mSharedFoldersChildEventListener);
                        }
                    }
                }
            }
        }

        //required override
        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };


    //mSharedFoldersChildEventListener - child event listener for shared folders to check if user has
    // been added as a contributor to a folder
    private final ChildEventListener mSharedFoldersChildEventListener = new ChildEventListener() {

        //required override
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {}

        //onChildChanged - called when child is changed allowing check if user has been added as a
        // contributor
        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            //verify snapshot is available
            if (dataSnapshot != null) {

                //verify auth and current user are available
                if ( mAuth != null && mAuth.getCurrentUser() != null) {

                    //retrieve the folder id
                    String folderId = dataSnapshot.getKey();

                    //retrieve the HashMap of the folder
                    /*
                    * Warning suppressed due to intentional and tested implementation.
                    * */
                    @SuppressWarnings("unchecked")
                    HashMap<String, Object> folderMap = (HashMap<String, Object>) dataSnapshot.getValue();

                    //verify the map is available
                    if (folderMap != null) {

                        //retrieve the owner and title
                        String owner = (String) folderMap.get(SharedFoldersActivity.KEY_OWNER);
                        String title = (String) folderMap.get(SharedFoldersActivity.KEY_TITLE);

                        //retrieve the HashMap of contributors
                        /*
                        * Warning suppressed due to intentional and tested implementation.
                        * */
                        @SuppressWarnings("unchecked")
                        HashMap<String, String> contributorMap = (HashMap<String, String>) folderMap.get(SharedFoldersActivity.KEY_CONTRIBUTORS);

                        //create boolean to determine if the current user is a contributor
                        boolean userIsContributor = false;

                        //verify the contributor map is available
                        if (contributorMap != null) {

                            //create ArrayList for the contributors
                            ArrayList<String> contributors = new ArrayList<>();

                            //loop over the contributors by key
                            for (String key: contributorMap.keySet()) {

                                //add the contributor at the current key to the ArrayList
                                contributors.add(contributorMap.get(key));

                                //check if the current user is the contributor at the current loop
                                if (contributorMap.get(key).equals(mAuth.getCurrentUser().getUid())) {

                                    //set boolean to true
                                    userIsContributor = true;
                                }
                            }

                            //check if current user is a contributor
                            if (userIsContributor) {

                                //create a new shared folder
                                SharedFolder sharedFolder = new SharedFolder(folderId, owner, title, new ArrayList<String>());

                                //loop over contributors
                                for (String contributor: contributors) {

                                    //add contributor to the shared folder
                                    sharedFolder.addContributorID(contributor);
                                }

                                //create boolean to determine if this is a new folder
                                boolean isNewFolder = true;

                                //loop over shared folders
                                for (SharedFolder folder: mSharedFolders) {

                                    //check if ids match
                                    if (folder.getFolderIdentifier().equals(sharedFolder.getFolderIdentifier())) {

                                        //set boolean to false
                                        isNewFolder = false;
                                    }
                                }

                                //check if folder is new shared folder
                                if (isNewFolder) {

                                    //add folder to shared folders array list
                                    mSharedFolders.add(sharedFolder);

                                    //create new intent for shared folder broadcast
                                    Intent broadcastIntent = new Intent(ACTION_NEW_FOLDER_ACCESS);

                                    //add title and owner as extras
                                    broadcastIntent.putExtra(SharedFoldersActivity.EXTRA_SHARED_FOLDER_TITLE, sharedFolder.getTitle());
                                    broadcastIntent.putExtra(SharedFoldersActivity.EXTRA_SHARED_FOLDER_OWNER, sharedFolder.getOwner());

                                    //verify the current user is not the owner
                                    if (!mAuth.getCurrentUser().getUid().equals(owner)) {

                                        //send the broadcast
                                        sendBroadcast(broadcastIntent);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //required overrides
        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {}

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
}