/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.vending.billing.IInAppBillingService;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarPhoto;
import com.bakerheather.hb.pictureseverywhere.dataclasses.MapPhotoFile;
import com.bakerheather.hb.pictureseverywhere.fragments.GalleryListFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.DownloadBackupListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.ExportCompletedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.MapPhotosListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.StorageTotalListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.UserLocationListener;
import com.bakerheather.hb.pictureseverywhere.services.SharedFolderService;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.MediaStoreUtils;
import com.bakerheather.hb.pictureseverywhere.fragments.GalleryCalendarFragment;
import com.bakerheather.hb.pictureseverywhere.fragments.GalleryGridFragment;
import com.bakerheather.hb.pictureseverywhere.fragments.GalleryMapFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.CalendarDateListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.CalendarMonthPhotosListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.GalleryListListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.MapListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoCursorListener;
import com.bakerheather.hb.pictureseverywhere.utilities.MenuUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.PhotoUtils;
import com.facebook.login.LoginManager;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


//MainActivity - displays tabs and fragments for grid gallery, map gallery, and calendar gallery,
// handles main application navigation and updates
public class MainActivity extends AppCompatActivity implements PhotoClickedListener,
        CalendarDateListener, PhotoCursorListener, MapListener, GalleryListListener,
        CalendarMonthPhotosListener, MapPhotosListener, LocationListener, UserLocationListener, ServiceConnection,
        DownloadBackupListener, PurchasesUpdatedListener, ExportCompletedListener,
        StorageTotalListener {


    //intent filters
    public static final String MEDIA_STORE_DATA_CHANGED = "com.bakerheather.hb.pictureseverywhere.PHOTO_ADDED";
    private static final String OK_GOOGLE_CAPTURE_PHOTO = "android.media.action.IMAGE_CAPTURE";
    private static final String OK_GOOGLE_CAPTURE_STILL = "android.media.action.STILL_IMAGE_CAMERA";
    private static final String ACTION_OK_CAPTURE_PHOTO = "com.bakerheather.hb.pictureseverywhere.opened_by_ok_google";

    //request and result codes
    private static final int RC_STORAGE_PERMISSION = 0x0010;
    private static final int RC_CAPTURE_PHOTO = 0x00001;
    private static final int RC_LOCATION = 0x000101;
    public static final int RC_SIGN_IN = 0x0001;
    private static final int RC_SIGN_OUT = 0x0101;
    private static final int RC_SEARCH_MAP = 0x1101;
    private static final int RC_OPEN_SHARED_FOLDERS = 0x000111;
    public static final int RESULT_CAPTURE_NEW_PHOTO = 0x0001110;
    public static final int RESULT_SIGNED_IN = 0x00010;
    public static final int RESULT_SIGNED_OUT = 0x00100;
    public static final int RESULT_SIGN_IN = 0x0011100;
    public static int RC_BUY_SUBSCRIPTION = 0x0001010;

    //sku for month subscription
    public static final String SUBSCRIPTION_SKU = "rax89c2m480s56_sub_1_month";

    //extra keys
    public static final String EXTRA_PHOTO_FILE = "EXTRA_PHOTO_FILE";
    public static final String EXTRA_CALENDAR_DATE_FOLDER = "EXTRA_CALENDAR_DATE_FOLDER";
    public static final String EXTRA_DATE = "EXTRA_DATE";
    public static final String EXTRA_SIGNED_IN = "EXTRA_SIGNED_IN";
    public static final String EXTRA_SIGNED_OUT = "EXTRA_SIGNED_OUT";
    public static final String EXTRA_USER_IS_SUBSCRIBED = "EXTRA_USER_IS_SUBSCRIBED";

    //reference to Toolbar Menu
    private Menu mToolbarMenu;

    //reference to TabLayout
    private TabLayout mTabLayout;

    //int to identify the selected fragment
    private int mSelectedFragment = 0;

    //data models for UI
    private android.database.Cursor mPhotoCursor;
    private HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> mCalendarPhotos;

    //variable to store file of newly captured photo
    private File mNewPhotoFile;

    //reference storage for Firebase auth
    private FirebaseAuth mAuth;

    //login manager reference for Facebook
    private LoginManager mLoginManager;

    //variable to store reference to loading progress bar1
    private ProgressBar mProgressBar;

    //boolean to store if the data set has been changed and the UI needs to be reloaded
    private boolean mDataSetChanged = false;

    //variables to store references for the user location
    private Location mCurrentLocation;

    //boolean to determine if service is bound
    private boolean mIsBound = false;

    //variables for billing with GooglePlay
    private IInAppBillingService mBillingService;
    private BillingClient mBillingClient;
    private boolean mBillingConnected = false;

    //boolean to determine if the user is subscribed
    private boolean mUserSubscribed = false;

    //boolean to determine if user wants to open shared folders
    private boolean mOpenSharedFolders = false;

    //listener for download
    private static DownloadBackupListener mDownloadBackupListener;

    //boolean for map search
    private static boolean mMapIsSearched = false;


    //onCreate - set up UI and retrieve required references
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create billing client and start connection
        mBillingClient = BillingClient.newBuilder(MainActivity.this).setListener(this).build();
        mBillingClient.startConnection(mBillingClientStateListener);

        //retrieve instances of FirebaseAuth and Facebook LoginManager
        mAuth = FirebaseAuth.getInstance();
        mLoginManager = LoginManager.getInstance();

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_gallery_main);
        setSupportActionBar(myToolbar);

        //retrieve tab layout from resources and set gravity to fill
        mTabLayout = findViewById(R.id.tab_layout);
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //set tab selected listener
        mTabLayout.addOnTabSelectedListener(mTabSelectedListener);

        //retrieve the current device size to configure for phone/tablet interfaces
        String deviceSize = getString(R.string.layout_size);

        //check if external storage permission is not granted
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            //request permissions for external storage and location
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, RC_STORAGE_PERMISSION);

            //if storage permission is granted
        } else {

            //verify permission for location
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //retrieve the user's current location
                getUserLocation();
            }

            //retrieve cursor from the media store
            mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);

            //if the device is large
            if (deviceSize.equals("large")) {

                //add the list fragment to the view
                getFragmentManager().beginTransaction().add(R.id.gallery_list_frame_layout_fragment_container, GalleryListFragment.newInstance(), GalleryListFragment.TAG).commit();
            }

            //show the currently selected gallery view
            showSelectedFragment(mSelectedFragment);
        }

        //check if the current device is larger
        if (deviceSize.equals("large")) {

            //add the list fragment to the view
            getFragmentManager().beginTransaction().add(R.id.gallery_list_frame_layout_fragment_container, GalleryListFragment.newInstance(), GalleryListFragment.TAG).commit();
        }

        //check if the current device is small
        if (deviceSize.equals("small")) {

            //set the orientation of the screen to portrait only
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //register the broadcast receivers
        registerReceiver(mPhotoAddedReceiver, new IntentFilter(MainActivity.MEDIA_STORE_DATA_CHANGED));
        registerReceiver(mNewSharedFolderReceiver, new IntentFilter(SharedFolderService.ACTION_NEW_FOLDER_ACCESS));

        //retrieve the progress bar and store reference
        mProgressBar = findViewById(R.id.activity_main_progress_bar);

        //retrieve the starting intent
        Intent intent = getIntent();

        //verify intent is available and get the action
        if (intent != null) {

            String filter = intent.getAction();

            //verify action is available and check for capture photo
            if (filter != null && (filter.equals(OK_GOOGLE_CAPTURE_PHOTO) || filter.equals(OK_GOOGLE_CAPTURE_STILL))) {

                //create a new intent for a new task to capture a photo and start the activity
                Intent voiceIntent = new Intent(ACTION_OK_CAPTURE_PHOTO);
                voiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(voiceIntent);

                //set the result as ok and finish the activity
                setResult(RESULT_OK);
                finish();

                //if action is to capture a photo
            } else if (filter != null && filter.equals(ACTION_OK_CAPTURE_PHOTO)) {

                //start photo capture process
                capturePhoto();
            }
        }
    }


    //onStart - bind the shared folder service
    @Override
    protected void onStart() {
        super.onStart();

        //create a new intent for the shared folder service
        Intent sharedFolderServiceIntent = new Intent(this, SharedFolderService.class);

        //bind the service using the created intent
        bindService(sharedFolderServiceIntent, this, BIND_AUTO_CREATE);

    }


    //onDestroy - unregister the data model broadcast receiver
    @Override
    protected void onDestroy() {
        super.onDestroy();

        //unregister the receivers
        unregisterReceiver(mPhotoAddedReceiver);
        unregisterReceiver(mNewSharedFolderReceiver);

        //check if the service is bound
        if (mIsBound) {

            //set boolean to false and unbind
            mIsBound = false;
            unbindService(this);
        }

        //check if the billing service is available
        if (mBillingService != null) {

            //unbind the service and remove the service
            unbindService(mBillingServiceConnection);
            mBillingService = null;
        }
    }


    //onResume - check if the data set has changed and reload gallery if necessary and verify subscription
    @Override
    protected void onResume() {
        super.onResume();

        //check if the billing client is available
        if (mBillingClient != null) {

            //if the client is not connected
            if (!mBillingConnected) {

                //start the connection
                mBillingClient.startConnection(mBillingClientStateListener);

                //if connected
            } else {

                //query the user's purchase history
                mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                        //set user subscribed to false
                        mUserSubscribed = false;

                        //verify the purchase list is available
                        if (purchasesList != null) {

                            //loop over the purchase list
                            for (Purchase purchase : purchasesList) {

                                //check if the SKU is for a month subscription
                                if (purchase.getSku().equals(SUBSCRIPTION_SKU)) {

                                    //set boolean for if the user is subscribed
                                    mUserSubscribed = true;

                                    //verify toolbar menu is available
                                    if (mToolbarMenu != null) {

                                        //hide the subscribe menu item
                                        MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_subscribe);
                                    }
                                }
                            }
                        }

                        //check if the user is not subscribed
                        if (!mUserSubscribed) {

                            //verify toolbar menu is available
                            if (mToolbarMenu != null) {

                                //show the subscribe menu item
                                MenuUtils.showMenuItem(mToolbarMenu, R.id.action_subscribe);
                            }
                        }
                    }
                });
            }
        }

        //check if the data set has changed
        if (mDataSetChanged) {

            //reset boolean to false
            mDataSetChanged = false;

            //reload the selected gallery
            showSelectedFragment(mSelectedFragment);
        }

        //verify auth and the menu are available
        if (mAuth != null && mToolbarMenu != null) {

            //check if a user is not signed in
            if (mAuth.getCurrentUser() == null) {

                //hide and show appropriate menu items
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_profile);
                MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_in);
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
            }
        }

        //try
        try {

            //retrieve cursor of MediaStore photos
            Cursor cursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);

            //check if cursor has changed
            if (cursor != mPhotoCursor) {

                //store new cursor
                mPhotoCursor = cursor;

                //verify not resume from map search
                if (!mMapIsSearched) {

                    //set boolean
                    mMapIsSearched = false;

                    //reload the selected fragment
                    showSelectedFragment(mSelectedFragment);
                }
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //mBillingClientStateListener - state listener for billing client
    private final BillingClientStateListener mBillingClientStateListener = new BillingClientStateListener() {

        //onBillingSetupFinished - called when billing setup is finished - gets the purchase history
        @Override
        public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
            if (billingResponseCode == BillingClient.BillingResponse.OK) {

                //set billing boolean to true
                mBillingConnected = true;

                //verify the client is available
                if (mBillingClient != null) {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserSubscribed = true;

                                        //verify toolbar menu is available
                                        if (mToolbarMenu != null) {

                                            //hide the subscribe menu item
                                            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_subscribe);
                                        }
                                    }
                                }
                            }

                            //check if the user is not subscribed
                            if (!mUserSubscribed) {

                                //verify toolbar menu is available
                                if (mToolbarMenu != null) {

                                    //show the subscribe menu item
                                    MenuUtils.showMenuItem(mToolbarMenu, R.id.action_subscribe);
                                }
                            }
                        }
                    });
                }
            }
        }

        //onBillingServiceDisconnected - called when billing is disconnected
        @Override
        public void onBillingServiceDisconnected() {

            //set connected boolean to false
            mBillingConnected = false;
        }
    };


    //onServiceConnected - called when the service is connected
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        //set bound boolean to true
        mIsBound = true;
    }


    //onServiceDisconnected - called when the service is disconnected
    @Override
    public void onServiceDisconnected(ComponentName componentName) {

        //set bound boolean to false
        mIsBound = false;
    }


    //mBillingServiceConnection - service connection for in app subscriptions
    private final ServiceConnection mBillingServiceConnection = new ServiceConnection() {

        //onServiceDisconnected - called when the service is disconnected
        @Override
        public void onServiceDisconnected(ComponentName name) {

            //set billing service to null
            mBillingService = null;
        }


        //onServiceConnected - called when the service is connected
        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {

            //store the billing service
            mBillingService = IInAppBillingService.Stub.asInterface(service);
        }
    };


    //required override
    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {}


    //mNewSharedFolderReceiver - broadcast receiver for if user is given access to a new shared folder
    private final BroadcastReceiver mNewSharedFolderReceiver = new BroadcastReceiver() {

        //onReceive -
        @Override
        public void onReceive(Context context, Intent intent) {

            //verify the intent is available
            if (intent != null) {

                //retrieve the extras from the intent and verify available
                Bundle extras = intent.getExtras();

                if (extras != null) {

                    //retrieve the name of the folder from the extras
                    String name = extras.getString(SharedFoldersActivity.EXTRA_SHARED_FOLDER_TITLE, "");

                    //verify the name is not an empty string
                    if (!name.equals("")) {

                        //create a snackbar to inform the user they have received access to a new folder
                        Snackbar snackbar = Snackbar.make(findViewById(R.id.main_activity_root_layout),
                                getString(R.string.granted_access_prefix) + " " + name, Snackbar.LENGTH_INDEFINITE);

                        //set the snackbar action to view the shared folders
                        snackbar.setAction(R.string.view, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                //create a new intent to start the shared folders activity and start the activity for result
                                Intent sharedIntent = new Intent(MainActivity.this, SharedFoldersActivity.class);
                                sharedIntent.putExtra(EXTRA_USER_IS_SUBSCRIBED, mUserSubscribed);
                                startActivityForResult(sharedIntent, RC_CAPTURE_PHOTO);

                            }
                        });

                        //show the snack bar
                        snackbar.show();
                    }
                }
            }
        }
    };


    //mPhotoAddedReceiver - verify external permission and reload the data model, set boolean to
    // reload gallery when fragment is brought to forefront
    private final BroadcastReceiver mPhotoAddedReceiver = new BroadcastReceiver() {

        //onReceive - verify permissions and reload data
        @Override
        public void onReceive(Context context, Intent intent) {

            //verify storage permission
            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                //reload photo data from media store
                mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);

                //set boolean for data model change to true
                mDataSetChanged = true;
            }
        }
    };


    //onNewIntent - receives OK Google capture photo intent when app is open and starts activity to
    // capture a new photo
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        //verify intent is available
        if (intent != null) {

            //retrieve the action
            String filter = intent.getAction();

            //verify action is to capture a new photo
            if (filter != null && (filter.equals(OK_GOOGLE_CAPTURE_PHOTO) || filter.equals(OK_GOOGLE_CAPTURE_STILL))) {

                //set intent for activity to open then capture a photo, set flags and start the activity
                Intent voiceIntent = new Intent(ACTION_OK_CAPTURE_PHOTO);
                voiceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(voiceIntent);

                //set the result and finish the activity
                setResult(RESULT_OK);
                finish();

                //check if filter is for ok google
            } else if (filter != null && filter.equals(ACTION_OK_CAPTURE_PHOTO)) {

                //capture a photo
                capturePhoto();
            }
        }
    }


    //onRequestPermissionsResult - verify permission request and update UI
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        //check if storage permission was requested
        if (requestCode == RC_STORAGE_PERMISSION) {

            //check if permission is given for user location
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //retrieve the user's current location
                getUserLocation();
            }

            //verify permission was given
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                //reload data model and reload current gallery
                mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);
                showSelectedFragment(mSelectedFragment);

                //if permission was not given
            } else {

                //create alert to inform user storage permission is required
                AlertDialog.Builder permissionAlertBuilder = new AlertDialog.Builder(MainActivity.this);

                //set title, message, and buttons, prevent alert cancellation
                permissionAlertBuilder.setTitle(R.string.storage_permission);
                permissionAlertBuilder.setCancelable(false);
                permissionAlertBuilder.setMessage(R.string.need_permission_message);
                permissionAlertBuilder.setPositiveButton(R.string.provide_permission, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //request permissions again
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, RC_STORAGE_PERMISSION);
                    }
                });
                permissionAlertBuilder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //finish the activity to close the app due to requirement of storage permissions
                        finish();
                    }
                });

                //display the alert
                permissionAlertBuilder.create().show();
            }

            //if location permission was requested
        } else if (requestCode == RC_LOCATION) {

            //verify permission for location was granted
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                //get the user location
                getUserLocation();
            }
        }
    }


    //onActivityResult - verify request code and update UI
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check if result is to sign in
        if (resultCode == RESULT_SIGN_IN) {

            //start sign in process
            signIn();

            //check if result is to capture a new photo
        } else if (resultCode == RESULT_CAPTURE_NEW_PHOTO) {

            //start photo capture process
            capturePhoto();

            //check if the request was to capture a new photo
        } else if (requestCode == RC_CAPTURE_PHOTO) {

            //verify result is ok
            if (resultCode == RESULT_OK) {

                //verify photo file was properly stored
                if (mNewPhotoFile != null) {

                    //retrieve photo from the media store
                    MediaStoreUtils.storeMediaStorePhoto(MainActivity.this, mNewPhotoFile, null);

                    //verify storage permission is granted
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                        //update data model from media store, reload the selected gallery and open the newly captured photo in single photo view
                        mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);

                        //move the cursor to the first position
                        mPhotoCursor.moveToFirst();

                        //retrieve the path of the image
                        String imagePath = mPhotoCursor.getString(mPhotoCursor.getColumnIndex(MediaStore.Images.Media.DATA));

                        //create a file for the image
                        File imageFile = new File(imagePath);

                        //store the exif data for the photo
                        MediaStoreUtils.saveExifData(imageFile, mCurrentLocation, mNewPhotoFile);

                        //update data model from media store, reload the selected gallery and open the newly captured photo in single photo view
                        mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);

                        //show the selected fragment
                        showSelectedFragment(mSelectedFragment);
                        photoClicked(MediaStoreUtils.getLastPhotoCaptured(mPhotoCursor));
                    }

                    //remove the new photo file
                    boolean removed = mNewPhotoFile.delete();

                    //check if the photo was removed
                    if (removed) {

                        //log that the file was removed
                        Log.i("MainActivity", "File Removed");
                    }

                    //remove the file from memory
                    mNewPhotoFile = null;
                }
            }

            //if request was to search the map
        } else if (requestCode == RC_SEARCH_MAP) {

            //verify result is ok
            if (resultCode == RESULT_OK) {

                //retrieve the selected place from the intent data
                Place selectedPlace = PlaceAutocomplete.getPlace(MainActivity.this, data);

                //retrieve the map fragment
                GalleryMapFragment galleryMapFragment = (GalleryMapFragment) getFragmentManager().findFragmentByTag(GalleryMapFragment.TAG);

                //verify the map fragment is available
                if (galleryMapFragment != null) {

                    //zoom the map to the selected location and display a toast to inform the user of the selected place
                    galleryMapFragment.zoomToSearchLocation(selectedPlace.getLatLng());
                    Toast.makeText(MainActivity.this, selectedPlace.getName().toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }

        //check if data is not from request code but is available
        if (data != null) {

            //retrieve extras bundle
            Bundle extras = data.getExtras();

            //verify extras are available
            if (extras != null) {

                //retrieve booleans for signed in and signed out
                boolean signedIn = extras.getBoolean(EXTRA_SIGNED_IN, false);
                boolean signedOut = extras.getBoolean(EXTRA_SIGNED_OUT, false);

                //check if user signed in
                if (signedIn) {

                    //verify Firebase is authenticated
                    if (mAuth.getCurrentUser() != null) {

                        //verify toolbar menu is available
                        if (mToolbarMenu != null) {

                            //hide and show menu items for signed in user
                            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_profile);
                            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_in);
                            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_out);

                            //check if the request code was to open shared folders
                            if (requestCode == RC_OPEN_SHARED_FOLDERS) {

                                //verify Firebase auth is available and a current user is available
                                if (mAuth != null) {

                                    if (mAuth.getCurrentUser() != null) {

                                        //check if internet is available
                                        if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                                            //create intent and open the shared folders activity
                                            Intent sharedFoldersIntent = new Intent(MainActivity.this, SharedFoldersActivity.class);
                                            sharedFoldersIntent.putExtra(EXTRA_USER_IS_SUBSCRIBED, mUserSubscribed);
                                            startActivityForResult(sharedFoldersIntent, RC_CAPTURE_PHOTO);

                                            //if no internet
                                        } else {

                                            //display toast to inform user internet is required for SharedFolders
                                            Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //if user signed out
                if (signedOut) {

                    //sign out the user
                    signOut();
                }
            }
        }

        //check if the request code is to buy a subscription
        if (requestCode == RC_BUY_SUBSCRIPTION) {

            //verify the billing client is available
            if (mBillingClient != null) {

                //check if the billing client is not connected
                if (!mBillingConnected) {

                    //start the billing connection
                    mBillingClient.startConnection(mBillingClientStateListener);

                    //if the client is connected
                } else {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserSubscribed = true;

                                        //verify toolbar menu is available
                                        if (mToolbarMenu != null) {

                                            //hide the subscribe menu item
                                            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_subscribe);
                                        }
                                    }
                                }
                            }

                            //check if the user is not subscribed
                            if (!mUserSubscribed) {

                                //verify toolbar menu is available
                                if (mToolbarMenu != null) {

                                    //show the subscribe menu item
                                    MenuUtils.showMenuItem(mToolbarMenu, R.id.action_subscribe);
                                }
                            }
                        }
                    });
                }
            }
        }
    }


    //signOut - sign out user and display menu items for signed out user
    private void signOut() {

        //sign out of Firebase and log out of Facebook
        mAuth.signOut();
        mLoginManager.logOut();

        //verify toolbar menu is available
        if (mToolbarMenu != null) {

            //hide and show menu items for signed out user
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_profile);
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_in);
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
        }

        //display toast to inform user they have signed out
        Toast.makeText(MainActivity.this, getString(R.string.signed_out), Toast.LENGTH_SHORT).show();
    }


    //mTabSelectedListener - listener for TabLayout tab selection to navigate between galleries
    private final TabLayout.OnTabSelectedListener mTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            //hide the progress bar
            hideProgressBar();

            //retrieve tab text
            CharSequence tabText = tab.getText();

            //verify text is available
            if (tabText != null) {

                //switch on the selected tab text
                switch (tabText.toString()) {

                    //grid
                    case "GRID":

                        //show the selected fragment and correct options menu items
                        showSelectedFragment(0);
                        break;

                    //map
                    case "MAP":

                        //show the selected fragment and correct options menu items
                        showSelectedFragment(1);
                        break;

                    //calendar
                    case "CALENDAR":

                        //show the selected fragment and correct options menu items
                        showSelectedFragment(2);
                        break;
                }
            }
        }

        //required overrides
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {}
        @Override
        public void onTabReselected(TabLayout.Tab tab) {}
    };


    //displayGallery - display the gallery based on index received as parameter
    @Override
    public void displayGallery(int _galleryIndex) {

        //show the selected fragment passing received index
        showSelectedFragment(_galleryIndex);
    }


    //showSelectedFragment - display fragment and toolbar options according to the selected tab
    private void showSelectedFragment(int _selectedIndex) {

        //store the selected fragment index
        mSelectedFragment = _selectedIndex;

        //switch on the selected tab index
        switch (_selectedIndex) {

            //GRID
            case 0:

                //verify toolbar menu is available
                if (mToolbarMenu != null) {

                    //hide the search menu item
                    MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_search_map);
                }

                //verify data model cursor is available
                if (mPhotoCursor != null) {

                    //replace fragment with list fragment
                    getFragmentManager().beginTransaction()
                            .replace(R.id.gallery_list_frame_layout_fragment_container, GalleryListFragment.newInstance(), GalleryListFragment.TAG)
                            .commit();

                    //replace fragment with Grid fragment
                    getFragmentManager().beginTransaction()
                            .replace(R.id.gallery_frame_layout_fragment_container, GalleryGridFragment.newInstance(), GalleryGridFragment.TAG)
                            .commit();
                }

                break;

            //MAP
            case 1:

                //verify toolbar menu is available
                if (mToolbarMenu != null) {

                    //show the search menu item
                    MenuUtils.showMenuItem(mToolbarMenu, R.id.action_search_map);
                }

                //check if location permission has not been given
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    //request location permission
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, RC_LOCATION);

                    //if location permission is given
                } else {

                    //get the current user location
                    getUserLocation();
                }

                //replace fragment with list fragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.gallery_list_frame_layout_fragment_container, GalleryListFragment.newInstance(), GalleryListFragment.TAG)
                        .commit();

                //replace fragment with Map fragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.gallery_frame_layout_fragment_container, GalleryMapFragment.newInstance(), GalleryMapFragment.TAG)
                        .commit();

                break;

            //CALENDAR
            case 2:

                //verify toolbar menu is available
                if (mToolbarMenu != null) {

                    //hide the search menu item
                    MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_search_map);
                }

                //replace fragment with list fragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.gallery_list_frame_layout_fragment_container, GalleryListFragment.newInstance(), GalleryListFragment.TAG)
                        .commit();

                //replace fragment with Calendar fragment
                getFragmentManager().beginTransaction()
                        .replace(R.id.gallery_frame_layout_fragment_container, GalleryCalendarFragment.newInstance(), GalleryCalendarFragment.TAG)
                        .commit();

                break;
        }
    }


    //onCreateOptionsMenu - inflate menu from resources, load fragment, and hide options items accordingly
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //store reference to toolbar menu
        mToolbarMenu = menu;

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_main_gallery_activity, menu);

        //verify Firebase auth has been referenced
        if (mAuth != null) {

            //retrieve current Firebase user and verify the user is available
            FirebaseUser user = mAuth.getCurrentUser();

            //if user is signed into firebase
             if (user != null){

                 //hide and show menu items for signed in user
                 MenuUtils.showMenuItem(mToolbarMenu, R.id.action_profile);
                 MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_in);

                 //if user is not signed in
             } else {

                 //hide the sign out option
                 MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
             }
        }

        //try
        try {

            //verify tab layout is available
            if (mTabLayout != null) {

                //retrieve selected tab
                int selectedTab = mTabLayout.getSelectedTabPosition();

                //load the selected fragment and hide options menu items accordingly
                showSelectedFragment(selectedTab);
            }

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //check if the user is subscribed
        if (mUserSubscribed) {

            //verify toolbar menu is available
            if (mToolbarMenu != null) {

                //hide the subscribe button
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_subscribe);
            }
        }

        //return true
        return true;
    }


    //onOptionsItemSelected - verify the selected options item by index and take action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the id of the selected menu item
        switch (item.getItemId()) {

            //search the map
            case R.id.action_search_map:

                //display AutoComplete place search
                searchMap();

                break;

            //capture a new photo
            case R.id.action_capture_photo:

                //open default camera to capture a new photo
                capturePhoto();

                break;

            //shared folders
            case R.id.action_shared_folders:

                //verify Firebase auth is available and a current user is available
                if (mAuth != null) {

                    if (mAuth.getCurrentUser() != null) {

                        //check if internet is available
                        if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                            //create intent and open the shared folders activity
                            Intent sharedFoldersIntent = new Intent(MainActivity.this, SharedFoldersActivity.class);
                            sharedFoldersIntent.putExtra(EXTRA_USER_IS_SUBSCRIBED, mUserSubscribed);
                            startActivityForResult(sharedFoldersIntent, RC_CAPTURE_PHOTO);

                            //if no internet
                        } else {

                            //display toast to inform user internet is required for SharedFolders
                            Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                        }

                        //if a user is not currently signed in
                    } else {

                        //set boolean to open shared folders to true
                        mOpenSharedFolders = true;

                        //start sign in process
                        signIn();
                    }
                }

                break;

            //sync
            case R.id.action_sync:

                //check if internet is available
                if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                    //begin syncing process
                    startSync();

                    //if no internet
                } else {

                    //display toast to inform user internet is required for sync
                    Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //export to sd
            case R.id.action_export_sd:

                //begin export process
                export();

                break;

            //subscribe
            case R.id.action_subscribe:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                    //begin subscription process
                    startSubscription();

                //if no internet
                } else {

                    //display toast to inform user internet is required for subscription
                    Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //sign in
            case R.id.action_sign_in:

                //start sign in process
                signIn();

                break;

            //profile
            case R.id.action_profile:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                    //create intent and start the profile activity
                    Intent profileIntent = new Intent(MainActivity.this, ProfileActivity.class);
                    startActivityForResult(profileIntent, RC_SIGN_OUT);

                    //if no internet
                } else {

                    //display toast to inform user internet is required for profile
                    Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //sign out
            case R.id.action_sign_out:

                //sign out of current accounts
                signOut();

                break;
        }

        //return result of super implementation
        return super.onOptionsItemSelected(item);
    }


    //startSubscription - create alert to inform user of subscription benefits and allow user to start the subscription process
    private void startSubscription() {

        //create alert
        AlertDialog.Builder subscriptionAlertBuilder = new AlertDialog.Builder(MainActivity.this);

        //set title, message, buttons, and prevent cancellation
        subscriptionAlertBuilder.setTitle(R.string.title_start_subscription);
        subscriptionAlertBuilder.setCancelable(false);
        subscriptionAlertBuilder.setMessage(R.string.message_start_subscription);

        //set positive button to start the subscription process
        subscriptionAlertBuilder.setPositiveButton(R.string.subscribe, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //verify billing client is available
                if (mBillingClient != null) {

                    //try
                    try {

                        //create BillingFlowParams for a monthly subscription
                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                .setSku(SUBSCRIPTION_SKU)
                                .setType(BillingClient.SkuType.SUBS)
                                .build();

                        //set the request code and launch the subscription process
                        RC_BUY_SUBSCRIPTION = mBillingClient.launchBillingFlow(MainActivity.this, billingFlowParams);

                        //catch any exceptions and print the stack trace
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //set the negative button to cancel and complete no action
        subscriptionAlertBuilder.setNegativeButton(getString(R.string.maybe_later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //display the alert
        subscriptionAlertBuilder.create().show();
    }


    //storageTotaled - called when storage amount for user has been totaled and beings upload if space is available
    @Override
    public void storageTotaled(boolean _spaceIsAvailable) {

        //check if space is available
        if (_spaceIsAvailable) {

            //store the download listener
            mDownloadBackupListener = MainActivity.this;

            //create upload task and execute
            PhotoUploadTask photoUploadTask = new PhotoUploadTask();
            photoUploadTask.execute(mPhotoCursor);

            //display toast to inform user the upload has started
            Toast.makeText(MainActivity.this, getString(R.string.uploading), Toast.LENGTH_SHORT).show();

            //if no space available
        } else {

            //display toast to inform user there is no space available to upload
            Toast.makeText(MainActivity.this, R.string.no_available_storage_space, Toast.LENGTH_SHORT).show();
        }
    }


    //startSync - start the syncing process for subscribed users and direct unsubscribed users to subscribe
    private void startSync() {

        //check if user is subscribed
        if (mUserSubscribed) {

            //verify Firebase auth is referenced and user is authorized
            if (mAuth != null) {

                if (mAuth.getCurrentUser() != null) {

                    //create alert to start sync process
                    AlertDialog.Builder startSyncAlertDialogBuilder = new AlertDialog.Builder(MainActivity.this);

                    //set title, message, and buttons
                    startSyncAlertDialogBuilder.setTitle(R.string.sync_pictures);
                    startSyncAlertDialogBuilder.setMessage(R.string.message_alert_start_sync);

                    //set the neutral button
                    startSyncAlertDialogBuilder.setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    //set the negative button
                    startSyncAlertDialogBuilder.setNegativeButton(getString(R.string.backup), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //verify network is connected
                            if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                                //retrieve total bytes for user in database
                                PhotoUtils.getSubscribersTotalBytes(MainActivity.this, mAuth.getCurrentUser().getUid());

                                //if no network connection
                            } else {

                                //display toast to inform user there is not a network connection
                                Toast.makeText(MainActivity.this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    //set the positive button
                    startSyncAlertDialogBuilder.setPositiveButton(getString(R.string.download), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //verify network is connected
                            if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                                //store reference to listener
                                mDownloadBackupListener = MainActivity.this;

                                //get the application context and start the download task
                                final Context context = getApplicationContext();
                                PhotoDownloadTask photoDownloadTask = new PhotoDownloadTask();
                                photoDownloadTask.execute(context);

                                //display toast to inform user their download has started
                                Toast.makeText(MainActivity.this, R.string.downloading, Toast.LENGTH_SHORT).show();

                                //if no network connection
                            } else {

                                //display toast to inform user there is not a network connection
                                Toast.makeText(MainActivity.this, R.string.no_network_connection, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    //display the alert
                    startSyncAlertDialogBuilder.create().show();

                    //if not signed in
                } else {

                    //start sign in process
                    signIn();
                }
            }

            //if no subscription
        } else {

            //check for internet
            if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                //begin subscription process
                startSubscription();

                //if no internet
            } else {

                //display toast to inform user internet is required for subscription
                Toast.makeText(MainActivity.this, R.string.subscription_check_network, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //signIn - start sign in activity if internet is available
    private void signIn() {

        //check for internet
        if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

            //check if user wants to view shared folders
            if (mOpenSharedFolders) {

                //set open shared folders to false
                mOpenSharedFolders = false;

                //create intent and start sign in activity
                Intent signInIntent = new Intent(MainActivity.this, SignInActivity.class);
                startActivityForResult(signInIntent, RC_OPEN_SHARED_FOLDERS);

                //if user has not attempted to open shared folders
            } else {

                //create intent and start sign in activity
                Intent signInIntent = new Intent(MainActivity.this, SignInActivity.class);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }


            //if no internet
        } else {

            //display toast to inform user internet is required for sign in
            Toast.makeText(MainActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
        }
    }


    //export - begin export process
    private void export() {

        //verify user has a subscription
        if (mUserSubscribed) {

            //display toast to inform user the photos are exporting
            Toast.makeText(MainActivity.this, R.string.exporting, Toast.LENGTH_SHORT).show();

            //export photos to the sd card in PicturesEverywhere directory.
            PhotoUtils.exportPhotosToSD(MainActivity.this, this);

            //if not subscribed
        } else {

            //check for internet
            if (InternetUtils.getIsNetworkAvailable(MainActivity.this)) {

                //begin subscription process
                startSubscription();

                //if no internet
            } else {

                //display toast to inform user internet is required for subscription
                Toast.makeText(MainActivity.this, R.string.subscription_check_network, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //exportCompleted - called when export of photos to SD card is completed
    @Override
    public void exportCompleted() {

        //display toast to inform user the export is complete
        Toast.makeText(MainActivity.this, R.string.export_complete, Toast.LENGTH_SHORT).show();
    }


    //capturePhoto - begin capture photo process with built in device camera
    private void capturePhoto() {

        //create and store a file for the new photo
        mNewPhotoFile = MediaStoreUtils.getFileForNewPhoto();

        //verify file was created
        if (mNewPhotoFile != null) {

            //retrieve the uri for the new photo
            Uri uri = MediaStoreUtils.getFileUri(mNewPhotoFile, MainActivity.this);

            //verify the uri is available
            if (uri != null) {

                //create a new intent, set uri extra and start the photo capture activity
                Intent capturePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                capturePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

                //retrieve the package manager and verify a camera app is available
                PackageManager packageManager = getPackageManager();
                List<ResolveInfo> listCam = packageManager.queryIntentActivities(capturePhotoIntent, 0);

                //loop over camera apps
                for (ResolveInfo res : listCam) {

                    //verify camera app
                    if (res.activityInfo.packageName.toLowerCase().contains("camera")) {

                        //start the camera activity for a capture result
                        capturePhotoIntent.setPackage(res.activityInfo.packageName);
                        startActivityForResult(capturePhotoIntent, RC_CAPTURE_PHOTO);
                        break;
                    }
                }
            }
        }
    }


    //searchMap - display PlaceAutocomplete popover allowing user to search the map
    private void searchMap() {

        //create place intent builder for popover
        PlaceAutocomplete.IntentBuilder placeIntentBuilder = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY);

        //try
        try {

            //set map is searched
            mMapIsSearched = true;

            //create an intent with the builder and start the search activity
            Intent placeAutoIntent = placeIntentBuilder.build(this);
            startActivityForResult(placeAutoIntent, RC_SEARCH_MAP);

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    //photoClicked - open clicked photo in Single Photo Activity
    @Override
    public void photoClicked(File _photoFile) {

        //create a new intent, add file for clicked photo as extra and start the activity
        Intent singlePhotoIntent = new Intent(MainActivity.this, SinglePhotoActivity.class);
        singlePhotoIntent.putExtra(EXTRA_PHOTO_FILE, _photoFile);
        singlePhotoIntent.putExtra(EXTRA_USER_IS_SUBSCRIBED,mUserSubscribed);
        startActivityForResult(singlePhotoIntent, RC_CAPTURE_PHOTO);
    }


    //getPhotoCursor - return the current cursor data model
    @Override
    @Nullable
    public Cursor getPhotoCursor() {

        //return the current data model
        return mPhotoCursor;
    }


    //addMapMarker - add a map marker for the map photo file received as parameter
    @Override
    public void addMapMarker(MapPhotoFile _mapPhotoFile) {

        //retrieve the map fragment
        GalleryMapFragment galleryMapFragment = (GalleryMapFragment) getFragmentManager().findFragmentByTag(GalleryMapFragment.TAG);

        //verify the map fragment is available
        if (galleryMapFragment != null) {

            //add a map marker
            galleryMapFragment.addMarker(_mapPhotoFile);
        }
    }


    //getMarkers - retrieve the map photos from the current data model
    @Override
    public void getMarkers() {

        //retrieve the map photos setting this as the listener for retrieved photos to display markers
        PhotoUtils.getMapPhotos(MainActivity.this, this, mPhotoCursor);
    }


    //getCalendarPhotos - retrieve MediaStore images for calendar, create and return calendar gallery data model
    @Override
    public void getCalendarPhotos() {

        //show the animating progress bar
        showProgressBar();

        //retrieve the photos with date meta data setting this as the listener for the created data model
        PhotoUtils.getCalendarMonths(MainActivity.this, this);
    }


    //calendarDateClicked - open CalendarDateFolder activity for the clicked date received as parameter
    @Override
    public void calendarDateClicked(Date _clickedDate) {

        //create a new intent for the activity to display date photos
        Intent calendarDateIntent = new Intent(MainActivity.this, CalendarDateFolderActivity.class);

        //add the date and data model for the selected date as intent extras, add subscription boolean
        calendarDateIntent.putExtra(EXTRA_DATE, _clickedDate);
        calendarDateIntent.putExtra(EXTRA_CALENDAR_DATE_FOLDER, MediaStoreUtils.getDateFolderPhotos(_clickedDate, mCalendarPhotos));
        calendarDateIntent.putExtra(EXTRA_USER_IS_SUBSCRIBED, mUserSubscribed);

        //start the date activity
        startActivityForResult(calendarDateIntent, RC_CAPTURE_PHOTO);
    }


    //storeAllMonths - store the calendar data model
    @Override
    public void storeAllMonths(HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> _allCalendarMonths) {

        //store the received data model
        mCalendarPhotos = _allCalendarMonths;
    }


    //displayCalendarMonths - display the calendar months with photos available
    @Override
    public void displayCalendarMonths(ArrayList<CalendarMonth> _calendarMonths) {

        //hide the animating progress bar
        hideProgressBar();

        //retrieve the calendar fragment
        GalleryCalendarFragment galleryCalendarFragment = (GalleryCalendarFragment) getFragmentManager().findFragmentByTag(GalleryCalendarFragment.TAG);

        //verify the fragment is available
        if (galleryCalendarFragment != null) {

            //display the calendar months with photos in fragment
            galleryCalendarFragment.displayCalendarPhotos(_calendarMonths);
        }
    }


    //showProgressBar - verify the progress bar has been referenced, show, and animate
    private void showProgressBar() {

        //verify the progress bar is available
        if (mProgressBar != null) {

            //set the bar to visible and animating
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.animate();
        }
    }


    //hideProgressBar - verify the progress bar has been referenced, hide
    private void hideProgressBar() {

        //verify the progress bar is available
        if (mProgressBar != null) {

            //set the bar visibility to gone
            mProgressBar.setVisibility(View.GONE);
        }
    }


    //getUserLocation - verify permissions and get the stored or current user location
    private void getUserLocation() {

        //verify location permissions are given
        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //retrieve location service from the system
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            //verify location manager has been referenced
            if (locationManager != null) {

                //verify GPS provider is enabled
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                        || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

                    //retrieve the last known location
                    Location lastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    Location lastNetworkLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                    //verify location is available
                    if (lastLocation != null) {

                        //store the last know location
                        mCurrentLocation = lastLocation;

                        //if the last network location is available
                    } else if (lastNetworkLocation != null) {

                        //store the last know location
                        mCurrentLocation = lastNetworkLocation;

                        //if the last location is unavailable
                    } else {

                        //request a single location update for GPS and NETWORK
                        locationManager.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, Looper.myLooper());
                        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, this, Looper.myLooper());
                    }

                    //if location manager is not available
                } else {

                    //display a toast to inform user to check their device location settings to enable location
                    Toast.makeText(MainActivity.this, R.string.check_location_settings, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    //onLocationChanged - store the received user location
    @Override
    public void onLocationChanged(Location location) {

        //store location received as parameter in member variable
        mCurrentLocation = location;
    }


    //required overrides
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}
    @Override
    public void onProviderEnabled(String s) {}
    @Override
    public void onProviderDisabled(String s) {}


    //getCurrentLocation - return the current user location
    public Location getCurrentLocation() {

        //return the current user location
        return mCurrentLocation;
    }


    //downloadComplete - called when a photo has finished downloading a photo
    @Override
    public void downloadComplete() {

        //create new runnable and run on the ui thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //retrieve and display the current data model
                mPhotoCursor = MediaStoreUtils.getMediaStoreCursor(MainActivity.this);
                showSelectedFragment(mSelectedFragment);
            }
        });
    }


    //fullDownloadComplete - called when sync download is completed to inform the user
    @Override
    public void fullDownloadComplete() {

        //create new runnable and run on the ui thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //display toast to inform user the download is complete
                Toast.makeText(MainActivity.this, R.string.download_complete, Toast.LENGTH_SHORT).show();
            }
        });
    }


    //fullUploadComplete - called when sync upload is completed to inform the user
    @Override
    public void fullUploadComplete() {

        //create new runnable and run on the ui thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //display toast to inform user the upload is complete
                Toast.makeText(MainActivity.this, R.string.upload_complete, Toast.LENGTH_SHORT).show();
            }
        });
    }


    //PhotoUploadTask - uploads backup photos to firebase
    private static class PhotoUploadTask extends AsyncTask<Cursor, Void, Void> {

        @Override
        protected Void doInBackground(Cursor... cursors) {

            //verify interface is available
            if (mDownloadBackupListener != null) {

                //begin upload
                PhotoUtils.uploadAllPhotosToFirebase(cursors[0], mDownloadBackupListener);
            }

            //return null
            return null;
        }
    }


    //PhotoDownloadTask - download photos from backup
    private static class PhotoDownloadTask extends AsyncTask<Context, Void, Void> {

        @Override
        protected Void doInBackground(Context... contexts) {

            if (mDownloadBackupListener != null) {

                //start retrieval of all photos in Firebase
                PhotoUtils.getAllPhotosFromFirebase(contexts[0], mDownloadBackupListener);
            }

            //return null
            return null;
        }
    }
}