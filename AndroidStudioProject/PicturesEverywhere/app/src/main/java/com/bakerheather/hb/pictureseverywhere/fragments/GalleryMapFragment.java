/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.dataclasses.MapPhotoFile;
import com.bakerheather.hb.pictureseverywhere.dataclasses.PhotoMarker;
import com.bakerheather.hb.pictureseverywhere.interfaces.UserLocationListener;
import com.bakerheather.hb.pictureseverywhere.utilities.MediaStoreUtils;
import com.bakerheather.hb.pictureseverywhere.interfaces.MapListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;


//GalleryMapFragment - displays the google map with markers for each photo with available location data
public class GalleryMapFragment extends MapFragment implements OnMapReadyCallback, GoogleMap.InfoWindowAdapter {


    //fragment tag
    public static final String TAG = "FRAGMENT_GALLERY_MAP";

    //reference for the map
    private GoogleMap mMap;

    //references for the interfaces
    private PhotoClickedListener mPhotoClickedListener;
    private MapListener mMapListener;
    private UserLocationListener mUserLocationListener;

    //array list of PhotoMarkers to store the photo data for each marker
    private final ArrayList<PhotoMarker> mPhotoMarkers = new ArrayList<>();


    //newInstance - create and return a new instance of the fragment
    public static GalleryMapFragment newInstance() {

        //return a new instance of the fragment
        return new GalleryMapFragment();
    }


    //onAttach - verify context implements required interfaces
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify interface is implemented
        if (context instanceof PhotoClickedListener) {

            //cast and store reference
            mPhotoClickedListener = (PhotoClickedListener) context;
        }

        //verify interface is implemented
        if (context instanceof MapListener) {

            //cast and store reference
            mMapListener = (MapListener) context;
        }

        //verify interface is implemented
        if (context instanceof UserLocationListener) {

            //cast and store reference
            mUserLocationListener = (UserLocationListener) context;
        }
    }


    //onActivityCreated - get the map asynchronously
    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        //retrieve the map asynchronously
        getMapAsync(this);
    }


    //onMapReady - setup the map with adapters, zoom to the user's location and retrieve the markers
    @Override
    public void onMapReady(GoogleMap googleMap) {

        //store a reference to the amp
        mMap = googleMap;

        //set the adapter, click listener, and enable buildings
        mMap.setInfoWindowAdapter(this);
        mMap.setOnInfoWindowClickListener(mInfoWindowClickListener);
        mMap.setBuildingsEnabled(true);

        //verify permission for user location
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //enable user location in the map
            mMap.setMyLocationEnabled(true);
        }

        //get the user's current location
        Location curUserLocation = mUserLocationListener.getCurrentLocation();

        //verify the location is available
        if (curUserLocation != null) {

            //zoom to the user's location
            zoomToLocation(new LatLng(curUserLocation.getLatitude(), curUserLocation.getLongitude()));
        }

        //verify the map listener is available
        if (mMapListener != null) {

            //retrieve map markers for photos with location data
            mMapListener.getMarkers();
        }
    }


    //getInfoWindow - required implementation returns null
    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }


    //getInfoContents - set up and return view of info window for the received marker
    @Override
    public View getInfoContents(Marker marker) {

        //verify info window is available
        if (getActivity().getResources().getIdentifier("map_info_window", "layout", getActivity().getPackageName()) != 0) {

            //retrieve map info window view
            /*
             * Lint warning suppressed due to intentional passing of null as root view
             * */
            //inflate the layout from resources
            @SuppressLint("InflateParams")
            View infoWindow = LayoutInflater.from(getActivity()).inflate(R.layout.map_info_window, null);

            //retrieve the marker for the photo
            PhotoMarker photoMarker = getPhotoMarker(marker);

            //verify photo marker exists
            if (photoMarker != null) {

                //retrieve the square image view
                SquareGridImageView imageView = infoWindow.findViewById(R.id.map_info_window_image);

                //retrieve the bitmap for the preview
                Bitmap photo = MediaStoreUtils.getPhotoBitmapFromFile(photoMarker.getPhotoFile());

                //rotate the photo based on EXIF data
                Bitmap rotatedPhoto = MediaStoreUtils.rotatePhoto(photo, MediaStoreUtils.getRotateDegrees(photoMarker.getPhotoFile()));

                //verify rotated photo is available
                if (rotatedPhoto != null) {

                    //set the photo as the rotated version
                    photo = rotatedPhoto;
                }

                //verify the photo is available
                if (photo != null) {

                    //set the bitmap in the image view and return the info window view
                    imageView.setImageBitmap(photo);
                    return infoWindow;
                }
            }
        }

        //return null
        return null;
    }


    //getPhotoMarker - create and return a PhotoMarker for the received Marker
    private PhotoMarker getPhotoMarker(Marker _marker) {

        //loop over the available PhotoMarkers
        for (PhotoMarker photoMarker: mPhotoMarkers) {

            //verify the current markers match
            if (_marker.getTitle().equals(photoMarker.getMarker().getTitle())) {

                //return the PhotoMarker
                return photoMarker;
            }
        }

        //return null
        return null;
    }


    //addMarker - add a marker to the map for the received PhotoFile
    public void addMarker(MapPhotoFile _mapPhotoFile) {

        //verify map is available
        if (mMap != null) {

            //create new MarkerOptions
            MarkerOptions markerOptions = new MarkerOptions();

            //set the position and title, create new Marker and add to the map
            markerOptions.position(new LatLng(_mapPhotoFile.getLatitude(), _mapPhotoFile.getLongitude()));
            markerOptions.title(_mapPhotoFile.getFile().getAbsolutePath());
            Marker marker = mMap.addMarker(markerOptions);

            //create new PhotoMarker and add to the ArrayList
            mPhotoMarkers.add(new PhotoMarker(marker, _mapPhotoFile.getFile()));
        }
    }


    //mInfoWindowClickListener - open clicked photo in single photo activity
    private final GoogleMap.OnInfoWindowClickListener mInfoWindowClickListener = new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {

            //retrieve the PhotoMarker for the received map marker
            PhotoMarker photoMarker = getPhotoMarker(marker);

            //verify marker and listener are available
            if (photoMarker != null && mPhotoClickedListener != null) {

                //use interface to open the clicked photo in the single photo activity
                mPhotoClickedListener.photoClicked(photoMarker.getPhotoFile());
            }
        }
    };


    //zoomToLocation - zoom the map to the received lat and lng
    private void zoomToLocation(LatLng _latLng){

        //verify LatLng is available
        if (_latLng != null) {

            //create a new camera position for the specified coordinates
            CameraPosition pocLocation = new CameraPosition.Builder()
                    .target(_latLng)
                    .zoom(8f)
                    .build();

            //animate the map camera to the position
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(pocLocation));
        }
    }


    //zoomToSearchLocation - zoom the map to the received lat and lng with 12f zoom
    public void zoomToSearchLocation(LatLng _latLng){

        //verify LatLng is available
        if (_latLng != null) {

            //create a new camera position for the specified coordinates
            CameraPosition pocLocation = new CameraPosition.Builder()
                    .target(_latLng)
                    .zoom(12f)
                    .build();

            //animate the map camera to the position
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(pocLocation));
        }
    }
}