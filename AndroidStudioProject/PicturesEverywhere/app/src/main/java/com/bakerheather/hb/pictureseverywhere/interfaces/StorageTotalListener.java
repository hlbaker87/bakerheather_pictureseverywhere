/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//StorageTotalListener - interface to update activity when a user's total storage used has been retrieved
public interface StorageTotalListener {

    //method to pass boolean of if space is available for user back to activity
    void storageTotaled(boolean _spaceIsAvailable);
}
