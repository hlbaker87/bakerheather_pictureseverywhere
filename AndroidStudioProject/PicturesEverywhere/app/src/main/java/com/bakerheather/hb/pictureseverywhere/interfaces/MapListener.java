/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//MapListener - interface to get the map markers once the map gallery has loaded
public interface MapListener {

    //method to initiate retrieval of map markers
    void getMarkers();
}
