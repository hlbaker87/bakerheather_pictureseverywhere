/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.bakerheather.hb.pictureseverywhere.interfaces.EffectAppliedListener;
import com.mukesh.image_processing.ImageProcessor;


//EffectUtils - utility class for adding effects to photos
public class EffectUtils {


    //variable to store reference to intervace
    private static EffectAppliedListener mEffectAppliedListener;

    //ints for user input values
    private static int mRed = 0;
    private static int mGreen = 0;
    private static int mBlue = 0;
    private static int mValue = 0;


    //applyGamma - store parameters for use in async task and add effect to the bitmap
    public static void applyGamma(Bitmap _bitmap, int _red, int _green, int _blue, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mRed = _red;
        mGreen = _green;
        mBlue = _blue;

        //create new instance of the effect task and execute
        GammaEffectTask gammaEffectTask = new GammaEffectTask();
        gammaEffectTask.execute(_bitmap);
    }


    //applySepia - store parameters for use in async task and add effect to the bitmap
    public static void applySepia(Bitmap _bitmap, int _red, int _green, int _blue, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mRed = _red;
        mGreen = _green;
        mBlue = _blue;

        //create new instance of the effect task and execute
        SepiaEffectTask sepiaEffectTask = new SepiaEffectTask();
        sepiaEffectTask.execute(_bitmap);
    }


    //applyGreyscale - store parameters for use in async task and add effect to the bitmap
    public static void applyGreyscale(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        GreyscaleEffectTask greyscaleEffectTask = new GreyscaleEffectTask();
        greyscaleEffectTask.execute(_bitmap);
    }


    //applyInverted - store parameters for use in async task and add effect to the bitmap
    public static void applyInverted(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        InvertedEffectTask invertedEffectTask = new InvertedEffectTask();
        invertedEffectTask.execute(_bitmap);
    }


    //applyBlackfilter - store parameters for use in async task and add effect to the bitmap
    public static void applyBlackfilter(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        BlackfilterEffectTask blackfilterEffectTask = new BlackfilterEffectTask();
        blackfilterEffectTask.execute(_bitmap);
    }


    //applyBrighten - store parameters for use in async task and add effect to the bitmap
    public static void applyBrighten(Bitmap _bitmap, int _value, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mValue = _value;

        //create new instance of the effect task and execute
        BrightenEffectTask brightenEffectTask = new BrightenEffectTask();
        brightenEffectTask.execute(_bitmap);
    }


    //applyColorFilter - store parameters for use in async task and add effect to the bitmap
    public static void applyColorFilter(Bitmap _bitmap, int _red, int _green, int _blue, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mRed = _red;
        mGreen = _green;
        mBlue = _blue;

        //create new instance of the effect task and execute
        ColorFilterEffectTask colorFilterEffectTask = new ColorFilterEffectTask();
        colorFilterEffectTask.execute(_bitmap);
    }


    //applyContrast - store parameters for use in async task and add effect to the bitmap
    public static void applyContrast(Bitmap _bitmap, int _value, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mValue = _value;

        ContrastEffectTask contrastEffectTask = new ContrastEffectTask();
        contrastEffectTask.execute(_bitmap);
    }


    //applyDepth - store parameters for use in async task and add effect to the bitmap
    public static void applyDepth(Bitmap _bitmap, int _value, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mValue = _value;

        //create new instance of the effect task and execute
        DepthEffectTask depthEffectTask = new DepthEffectTask();
        depthEffectTask.execute(_bitmap);
    }


    //applyEmboss - store parameters for use in async task and add effect to the bitmap
    public static void applyEmboss(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        EmbossEffectTask embossEffectTask = new EmbossEffectTask();
        embossEffectTask.execute(_bitmap);
    }


    //applyEngrave - store parameters for use in async task and add effect to the bitmap
    public static void applyEngrave(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        EngraveEffectTask engraveEffectTask = new EngraveEffectTask();
        engraveEffectTask.execute(_bitmap);
    }


    //applyHorizontal - store parameters for use in async task and add effect to the bitmap
    public static void applyHorizontal(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        HorizontalEffectTask horizontalEffectTask = new HorizontalEffectTask();
        horizontalEffectTask.execute(_bitmap);
    }


    //applyVertical - store parameters for use in async task and add effect to the bitmap
    public static void applyVertical(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        VerticalEffectTask verticalEffectTask = new VerticalEffectTask();
        verticalEffectTask.execute(_bitmap);
    }


    //applyMean - store parameters for use in async task and add effect to the bitmap
    public static void applyMean(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        MeanEffectTask meanEffectTask = new MeanEffectTask();
        meanEffectTask.execute(_bitmap);
    }


    //applyReflection - store parameters for use in async task and add effect to the bitmap
    public static void applyReflection(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        ReflectionEffectTask reflectionEffectTask = new ReflectionEffectTask();
        reflectionEffectTask.execute(_bitmap);
    }


    //applyRotate - store parameters for use in async task and add effect to the bitmap
    public static void applyRotate(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        RotateEffectTask rotateEffectTask = new RotateEffectTask();
        rotateEffectTask.execute(_bitmap);
    }


    //applySnow - store parameters for use in async task and add effect to the bitmap
    public static void applySnow(Bitmap _bitmap, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //create new instance of the effect task and execute
        SnowEffectTask snowEffectTask = new SnowEffectTask();
        snowEffectTask.execute(_bitmap);
    }


    //applyTint - store parameters for use in async task and add effect to the bitmap
    public static void applyTint(Bitmap _bitmap, int _value, EffectAppliedListener _effectAppliedListener) {

        //store reference to listener
        mEffectAppliedListener = _effectAppliedListener;

        //store user input for effect
        mValue = _value;

        //create new instance of the effect task and execute
        TintEffectTask tintEffectTask = new TintEffectTask();
        tintEffectTask.execute(_bitmap);
    }


    //GammaEffectTask - AsyncTask to apply photo effect
    private static class GammaEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add gamma effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.doGamma(bitmaps[0], mRed, mGreen, mBlue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //SepiaEffectTask - AsyncTask to apply photo effect
    private static class SepiaEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add sepia effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.createSepiaToningEffect(bitmaps[0],1 ,mRed,mGreen,mBlue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //GreyscaleEffectTask - AsyncTask to apply photo effect
    private static class GreyscaleEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add grey scale effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.doGreyScale(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //InvertedEffectTask - AsyncTask to apply photo effect
    private static class InvertedEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add invert effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.doInvert(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //BlackfilterEffectTask - AsyncTask to apply photo effect
    private static class BlackfilterEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add black filter effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.applyBlackFilter(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //BrightenEffectTask - AsyncTask to apply photo effect
    private static class BrightenEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add brightness effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.doBrightness(bitmaps[0], mValue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //ColorFilterEffectTask - AsyncTask to apply photo effect
    private static class ColorFilterEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add color filter effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.doColorFilter(bitmaps[0], mRed, mGreen, mBlue);


                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //ContrastEffectTask - AsyncTask to apply photo effect
    private static class ContrastEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add contrast effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.createContrast(bitmaps[0], mValue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //DepthEffectTask - AsyncTask to apply photo effect
    private static class DepthEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add depth effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.decreaseColorDepth(bitmaps[0], mValue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //EmbossEffectTask - AsyncTask to apply photo effect
    private static class EmbossEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add emboss effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.emboss(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //EngraveEffectTask - AsyncTask to apply photo effect
    private static class EngraveEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add engrave effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.engrave(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //HorizontalEffectTask - AsyncTask to apply photo effect
    private static class HorizontalEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add flip effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.flip(bitmaps[0], 2);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //VerticalEffectTask - AsyncTask to apply photo effect
    private static class VerticalEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add flip effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.flip(bitmaps[0], 1);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //MeanEffectTask - AsyncTask to apply photo effect
    private static class MeanEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add mean effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.applyMeanRemoval(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //ReflectionEffectTask - AsyncTask to apply photo effect
    private static class ReflectionEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add reflection effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.applyReflection(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //RotateEffectTask - AsyncTask to apply photo effect
    private static class RotateEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try{

                //create image processor, add rotate effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.rotate(bitmaps[0], 90);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //SnowEffectTask - AsyncTask to apply photo effect
    private static class SnowEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add snow effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.applySnowEffect(bitmaps[0]);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }


    //TintEffectTask - AsyncTask to apply photo effect
    private static class TintEffectTask extends AsyncTask<Bitmap, Bitmap, Bitmap> {

        //doInBackground - apply effect to photo in background thread
        protected Bitmap doInBackground(Bitmap... bitmaps) {

            //try
            try {

                //create image processor, add tint effect and return the result
                ImageProcessor processor = new ImageProcessor();
                return processor.tintImage(bitmaps[0], mValue);

                //catch any exceptions and print the stack trace
            } catch (Exception e) {
                e.printStackTrace();
            }

            //return null
            return null;
        }


        //onPostExecute - use interface to send the result back to the UI
        protected void onPostExecute(Bitmap result) {

            //verify listener and result is available
            if (mEffectAppliedListener != null && result != null) {

                //send the result back to be displayed and stored
                mEffectAppliedListener.effectApplied(result);

                //if listener is available
            } else if (mEffectAppliedListener != null) {

                //send back null do denote effect failed
                mEffectAppliedListener.effectApplied(null);
            }
        }
    }
}
