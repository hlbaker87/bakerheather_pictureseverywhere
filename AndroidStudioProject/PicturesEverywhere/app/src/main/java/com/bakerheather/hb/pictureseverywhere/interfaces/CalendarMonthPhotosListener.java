/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;

import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarPhoto;

import java.util.ArrayList;
import java.util.HashMap;


//CalendarMonthPhotosListener - interface for displaying and storing photos sorted into calendar months
public interface CalendarMonthPhotosListener {

    //methods for displaying sorted calendar months and sorting months
    void displayCalendarMonths(ArrayList<CalendarMonth> _calendarMonths);
    void storeAllMonths(HashMap<String, HashMap<String, HashMap<String, ArrayList<CalendarPhoto>>>> _allCalendarMonths);
}
