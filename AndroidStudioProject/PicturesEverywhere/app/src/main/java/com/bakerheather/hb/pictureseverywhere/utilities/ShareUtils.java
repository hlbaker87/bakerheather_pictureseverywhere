/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


//ShareUtils - utility class for sharing photos to social media platforms
public class ShareUtils {


    //filename for share photo
    private static final String SHARING_PHOTO_FILENAME = "sharingphoto.png";


    //getSharingUri - store bitmap in internal File for sharing to apps that do not have external
    // read permissions and return Uri
    public static Uri getSharingUri(Context _context, Bitmap _bitmap) {

        //create boolean to determine if file is available
        boolean fileAvailable = false;

        //create file for cache directory
        File sharingFile = new File(_context.getCacheDir(), SHARING_PHOTO_FILENAME);

        //check if file does not already exist
        if (!sharingFile.exists()) {

            //try
            try {

                //attempt to create a new file and store boolean result
                fileAvailable = sharingFile.createNewFile();

            } catch (IOException e) {
                e.printStackTrace();
            }

            //if file already exists
        } else {

            //set boolean to true
            fileAvailable = true;
        }

        //verify the file is available
        if (fileAvailable) {

            //retrieve Uri for file
            Uri photoUri = MediaStoreUtils.getFileUri(sharingFile, _context);

            //verify photo Uri is available
            if (photoUri != null) {

                //try
                try {

                    //create output stream for file
                    FileOutputStream fileOutputStream = new FileOutputStream(sharingFile);
                    _bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream);

                    //flush and close the output stream
                    fileOutputStream.flush();
                    fileOutputStream.close();

                    //return the Uri with the photo to be shared
                    return photoUri;

                    //catch and exceptions and print the stack trace
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        //return null
        return null;
    }
}