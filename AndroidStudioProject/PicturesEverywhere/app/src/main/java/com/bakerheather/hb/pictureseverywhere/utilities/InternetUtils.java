/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


//InternetUtils - utility class for internet verification
public class InternetUtils {


    //getIsNetworkAvailable - return boolean determining if network access is available
    public static boolean getIsNetworkAvailable(Context _context) {

        //create boolean to determine connection
        boolean networkIsAvailable = false;

        //retrieve the ConnectivityManager
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);

        //verify the manager is available
        if (connectivityManager != null) {

            //get network info
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            //verify the network info is available
            if (networkInfo != null) {

                //store boolean of if the network is available
                networkIsAvailable = networkInfo.isConnected();
            }
        }

        //return boolean to denote if network is available
        return networkIsAvailable;
    }
}
