/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//ExportCompletedListener - interface to notify the activity when an export has completed
public interface ExportCompletedListener {

    //method to be called when the export is complete
    void exportCompleted();
}
