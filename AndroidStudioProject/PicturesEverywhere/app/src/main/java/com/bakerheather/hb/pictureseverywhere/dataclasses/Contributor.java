/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


//Contributor - class for creating objects to store the data of a shared folder contributor
public class Contributor {

    //member variables
    private final String mUID;
    private final String mEmail;

    //getters
    public String getUID() {
        return mUID;
    }

    public String getEmail() {
        return mEmail;
    }

    //public constructor setting uid and email as member variables
    public Contributor(String _UID, String _email) {

        //store uid and email in member variable
        this.mUID = _UID;
        this.mEmail = _email;
    }
}
