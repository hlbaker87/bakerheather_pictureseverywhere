/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.CalendarAdapter;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.interfaces.CalendarDateListener;

import java.util.ArrayList;

//GalleryCalendarFragment - displays calendar with highlighted days the have photos with saved date data
public class GalleryCalendarFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_GALLERY_CALENDAR";

    //reference for listener
    private CalendarDateListener mCalendarDateListener;

    //reference to the adapter
    private CalendarAdapter mCalendarAdapter;

    //ArrayList of CalendarMonths for data model
    private final ArrayList<CalendarMonth> mCalendarMonths = new ArrayList<>();


    //public constructor
    public GalleryCalendarFragment() {}


    //newInstance - create and return new instance of the fragment
    public static GalleryCalendarFragment newInstance() {

        //return a new instance of the fragment
        return new GalleryCalendarFragment();
    }


    //onAttach - verify required interface is implemented
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify interface is implemented
        if (context instanceof CalendarDateListener) {

            //cast and store reference
            mCalendarDateListener = (CalendarDateListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery_calendar, container, false);
    }


    //onActivityCreated - set up the GridView and retrieve the calendar photos
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify the view is available
        if (getView() != null) {

            //verify the listener is available
            if (mCalendarDateListener != null) {

                //retrieve the GridView and set empty state
                GridView monthGridView = getView().findViewById(R.id.gallery_calendar_grid_view);
                monthGridView.setEmptyView(getView().findViewById(R.id.fragment_gallery_calendar_empty_list_state));

                //create and reference a new CalendarAdapter
                mCalendarAdapter = new CalendarAdapter(getActivity(), mCalendarMonths, mCalendarDateListener);

                //set the adapter
                monthGridView.setAdapter(mCalendarAdapter);

                //retrieve the data model
                mCalendarDateListener.getCalendarPhotos();
            }
        }
    }


    //displayCalendarPhotos - setup data model with received data and notify the adapter of the change
    public void displayCalendarPhotos(ArrayList<CalendarMonth> _calendarMonths) {

        //clear data model and add received data
        mCalendarMonths.clear();
        mCalendarMonths.addAll(_calendarMonths);

        //verify the adapter is available
        if (mCalendarAdapter != null) {

            //notify the adapter of the change
            mCalendarAdapter.notifyDataSetChanged();
        }
    }
}