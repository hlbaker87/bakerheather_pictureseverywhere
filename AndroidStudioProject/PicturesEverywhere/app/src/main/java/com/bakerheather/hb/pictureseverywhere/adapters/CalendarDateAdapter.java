/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;

//CalendarDateAdapter - adapter to show photos for a clicked calendar date
public class CalendarDateAdapter extends BaseAdapter {


    //int for use in creating item ids
    private static final int HEX_ID_CONSTANT = 0x001001;

    //ArrayList of Files for data model
    private final ArrayList<File> mDatePhotos;

    //reference to context
    private final Context mContext;


    //public constructor setting all parameters as member variables
    public CalendarDateAdapter(Context _context, ArrayList<File> _datePhotos) {

        //store context and data model
        this.mContext = _context;
        this.mDatePhotos = _datePhotos;
    }


    //getCount - return the count of data model items
    @Override
    public int getCount() {

        //verify the data model is available
        if (mDatePhotos != null) {

            //return the size of the data model
            return mDatePhotos.size();

            //if data model is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the current position of the adapter
    @Override
    public File getItem(int position) {

        //verify the position is available
        if (mDatePhotos != null && position >= 0 && position < mDatePhotos.size()) {

            //return the File for the current position
            return mDatePhotos.get(position);

            //if a file for the received position is unavailable
        } else {

            //return a new file
            return new File("");
        }
    }


    //getItemId - return the id of the item at the current position
    @Override
    public long getItemId(int position) {

        //verify position is available in the data model
        if (mDatePhotos != null && position >= 0 && position < mDatePhotos.size()) {

            //return the position added to the id constant
            return HEX_ID_CONSTANT + position;

            //if position is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getView - configure and return the view for the current position of the adapter
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //create view holder
        DatePhotoViewHolder datePhotoViewHolder;

        //check if convertView is null
        if(convertView == null) {

            //inflate layout and store as convert view
            convertView = LayoutInflater.from(mContext).inflate(R.layout.gallery_grid_image_item, parent, false);

            //create new view holder and set as tag for the convertView
            datePhotoViewHolder = new DatePhotoViewHolder(convertView);
            convertView.setTag(datePhotoViewHolder);

            //if convertView is available
        } else {

            //retrieve and store the view holder
            datePhotoViewHolder = (DatePhotoViewHolder) convertView.getTag();
        }

        //retrieve the file for the current position
        File photoAtPosition = getItem(position);

        //try
        try {

            //load the current photo with Glide into the view
            Glide.with(mContext)
                    .load(photoAtPosition).into(datePhotoViewHolder.mImageView);

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();
        }

        //return the configured view
        return convertView;
    }


    //DatePhotoViewHolder - view holder for CalendarDateAdapter
    private static class DatePhotoViewHolder {

        //image view
        final SquareGridImageView mImageView;

        //constructor with view as parameter
        DatePhotoViewHolder(View _view) {

            //store the image view
            this.mImageView = _view.findViewById(R.id.image_view_grid_item);
        }
    }
}