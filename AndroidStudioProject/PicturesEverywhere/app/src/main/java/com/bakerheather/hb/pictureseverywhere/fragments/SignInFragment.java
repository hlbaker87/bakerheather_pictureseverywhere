/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.interfaces.SignInListener;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;


//SignInFragment - fragment allowing user to sign up and sign in with email/password or facebook
public class SignInFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_SIGN_IN";

    //references for listener and callback manager
    private SignInListener mSignInListener;
    private CallbackManager mCallbackManager;

    //references for edit texts
    private EditText mEmail;
    private EditText mPassword;


    //public constructor
    public SignInFragment() {}


    //newInstance - create and return new instance of fragment
    public static SignInFragment newInstance() {

        //return new fragment
        return new SignInFragment();
    }


    //onAttach - verify implementation of listeners
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify implementation
        if (context instanceof SignInListener) {

            //cast and store
            mSignInListener = (SignInListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_in, container, false);
    }


    //onActivityCreated - set up ui
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available
        if (getView() != null) {

            //get the edit texts
            mEmail = getView().findViewById(R.id.sign_in_email_edit_text);
            mPassword = getView().findViewById(R.id.sign_in_password_edit_text);

            //set focus change listener
            mEmail.setOnFocusChangeListener(mOnFocusChangeListener);
            mPassword.setOnFocusChangeListener(mOnFocusChangeListener);

            //get the buttons
            View forgotPasswordText = getView().findViewById(R.id.sign_in_text_view_forgot_password);
            View signUpButton = getView().findViewById(R.id.sign_in_button_sign_up);
            View signInButton = getView().findViewById(R.id.sign_in_button_sign_in);

            //set click listeners
            forgotPasswordText.setOnClickListener(mForgotPasswordClickedListener);
            signUpButton.setOnClickListener(mSignUpClickedListener);
            signInButton.setOnClickListener(mSignInClickedListener);

            //get login button and set permissions
            LoginButton loginButton = getView().findViewById(R.id.facebook_login_button);
            loginButton.setReadPermissions("email");

            //set fragment for login button
            loginButton.setFragment(this);

            //create and store reference to callback manager
            mCallbackManager = CallbackManager.Factory.create();

            //Callback registration for Facebook button
            loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    //clear email and password input
                    clearInput(true);

                    //verify listener is available
                    if (mSignInListener != null) {

                        //send result to activity
                        mSignInListener.facebookSignedIn(loginResult);
                    }
                }

                @Override
                public void onCancel() {

                    //clear email and password input
                    clearInput(true);
                }

                @Override
                public void onError(FacebookException exception) {
                    exception.printStackTrace();

                    //clear email and password input
                    clearInput(true);

                    //display toast to inform user of difficulty connecting with facebook
                    Toast.makeText(getActivity(), R.string.facebook_error_try_again, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    //onActivityResult - use callback manager to process result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //set result
        mCallbackManager.onActivityResult(requestCode, resultCode, data);

        //call super implementation
        super.onActivityResult(requestCode, resultCode, data);
    }


    //mSignInClickedListener - listener for sign in button click
    private final View.OnClickListener mSignInClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mSignInListener != null) {

                //check if nothing or blank space was entered for email or password
                if (mEmail.getText().toString().trim().equals("") || mEmail.getText().toString().trim().equals("")) {

                    //display toast to inform user must enter credentials
                    Toast.makeText(getActivity(), R.string.email_and_pass_sign_in, Toast.LENGTH_SHORT).show();

                    //if all entered
                } else {

                    //sign in through activity with listener
                    mSignInListener.signInClicked(mEmail.getText().toString().trim(), mPassword.getText().toString().trim());
                }
            }
        }
    };


    //mSignUpClickedListener - listener for sign up clicked
    private final View.OnClickListener mSignUpClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mSignInListener != null) {

                //check if email and password have not been entered
                if (mEmail.getText().toString().trim().equals("") || mEmail.getText().toString().trim().equals("")) {

                    //display toast credentials are required
                    Toast.makeText(getActivity(), R.string.email_and_pass_sign_up, Toast.LENGTH_SHORT).show();

                    //if all have entries
                } else {

                    //send info to activity
                    mSignInListener.signUpClicked(mEmail.getText().toString().trim(), mPassword.getText().toString().trim());
                }
            }
        }
    };


    //mForgotPasswordClickedListener - on click listener for recovering password, verifies email
    // input and passes email to activity to recover password
    private final View.OnClickListener mForgotPasswordClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mSignInListener != null) {

                //check if email was not entered
                if (mEmail.getText().toString().trim().equals("")) {

                    //display toast to inform user an email must be entered to recover the password
                    Toast.makeText(getActivity(), R.string.enter_email_password_recovery, Toast.LENGTH_SHORT).show();

                    //if email entered
                } else {

                    //pass entered email to activity to recover password
                    mSignInListener.forgotPasswordClicked(mEmail.getText().toString().trim());
                }
            }
        }
    };


    //clearInput - clear input fields based on received booleans
    public void clearInput(boolean _clearEmail) {

        //verify EditText have been referenced
        if (mEmail != null & mPassword != null) {

            //check if email should be cleared
            if (_clearEmail) {

                //clear email input
                mEmail.setText("");
            }

            //clear password input
            mPassword.setText("");
        }
    }


    //mOnFocusChangeListener - change listener for edit texts, hides keyboard
    private final View.OnFocusChangeListener mOnFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {

            //get input method manager
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

            //verify manager is available
            if (inputMethodManager != null) {

                //kide the keyboard
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    };
}