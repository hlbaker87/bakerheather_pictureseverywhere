/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;


//SelectRemovalPhoto - class for creating objects to store the information of a photo for
// multiselection for removal from shared folder
public class SelectRemovalPhoto implements Serializable {

    ///member variables
    private final String mStoragePath;
    private Boolean mIsSelected = false;


    //getters
    public Boolean getIsSelected() {
        return mIsSelected;
    }

    public String getStoragePath() {
        return mStoragePath;
    }

    //setters
    public void setIsSelected(Boolean mIsSelected) {
        this.mIsSelected = mIsSelected;
    }


    //public constructor setting received path as member variable
    public SelectRemovalPhoto(String _storagePath) {

        //set path as value of member variable
        this.mStoragePath = _storagePath;
    }

    //public constructor setting empty string as value for storage path member variable
    public SelectRemovalPhoto() {

        //set empty string as value for storage path
        this.mStoragePath = "";
    }


    //getImageReference - creates and returns a StorageReference for the photo
    public StorageReference getImageReference() {

        //retrieve instances of Firebase auth and storage
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        //verify firebase auth and current user are available
        if (firebaseAuth != null && firebaseAuth.getCurrentUser() != null) {

            //return a reference for the current photo
            return firebaseStorage.getReference().child(mStoragePath);
        }

        //return null
        return null;
    }
}