/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;

import android.graphics.Bitmap;


//EffectAppliedListener - interface for applied effects
public interface EffectAppliedListener {

    //method for the bitmap with applied effects as parameter
    void effectApplied(Bitmap _bitmap);
}
