/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.ProfileActivity;
import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.interfaces.ProfileListener;
import com.facebook.AccessToken;


//ProfileFragment - fragment to display user's information and allow change of email and password
// for the account
public class ProfileFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_PROFILE";

    //reference to interface
    private ProfileListener mProfileListener;

    //reference to email TextView
    private TextView mEmailTextView;


    //public constructor
    public ProfileFragment() {}


    //newInstance - create and return a new instance of the fragment
    public static ProfileFragment newInstance(String _email) {

        //create a new bungle and add the email as argument
        Bundle args = new Bundle();
        args.putString(ProfileActivity.EXTRA_EMAIL, _email);

        //create a new instance of the fragment and set the arguments
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);

        //return the fragment
        return fragment;
    }


    //onAttach - verify the activity implements the ProfileListener interface and store reference
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify context implements listener
        if (context instanceof ProfileListener) {

            //cast and store reference to interface
            mProfileListener = (ProfileListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }


    //onActivityCreated - set up user's email in UI and set on click listeners, set up Facebook sign in
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available
        if (getView() != null) {

            //retrieve reference to the text view
            mEmailTextView = getView().findViewById(R.id.profile_email_text_view);

            //retrieve the arguments and verify available
            Bundle args = getArguments();

            if (args != null) {

                //retrieve user's email and set in TextView
                String email = args.getString(ProfileActivity.EXTRA_EMAIL);
                mEmailTextView.setText(email);

                //set click listener for email and password button
                getView().findViewById(R.id.profile_button_change_email).setOnClickListener(mChangeEmailClickedListener);
                Button passwordButton = getView().findViewById(R.id.profile_button_change_password);
                passwordButton.setOnClickListener(mChangePasswordClickedListener);

                //check if user is signed in with facebook by retrieving access token
                AccessToken accessToken = AccessToken.getCurrentAccessToken();

                //check if access token is available
                if (accessToken != null) {

                    //set visiblity of password change button to gone
                    passwordButton.setVisibility(View.GONE);
                }
            }
        }
    }


    //emailChanged - update UI with new email
    public void emailChanged(String _email) {

        //verify email text view has been referenced
        if (mEmailTextView != null) {

            //set email to display in text view
            mEmailTextView.setText(_email);
        }
    }


    //mChangeEmailClickedListener - click listener for email change button
    private final View.OnClickListener mChangeEmailClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mProfileListener != null) {

                //start email change
                mProfileListener.changeEmail();
            }
        }
    };


    //mChangePasswordClickedListener - click listener for password change button
    private final View.OnClickListener mChangePasswordClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mProfileListener != null) {

                //start password change
                mProfileListener.changePassword();
            }
        }
    };
}