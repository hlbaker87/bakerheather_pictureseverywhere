/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.vending.billing.IInAppBillingService;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.fragments.SinglePhotoFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedPhotosAddedListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.MediaStoreUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.MenuUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.PhotoUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.ShareUtils;
import com.facebook.login.LoginManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


//SinglePhotoActivity - displays selected photo in fragment, supports effects mode, sharing,
// and deletion
public class SinglePhotoActivity extends AppCompatActivity implements SharedPhotosAddedListener, PurchasesUpdatedListener, ServiceConnection {


    //reference to Toolbar Menu
    private Menu mToolbarMenu;

    //File for reference to File of currently selected photo
    private File mPhotoFile;

    //boolean to determine if the current photo was opened from a Shared Folder
    private Boolean mIsSharedPhoto = false;

    //string to store title of shared folder
    private String mFolderShareTitle = "";

    //ArrayList for shared folders user is contributor for
    private final ArrayList<SharedFolder> mSharedFolders = new ArrayList<>();

    //references to Firebase objects
    private FirebaseAuth mAuth;

    //boolean to determine if service is bound
    private boolean mIsBound = false;

    //variables for billing with GooglePlay
    private IInAppBillingService mBillingService;
    private BillingClient mBillingClient;
    private boolean mBillingConnected = false;

    //boolean to determine if user is subscribed
    private boolean mUserIsSubscribed = false;


    //onCreate - retrieve photo file from intent, verify location photo was selected from, pass photo
    // to fragment and set fragment for display in activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_photo);

        //create billing client and start connection
        mBillingClient = BillingClient.newBuilder(SinglePhotoActivity.this).setListener(this).build();
        mBillingClient.startConnection(mBillingClientStateListener);

        //retrieve references to database and storage
        mAuth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        //verify database is available and get reference
        if (database != null) {

            DatabaseReference databaseReference = database.getReference();

            //verify reference is available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference to shared folder and add single event listener
                DatabaseReference sharedFoldersReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS);
                sharedFoldersReference.addListenerForSingleValueEvent(mSharedFoldersEventListener);
            }
        }

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device size is small
        if (deviceSize.equals("small")) {

            //set orientation to portrait
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_single_photo);
        setSupportActionBar(myToolbar);

        //retrieve intent
        Intent intent = getIntent();

        //verify intent is available
        if (intent != null) {

            //retrieve extras bundle
            Bundle extras = intent.getExtras();

            //verify extras are available
            if (extras != null) {

                //retrieve File for photo and booleans to determine if the photo was opened from a
                // Shared Folder and if user is subscribed
                mPhotoFile = (File) extras.getSerializable(MainActivity.EXTRA_PHOTO_FILE);
                mIsSharedPhoto = extras.getBoolean(SingleSharedFolderActivity.EXTRA_IS_SHARED_PHOTO, false);
                mUserIsSubscribed = extras.getBoolean(MainActivity.EXTRA_USER_IS_SUBSCRIBED, false);

                //verify File for photo is available
                if (mPhotoFile != null) {

                    //add fragment to view with fragment manager
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_single_photo_fragment_container, SinglePhotoFragment.newInstance(mPhotoFile), SinglePhotoFragment.TAG)
                            .commit();
                }
            }
        }
    }


    //onDestroy - unbind services
    @Override
    protected void onDestroy() {
        super.onDestroy();

        //check if the service is bound
        if (mIsBound) {

            //set boolean to false and unbind
            mIsBound = false;
            unbindService(this);
        }

        //check if the billing service is available
        if (mBillingService != null) {

            //unbind the service and remove the service
            unbindService(mBillingServiceConnection);
            mBillingService = null;
        }
    }


    //onResume - check for subscription and refresh shared folders
    @Override
    protected void onResume() {
        super.onResume();

        //retrieve an instance of the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        //verify database is available and get reference
        if (database != null) {

            DatabaseReference databaseReference = database.getReference();

            //verify reference is available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference to shared folder and add single event listener
                DatabaseReference sharedFoldersReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS);
                sharedFoldersReference.addListenerForSingleValueEvent(mSharedFoldersEventListener);
            }
        }

        //check if the billing client is available
        if (mBillingClient != null) {

            //if the client is not connected
            if (!mBillingConnected) {

                //start the connection
                mBillingClient.startConnection(mBillingClientStateListener);

                //if connected
            } else {

                //query the user's purchase history
                mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                        //set user subscribed to false
                        mUserIsSubscribed = false;

                        //verify the purchase list is available
                        if (purchasesList != null) {

                            //loop over the purchase list
                            for (Purchase purchase : purchasesList) {

                                //check if the SKU is for a month subscription
                                if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                    //set boolean for if the user is subscribed
                                    mUserIsSubscribed = true;
                                }
                            }
                        }
                    }
                });
            }
        }
    }


    //onActivityResult - check if user has purchased a subscription
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check if the request code was to get a subscription
        if (requestCode == MainActivity.RC_BUY_SUBSCRIPTION) {

            //check if the billing client is available
            if (mBillingClient != null) {

                //if the client is not connected
                if (!mBillingConnected) {

                    //start the connection
                    mBillingClient.startConnection(mBillingClientStateListener);

                    //if connected
                } else {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserIsSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserIsSubscribed = true;
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }

        //check if activity was to sign in
        if (requestCode == MainActivity.RC_SIGN_IN) {

            //verify toolbar is available
            if (mToolbarMenu != null) {

                //check if firebase auth and current user are available
                if (mAuth != null && mAuth.getCurrentUser() != null) {

                    //show the sign out option
                    MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_out);

                    //if no current user
                } else {

                    //hide the sign out option
                    MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
                }
            }
        }
    }


    //onCreateOptionsMenu - inflate menu from resources, load fragment, hide delete option if photo
    // was opened from Shared Folder
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //store reference to toolbar menu
        mToolbarMenu = menu;

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_single_photo_activity, menu);

        //check if photo was opened from a SharedFolder
        if (mIsSharedPhoto) {

            //hide the Delete menu item
            MenuUtils.hideMenuItem(menu, R.id.action_delete);
            MenuUtils.hideMenuItem(menu, R.id.action_share_to_folder);
        }

        //verify auth and current user are available
        if (mAuth != null && mAuth.getCurrentUser() != null) {

            //show sign out
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_out);

            //if no user is signed in
        } else {

            //hide sign out
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
        }

        //return true
        return true;
    }


    //onOptionsItemSelected - verify the selection options item and take action accordingly
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the item id
        switch (item.getItemId()) {

            //capture photo
            case R.id.action_capture_photo:

                //set the result of the activity to capture a new photo and finish the activity
                setResult(MainActivity.RESULT_CAPTURE_NEW_PHOTO);
                finish();
                break;

            //save
            case R.id.action_effects_save:

                //save photo with added effects
                saveEffects();
                break;

            //cancel
            case R.id.action_effects_cancel:

                //cancel any added effects
                cancelEffects();
                break;

            //effects
            case R.id.action_effects:

                //start effects mode
                effectsMode();
                break;

            //snapchat
            case R.id.action_snapchat:

                //share currently displayed photo to snapchat
                shareToSnapchat();
                break;

            //instagram
            case R.id.action_instagram:

                //share currently displayed photo to instagram
                shareToInstagram();
                break;

            //twitter
            case R.id.action_twitter:

                //share currently displayed photo to twitter
                shareToTwitter();
                break;

            //facebook send
            case R.id.action_facebook_send:

                //send currently displayed photo on facebook messenger
                sendOnFacebook();
                break;

            //facebook share
            case R.id.action_facebook_share:

                //share currently displayed photo on facebook
                shareToFacebook();
                break;

            //email
            case R.id.action_email:

                //share currently displayed photo to email
                shareToEmail();
                break;

            //mms
            case R.id.action_mms:

                //share currently displayed photo to default MMS app
                shareToMMS();
                break;

            //share to folder
            case R.id.action_share_to_folder:

                //begin selection of the folder for user to add the photo
                shareToFolder();
                break;

            //delete
            case R.id.action_delete:

                //delete the photo with alert for user to select where photo should be deleted from
                deletePhoto();
                break;

            //sign out
            case R.id.action_sign_out:

                //sign out of firebase and facebook
                signOut();
                break;
        }

        //return super implementation
        return super.onOptionsItemSelected(item);
    }


    //startSubscription - create alert to inform user of subscription benefits and allow user to start the subscription process
    private void startSubscription() {

        //create alert
        AlertDialog.Builder subscriptionAlertBuilder = new AlertDialog.Builder(SinglePhotoActivity.this);

        //set title, message, buttons, and prevent cancellation
        subscriptionAlertBuilder.setTitle(R.string.title_start_subscription);
        subscriptionAlertBuilder.setCancelable(false);
        subscriptionAlertBuilder.setMessage(R.string.message_start_subscription);

        //set positive button to start the subscription process
        subscriptionAlertBuilder.setPositiveButton(R.string.subscribe, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //verify billing client is available
                if (mBillingClient != null) {

                    //try
                    try {

                        //create BillingFlowParams for a monthly subscription
                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                .setSku(MainActivity.SUBSCRIPTION_SKU)
                                .setType(BillingClient.SkuType.SUBS)
                                .build();

                        //set the request code and launch the subscription process
                        MainActivity.RC_BUY_SUBSCRIPTION = mBillingClient.launchBillingFlow(SinglePhotoActivity.this, billingFlowParams);

                        //catch any exceptions and print the stack trace
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //set the negative button to cancel and complete no action
        subscriptionAlertBuilder.setNegativeButton(getString(R.string.maybe_later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //display the alert
        subscriptionAlertBuilder.create().show();
    }


    //effectsMode - show and hide UI elements place activity into effects mode and allow user to
    // apply effects to the currently selected photo
    private void effectsMode() {

        //verify toolbar has been properly referenced
        if (mToolbarMenu != null) {

            //hide SinglePhoto menu and show effects menu options
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_single_more);
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_effects_save);
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_effects_cancel);

            //retrieve fragment
            SinglePhotoFragment singlePhotoFragment = (SinglePhotoFragment) getFragmentManager().findFragmentByTag(SinglePhotoFragment.TAG);

            //verify fragment is available
            if (singlePhotoFragment != null) {

                //notify fragment to display effects UI elements
                singlePhotoFragment.effectsMode();
            }
        }
    }


    //cancelEffects - show and hide UI elements return from effects mode and cancel all applied effects
    private void cancelEffects() {

        //verify toolbar has been properly referenced
        if (mToolbarMenu != null) {

            //hide effects menu and show SinglePhoto menu options
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_single_more);
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_effects_save);
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_effects_cancel);

            //retrieve fragment
            SinglePhotoFragment singlePhotoFragment = (SinglePhotoFragment) getFragmentManager().findFragmentByTag(SinglePhotoFragment.TAG);

            //verify fragment is available
            if (singlePhotoFragment != null) {

                //notify fragment effects have been canceled
                singlePhotoFragment.cancelEffects();
            }
        }
    }


    //saveEffects - show and hide UI elements return from effects mode and save photo with applied effects
    private void saveEffects() {

        //verify toolbar has been properly referenced
        if (mToolbarMenu != null) {

            //hide effects menu and show SinglePhoto menu options
            MenuUtils.showMenuItem(mToolbarMenu, R.id.action_single_more);
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_effects_save);
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_effects_cancel);

            //retrieve fragment
            SinglePhotoFragment singlePhotoFragment = (SinglePhotoFragment) getFragmentManager().findFragmentByTag(SinglePhotoFragment.TAG);

            //verify fragment is available
            if (singlePhotoFragment != null) {

                //retrieve temp file with applied effects
                File tempFile = singlePhotoFragment.mTempEffectFile;

                //verify temp photo has been saved
                if (tempFile != null) {

                    mPhotoFile = tempFile;

                    //show toast to notify user effects have been saved
                    Toast.makeText(SinglePhotoActivity.this, R.string.effects_saved, Toast.LENGTH_SHORT).show();

                    //store photo with effects to permanent storage in device media store
                    MediaStoreUtils.storeMediaStorePhoto(SinglePhotoActivity.this, tempFile, null);

                    //create intent and send broadcast to notify galleries of the new photo
                    Intent photoAddedBroadcastIntent = new Intent(MainActivity.MEDIA_STORE_DATA_CHANGED);
                    sendBroadcast(photoAddedBroadcastIntent);

                    //if no effects have been applied
                } else {

                    //show toast to notify user no effects have been added to the photo
                    Toast.makeText(SinglePhotoActivity.this, R.string.no_effects_added, Toast.LENGTH_SHORT).show();
                }

                //notify fragment effects have been saved
                singlePhotoFragment.saveEffects();
            }
        }
    }


    //deletePhoto - delete the currently displayed photo from the device
    private void deletePhoto() {

        //create dialog builder to selecting location to delete from
        AlertDialog.Builder deletePhotoAlertDialogBuilder = new AlertDialog.Builder(SinglePhotoActivity.this);

        //set the title and message for the alert
        deletePhotoAlertDialogBuilder.setTitle(R.string.delete_photo);
        deletePhotoAlertDialogBuilder.setMessage(R.string.message_delete_photo);

        //set the neural button to delete from the device
        deletePhotoAlertDialogBuilder.setNeutralButton(R.string.device, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //create and display toast to inform user the photo has been deleted from the device
                Toast.makeText(SinglePhotoActivity.this, R.string.photo_deleted_from_device, Toast.LENGTH_SHORT).show();

                //delete the photo from the media store
                MediaStoreUtils.deleteMediaStorePhoto(SinglePhotoActivity.this, mPhotoFile);

                //create intent and send broadcast to notify galleries of the deleted photo
                Intent photoDeletedBroadcastIntent = new Intent(MainActivity.MEDIA_STORE_DATA_CHANGED);
                sendBroadcast(photoDeletedBroadcastIntent);

                setResult(CalendarDateFolderActivity.RESULT_PHOTO_DELETED);
                //finish the activity
                finish();
            }
        });

        //set the negative button to delete from backup
        deletePhotoAlertDialogBuilder.setNegativeButton(getString(R.string.backup), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //check if the user is subscribed
                if (mUserIsSubscribed) {

                    //verify current user is available
                    if (mAuth != null && mAuth.getCurrentUser() != null) {

                        //display a toast to inform user the photo has been deleted from backup
                        Toast.makeText(SinglePhotoActivity.this, R.string.deleted_from_backup, Toast.LENGTH_SHORT).show();

                        //delete the photo from backup
                        PhotoUtils.deletePhotoFromBackup(mPhotoFile);

                        //if no current user is available
                    } else {

                        //start sign in activity
                        signIn();
                    }

                    //if the user is not subscribed
                } else {

                    //start the subscription process
                    startSubscription();
                }
            }
        });

        //set the positive button to delete from all storage
        deletePhotoAlertDialogBuilder.setPositiveButton(R.string.delete_all, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //check if the user is subscribed
                if (mUserIsSubscribed) {

                    //verify current user is available
                    if (mAuth != null && mAuth.getCurrentUser() != null) {

                        //display toast to inform the user the photo has been deleted from device and backup
                        Toast.makeText(SinglePhotoActivity.this, R.string.deleted_from_all, Toast.LENGTH_SHORT).show();

                        //delete the photo from backup
                        PhotoUtils.deletePhotoFromBackup(mPhotoFile);

                        //delete the photo from the media store
                        MediaStoreUtils.deleteMediaStorePhoto(SinglePhotoActivity.this, mPhotoFile);

                        //create intent and send broadcast to notify galleries of the deleted photo
                        Intent photoDeletedBroadcastIntent = new Intent(MainActivity.MEDIA_STORE_DATA_CHANGED);
                        sendBroadcast(photoDeletedBroadcastIntent);

                        //set the result as photo deleted
                        setResult(CalendarDateFolderActivity.RESULT_PHOTO_DELETED);

                        //finish the activity
                        finish();

                        //if no current user is available
                    } else {

                        //start sign in activity
                        signIn();
                    }

                    //if the user is not subscribed
                } else {

                    //start the subscription process
                    startSubscription();
                }
            }
        });

        //create and show the alert
        deletePhotoAlertDialogBuilder.create().show();
    }


    //getSharingIntent - create and return intent to share photo currently displayed to user
    private Intent getSharingIntent() {

        //retrieve fragment
        SinglePhotoFragment singlePhotoFragment = (SinglePhotoFragment) getFragmentManager()
                .findFragmentByTag(SinglePhotoFragment.TAG);

        //verify fragment is available
        if (singlePhotoFragment != null) {

            //retrieve photo bitmap from fragment
            Bitmap photo = singlePhotoFragment.mPhoto;

            //verify photo is available
            if (photo != null) {

                //create sharing Uri for photo
                Uri photoUri = ShareUtils.getSharingUri(SinglePhotoActivity.this, photo);

                //verify photo Uri creation was successful
                if (photoUri != null) {

                    //create ACTION_SEND intent, set type and flags, and add stream extra for photo
                    Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                    sharingIntent.setType("image/*");
                    sharingIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    sharingIntent.putExtra(Intent.EXTRA_STREAM, photoUri);

                    //return the intent
                    return sharingIntent;
                }
            }
        }

        //return null
        return null;
    }


    //shareToSnapchat - create and configure intent, verify Snapchat is installed on the device,
    // and share the photo to Snapchat
    private void shareToSnapchat() {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //set package for sharing
            sharingIntent.setPackage("com.snapchat.android");

            //retrieve package manager
            PackageManager packageManager = getPackageManager();

            //verify Snapchat is installed
            if (sharingIntent.resolveActivity(packageManager) != null) {

                //share to Snapchat
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_snapchat)));

                //if Snapchat is not installed
            } else {

                //display toast to inform user Snapchat must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_snapchat, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //shareToInstagram - create and configure intent, verify Instagram is installed on the device,
    // and share the photo to Instagram
    private void shareToInstagram() {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //set package for sharing
            sharingIntent.setPackage("com.instagram.android");

            //retrieve package manager
            PackageManager packageManager = getPackageManager();

            //verify Instagram is installed
            if (sharingIntent.resolveActivity(packageManager) != null) {

                //share to Instagram
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_instagram)));

                //if Instagram is not installed
            } else {

                //display toast to inform user Instagram must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_instagram, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //shareToTwitter - create and configure intent, verify Twitter is installed on the device,
    // and share the photo to Twitter
    private void shareToTwitter() {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //set package for sharing
            sharingIntent.setPackage("com.twitter.android");

            //retrieve package manager
            PackageManager packageManager = getPackageManager();

            //verify Twitter is installed
            if (sharingIntent.resolveActivity(packageManager) != null) {

                //share to Twitter
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_twitter)));

                //if Twitter is not installed
            } else {

                //display toast to inform user Twitter must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_twitter, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //shareToEmail - create and configure intent, verify Gmail is installed on the device,
    // and share the photo to Gmail
    private void shareToEmail() {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //set package for sharing
            sharingIntent.setPackage("com.google.android.gm");

            //retrieve package manager
            PackageManager packageManager = getPackageManager();

            //verify Gmail is installed
            if (sharingIntent.resolveActivity(packageManager) != null) {

                //share to Gmail
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_email)));

                //if Gmail is not installed
            } else {

                //display toast to inform user Gmail must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_gmail, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //shareToMMS - create and configure intent, verify a default MMS application is installed on the
    // device
    private void shareToMMS() {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //retrieve package name for the messaging app set as default by the user
            String defaultSmsApp = Telephony.Sms.getDefaultSmsPackage(SinglePhotoActivity.this);

            //verify package name was retrieved
            if (!defaultSmsApp.equals("")) {

                //set the package name for the intent
                sharingIntent.setPackage(defaultSmsApp);

                //retrieve package manager
                PackageManager packageManager = getPackageManager();

                //verify a mms app is installed
                if (sharingIntent.resolveActivity(packageManager) != null) {

                    //share to mms
                    startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_mms)));

                    //if a mms app is not installed
                } else {

                    //display toast to inform user a mms app must be installed to share this way
                    Toast.makeText(SinglePhotoActivity.this, R.string.install_mms, Toast.LENGTH_SHORT).show();
                }

                //if no default messaging app is installed
            } else {

                //display toast to inform user a mms app must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_mms, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //sendOnFacebook - create and configure intent, verify Facebook Messenger is installed on the device,
    // and share the photo to Facebook Messenger
    private void sendOnFacebook () {

        //create intent to share photo
        Intent sharingIntent = getSharingIntent();

        //verify sharing intent is available
        if (sharingIntent != null) {

            //set package for sharing
            sharingIntent.setPackage("com.facebook.orca");

            //retrieve package manager
            PackageManager packageManager = getPackageManager();

            //verify Facebook Messenger is installed
            if (sharingIntent.resolveActivity(packageManager) != null) {

                //share to Facebook Messenger
                startActivity(Intent.createChooser(sharingIntent, getString(R.string.send_facebook)));

                //if Facebook Messenger is not installed
            } else {

                //display toast to inform user Facebook Messenger must be installed to share this way
                Toast.makeText(SinglePhotoActivity.this, R.string.install_facebook_messenger, Toast.LENGTH_SHORT).show();
            }
        }
    }


    //shareToFacebook - verify Facebook is installed on the device, create SharePhoto and ShareContent,
    // and show a share dialog for user to share the photo to Facebook
    private void shareToFacebook() {

        //set boolean to determine if Facebook is installed
        boolean isFacebookInstalled = false;
        PackageManager packageManager = getPackageManager();

        //try
        try {

            //attempt to retrieve PackageInfo for Facebook
            PackageInfo packageInfo = packageManager.getPackageInfo("com.facebook.katana",
                    PackageManager.GET_ACTIVITIES);

            //verify PackageInfo was retrieved
            if (packageInfo != null) {

                //set boolean to true
                isFacebookInstalled = true;
            }

            //catch NameNotFound exceptions and print the stack trace
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //if Facebook is installed
        if (isFacebookInstalled) {

            //retrieve fragment
            SinglePhotoFragment singlePhotoFragment = (SinglePhotoFragment) getFragmentManager()
                    .findFragmentByTag(SinglePhotoFragment.TAG);

            //verify fragment is available
            if (singlePhotoFragment != null) {

                //retrieve photo bitmap from fragment
                Bitmap photo = singlePhotoFragment.mPhoto;

                //verify photo is available
                if (photo != null) {

                    //create SharePhoto
                    SharePhoto sharePhoto = new SharePhoto.Builder()
                            .setBitmap(photo)
                            .build();

                    //create SharePhotoContent for SharePhoto
                    SharePhotoContent content = new SharePhotoContent.Builder()
                            .addPhoto(sharePhoto)
                            .build();

                    //show a ShareDialog for the photo
                    ShareDialog.show(SinglePhotoActivity.this, content);
                }
            }

            //if Facebook is not installed
        } else {

            //display toast to inform user Facebook must be installed to share this way
            Toast.makeText(SinglePhotoActivity.this, R.string.install_facebook, Toast.LENGTH_SHORT).show();
        }
    }


    //shareToFolder - display titles of shared folders in spinner within alert for selection by user
    private void shareToFolder() {

        //verify firebase auth is available
        if (mAuth != null) {

            //verify a current user is signed in
            if (mAuth.getCurrentUser() != null) {

                //verify the user has access to shared folders
                if (mSharedFolders.size() > 0) {

                    //create ArrayList for titles of folders
                    final ArrayList<String> folderTitles = new ArrayList<>();

                    //loop over folders user has access to
                    for (SharedFolder folder : mSharedFolders) {

                        //add the title of the folder to the ArrayList
                        folderTitles.add(folder.getTitle());
                    }

                    //create alert builder to share photo to folder
                    AlertDialog.Builder shareToFolderAlertBuilder = new AlertDialog.Builder(SinglePhotoActivity.this);

                    //set alert title
                    shareToFolderAlertBuilder.setTitle(R.string.share_to_folder);

                    //retrieve and inflate layout from resources and set to view of alert
                    @SuppressLint("InflateParams")
                    View shareToFolderLayout = SinglePhotoActivity.this.getLayoutInflater().inflate(R.layout.alert_share_to_folder, null);
                    shareToFolderAlertBuilder.setView(shareToFolderLayout);

                    //retrieve spinner from layout
                    final Spinner shareSpinner = shareToFolderLayout.findViewById(R.id.select_folder_spinner);

                    //set temporary list to be replaced by Firebase data as data model for adapter
                    shareSpinner.setAdapter(new ArrayAdapter<>(SinglePhotoActivity.this, android.R.layout.simple_list_item_1, folderTitles));

                    //cancel button
                    shareToFolderAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });

                    //share button
                    shareToFolderAlertBuilder.setPositiveButton(R.string.share, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            //retrieve selected title
                            int selected = shareSpinner.getSelectedItemPosition();
                            SharedFolder folder = mSharedFolders.get(selected);
                            PhotoUtils.storeByteArrayForFolderPhoto(folder, mPhotoFile, mAuth.getCurrentUser().getUid(), SinglePhotoActivity.this);

                            //display toast to inform user they do not have access to any shared folders
                            Toast.makeText(SinglePhotoActivity.this, getString(R.string.photo_shared_to_prefix) + " " + folder.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    });

                    //build and show alert
                    shareToFolderAlertBuilder.create().show();

                    //if user does not have access to any shared folders
                } else {

                    //display toast to inform user they do not have access to any shared folders
                    Toast.makeText(SinglePhotoActivity.this, R.string.no_shared_folder_access, Toast.LENGTH_SHORT).show();
                }

                //if user is not signed in
            } else {

                //start sign in process
                signIn();
            }
        }
    }


    //signIn - start sign in activity if internet is available
    private void signIn() {

        //check for internet
        if (InternetUtils.getIsNetworkAvailable(SinglePhotoActivity.this)) {

            //create intent and start sign in activity
            Intent signInIntent = new Intent(SinglePhotoActivity.this, SignInActivity.class);
            startActivityForResult(signInIntent, MainActivity.RC_SIGN_IN);

            //if no internet
        } else {

            //display toast to inform user internet is required for sign in
            Toast.makeText(SinglePhotoActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
        }
    }


    //signOut - sign out user and display menu items for signed out user
    private void signOut() {

        //retrieve login manager for facebook
        LoginManager loginManager = LoginManager.getInstance();

        //verify firebase auth and facebook auth are available
        if (mAuth != null && loginManager != null) {

            //sign out of Firebase and log out of Facebook
            mAuth.signOut();
            loginManager.logOut();
        }

        //verify toolbar menu is available
        if (mToolbarMenu != null) {

            //hide and show menu items for signed out user
            MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
        }

        //display toast to inform user they have signed out
        Toast.makeText(SinglePhotoActivity.this, getString(R.string.signed_out), Toast.LENGTH_SHORT).show();
    }


    //photoAdded - interface callback for when photo is added to a shared folder
    @Override
    public void photoAdded() {

        //verify the title of the folder is available
        if (!mFolderShareTitle.equals("")) {

            //display toast to inform user
            Toast.makeText(SinglePhotoActivity.this, getString(R.string.photo_added_shared_prefix) + " " + mFolderShareTitle + " " + getString(R.string.photo_added_shared_suffix), Toast.LENGTH_SHORT).show();

            //clear the title of selected folder
            mFolderShareTitle = "";
        }
    }


    //mSharedFoldersEventListener - listener for shared folders to find folders user has access to
    private final ValueEventListener mSharedFoldersEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            //verify snapshot is available
            if (dataSnapshot != null) {

                //clear the list of shared folders
                mSharedFolders.clear();

                //verify auth and current user is available
                if ( mAuth != null && mAuth.getCurrentUser() != null) {

                    //verify snapshot children are available
                    if (dataSnapshot.getChildren() != null) {

                        //loop over the shared folders
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                            //retrieve the id of the folder
                            String folderId = snapshot.getKey();

                            /*
                            * Warning suppressed due to intentional and tested implementation.
                            * */
                            @SuppressWarnings("unchecked")
                            HashMap<String, Object> folderMap = (HashMap<String, Object>) snapshot.getValue();

                            //verify the map for the current loop index folder is available
                            if (folderMap != null) {

                                //retrieve the owner and title of folder
                                String owner = (String) folderMap.get(SharedFoldersActivity.KEY_OWNER);
                                String title = (String) folderMap.get(SharedFoldersActivity.KEY_TITLE);

                                /*
                                * Warning suppressed due to intentional and tested implementation.
                                * */
                                @SuppressWarnings("unchecked")
                                HashMap<String, String> contributorMap = (HashMap<String, String>) folderMap.get(SharedFoldersActivity.KEY_CONTRIBUTORS);

                                //create boolean to determine if the current user is a contributor
                                boolean userIsContributor = false;

                                //verify the map of contributors is available
                                if (contributorMap != null) {

                                    //array list for contributors
                                    ArrayList<String> contributors = new ArrayList<>();

                                    //loop over the contributors in the folder
                                    for (String key: contributorMap.keySet()) {

                                        //add the contributor to the list
                                        contributors.add(contributorMap.get(key));

                                        //check if the contributor is the current user
                                        if (contributorMap.get(key).equals(mAuth.getCurrentUser().getUid())) {

                                            //set boolean to true
                                            userIsContributor = true;
                                        }
                                    }

                                    //check if the current user is a contributor
                                    if (userIsContributor) {

                                        /*
                                        * Warning suppressed due to intentional and tested implementation.
                                        * */
                                        @SuppressWarnings("unchecked")
                                        HashMap<String, String> photoPathMap = (HashMap<String, String>) folderMap.get(SharedFoldersActivity.KEY_PHOTOS);

                                        //create ArrayList for the paths of the photos
                                        ArrayList<String> photoPaths = new ArrayList<>();

                                        //verify the map of photos paths is available
                                        if (photoPathMap != null) {

                                            //loop over the photo paths
                                            for (String photoKey : photoPathMap.keySet()) {

                                                //add the path to the list
                                                photoPaths.add(photoPathMap.get(photoKey));
                                            }
                                        }

                                        //create a new shared folder
                                        SharedFolder sharedFolder = new SharedFolder(folderId, owner, title, photoPaths);

                                        //loop over the contributors and add them to the folder
                                        for (String contributor: contributors) {

                                            sharedFolder.addContributorID(contributor);
                                        }

                                        //add the folder to the list of folders
                                        mSharedFolders.add(sharedFolder);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //required override
        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };


    //mBillingClientStateListener - state listener for billing client
    private final BillingClientStateListener mBillingClientStateListener = new BillingClientStateListener() {

        //onBillingSetupFinished - called when billing setup is finished - gets the purchase history
        @Override
        public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
            if (billingResponseCode == BillingClient.BillingResponse.OK) {

                //set billing boolean to true
                mBillingConnected = true;

                //verify the client is available
                if (mBillingClient != null) {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserIsSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserIsSubscribed = true;
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }

        //onBillingServiceDisconnected - called when billing is disconnected
        @Override
        public void onBillingServiceDisconnected() {

            //set connected boolean to false
            mBillingConnected = false;
        }
    };


    //required override
    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

    }


    //onServiceConnected - called when the service is connected
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        //set bound boolean to true
        mIsBound = true;
    }


    //onServiceDisconnected - called when the service is disconnected
    @Override
    public void onServiceDisconnected(ComponentName componentName) {

        //set bound boolean to false
        mIsBound = false;
    }


    //mBillingServiceConnection - service connection for in app subscriptions
    private final ServiceConnection mBillingServiceConnection = new ServiceConnection() {

        //onServiceDisconnected - called when the service is disconnected
        @Override
        public void onServiceDisconnected(ComponentName name) {

            //set billing service to null
            mBillingService = null;
        }

        //onServiceConnected - called when the service is connected
        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {

            //store the billing service
            mBillingService = IInAppBillingService.Stub.asInterface(service);
        }
    };
}