/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import java.util.Date;


//CalendarDateListener - interface for calendar date photos
public interface CalendarDateListener {

    //methods for reloading the calendar photos and opening activity for photos on a clicked date
    void getCalendarPhotos();
    void calendarDateClicked(Date _clickedDate);
}
