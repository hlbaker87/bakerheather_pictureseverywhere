/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;


//SharedFoldersAdapter - adapter for displaying grid of shared folders
public class SharedFoldersAdapter extends BaseAdapter {


    //int for creating item ids
    private static final int HEX_ID_CONSTANT = 0x00111;

    //data model
    private final ArrayList<SharedFolder> mSharedFolders;

    //context reference
    private final Context mContext;


    //public constructor setting data model and context references
    public SharedFoldersAdapter(Context _context, ArrayList<SharedFolder> _sharedFolders) {

        //set context and data model references
        this.mContext = _context;
        this.mSharedFolders = _sharedFolders;
    }


    //getCount - return the count of data model items
    @Override
    public int getCount() {

        //verify data model is available
        if (mSharedFolders != null) {

            //return the size of the data model
            return mSharedFolders.size();

            //if data model is not available
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the current position of the adapter
    @Override
    public SharedFolder getItem(int position) {

        //verify the current position is within the bounds of the data model
        if (mSharedFolders != null && position >= 0 && position < mSharedFolders.size()) {

            //return the item at the current position
            return mSharedFolders.get(position);

            //if not within bounds
        } else {

            //return a new SharedFolder
            return new SharedFolder("ID", "OWNER", "TITLE", new ArrayList<String>());
        }
    }


    //getItemId - create and return id for the current item
    @Override
    public long getItemId(int position) {

        //verify position is within the bounds of the data model
        if (mSharedFolders != null && position >= 0 && position < mSharedFolders.size()) {

            //return sum of constant and position
            return HEX_ID_CONSTANT + position;

            //if item is not within the bounds
        } else {

            //return 0
            return 0;
        }
    }


    //getView - configure and return view for the current position
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //reference for view holder
        SharedFolderViewHolder sharedFolderViewHolder;

        //check if convertView is not available
        if(convertView == null) {

            //inflate and store view
            convertView = LayoutInflater.from(mContext).inflate(R.layout.shared_folder_grid_item, parent, false);

            //create new view holder and set as tag
            sharedFolderViewHolder = new SharedFolderViewHolder(convertView);
            convertView.setTag(sharedFolderViewHolder);

            //if convertView is available
        } else {

            //retrieve the view holder
            sharedFolderViewHolder = (SharedFolderViewHolder) convertView.getTag();
        }

        //create final reference to view holder
        final SharedFolderViewHolder photosHolder = sharedFolderViewHolder;

        //retrieve the shared folder for the position
        SharedFolder folderAtPosition = getItem(position);

        //set the image and empty folder image
        sharedFolderViewHolder.mFolderPhotoImageView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.no_pictures));

        //if folder holds a photo
        if (folderAtPosition.getPhotoPaths().size() > 0) {

            //create OnSuccessListener
            OnSuccessListener onSuccessListener = new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {

                    //try
                    try {

                        //load the image into the image view with glide
                        Glide.with(mContext).load(uri).into(photosHolder.mFolderPhotoImageView);

                        //catch any exceptions and print the stack trace
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            //get the download url for the photo
            /*
            * Warning suppressed due to intentional and tested implementation.
            * */
            //noinspection unchecked
            folderAtPosition.getCoverPhotoStorageReference().getDownloadUrl().addOnSuccessListener(onSuccessListener);
        }

        //set the title and number of photos for the folder
        sharedFolderViewHolder.mFolderTitleTextView.setText(folderAtPosition.getTitle());
        sharedFolderViewHolder.mNumPhotosTextView.setText(String.valueOf(folderAtPosition.getPhotoPaths().size()));

        //return the configured view
        return convertView;
    }


    //SharedFolderViewHolder - view holder class for SharedFoldersAdapter
    private static class SharedFolderViewHolder {

        //references for image and text views
        final SquareGridImageView mFolderPhotoImageView;
        final TextView mFolderTitleTextView;
        final TextView mNumPhotosTextView;

        //constructor
        SharedFolderViewHolder(View _view) {

            //retrieve views and store references
            this.mFolderPhotoImageView = _view.findViewById(R.id.shared_folders_grid_folder_image);
            this.mFolderTitleTextView = _view.findViewById(R.id.shared_folders_grid_folder_title);
            this.mNumPhotosTextView = _view.findViewById(R.id.shared_folders_grid_folder_number_photos);
        }
    }
}