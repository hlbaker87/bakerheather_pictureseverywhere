/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.SelectionGridAdapter;
import com.bakerheather.hb.pictureseverywhere.utilities.MediaStoreUtils;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectionPhoto;
import com.bakerheather.hb.pictureseverywhere.interfaces.SelectPhotosListener;

import java.io.File;
import java.util.ArrayList;


//SelectPhotosFragment - fragment to allow multi selection from all device photos to add a photo to
// a shared folder
public class SelectPhotosFragment extends Fragment {

    //fragment tag
    public static final String TAG = "FRAGMENT_SELECT_PHOTOS";

    //reference to interface
    private SelectPhotosListener mSelectPhotosListener;

    //data model
    private final ArrayList<SelectionPhoto> mSelectionPhotos = new ArrayList<>();

    //reference to adapter
    private SelectionGridAdapter mSelectionGridAdapter;

    //reference to progress bar
    private ProgressBar mProgressBar;

    //public constructor
    public SelectPhotosFragment() {}


    //newInstance - create and return a new instance of the fragment
    public static SelectPhotosFragment newInstance() {

        //return new instance of the fragment
        return new SelectPhotosFragment();
    }


    //onAttach - verify implementation of required interface and store reference
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify interface is implemented
        if (context instanceof SelectPhotosListener) {

            //cast and store reference
            mSelectPhotosListener = (SelectPhotosListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_photos, container, false);
    }


    //onActivityCreated - set up the ui
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify the view is available
        if (getView() != null) {

            //get reference to progress bar
            mProgressBar = getView().findViewById(R.id.select_photos_progress_bar);

            //get the grid and set empty view
            GridView photoGrid = getView().findViewById(R.id.select_photos_grid_view);
            photoGrid.setEmptyView(getView().findViewById(R.id.fragment_select_photos_empty_list_state));

            //set up data model
            mSelectionPhotos.addAll(MediaStoreUtils.getAllPhotosForMultiselect(getActivity()));

            //create and set new adapter, set click listener
            mSelectionGridAdapter = new SelectionGridAdapter(getActivity(), mSelectionPhotos);
            photoGrid.setAdapter(mSelectionGridAdapter);
            photoGrid.setOnItemClickListener(mGridViewItemClickedListener);

            //get the done and cancel image views and set click listener
            ImageView done = getView().findViewById(R.id.select_photos_done);
            ImageView cancel = getView().findViewById(R.id.select_photos_cancel);
            done.setOnClickListener(mDoneClickedListener);
            cancel.setOnClickListener(mCancelClickedListener);
        }
    }


    //mGridViewItemClickedListener - click listener for grid items
    private final GridView.OnItemClickListener mGridViewItemClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify interface is available
            if (mSelectPhotosListener != null) {

                //verify done has not yet been clicked
                if (!mSelectPhotosListener.getDoneClicked()) {

                    //get the photo at the clicked position
                    SelectionPhoto clickedPhoto = mSelectionPhotos.get(position);

                    //set photo as selected
                    clickedPhoto.setIsSelected(!clickedPhoto.getIsSelected());

                    //verify adapter is available
                    if (mSelectionGridAdapter != null) {

                        //notify the adapter data set changed
                        mSelectionGridAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };


    //mCancelClickedListener - listener for cancel click
    private final View.OnClickListener mCancelClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify interface is available
            if (mSelectPhotosListener != null) {

                //cancel selection
                mSelectPhotosListener.cancelClicked();
            }
        }
    };


    //mDoneClickedListener - listener for done click
    private final View.OnClickListener mDoneClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //get array list for selected photos
            ArrayList<File> selectedPhotos = new ArrayList<>();

            //loop over photos
            for (SelectionPhoto selectionPhoto : mSelectionPhotos) {

                //check if photo is selected
                if (selectionPhoto.getIsSelected()) {

                    //add photo to selected photos
                    selectedPhotos.add(selectionPhoto.getImageFile());
                }
            }

            //verify listener is available
            if (mSelectPhotosListener != null) {

                //store the photos
                mSelectPhotosListener.doneClicked(selectedPhotos);
            }
        }
    };


    //showProgressBar - show and animate the progress bar
    public void showProgressBar() {

        //verify the progress bar is available
        if (mProgressBar != null) {

            //make visible and animate the progress bar
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.animate();
        }
    }


    //hideProgressBar - hide the progress bar
    public void hideProgressBar() {

        //verify bar is available
        if (mProgressBar != null) {

            //set visibility to gone
            mProgressBar.setVisibility(View.GONE);
        }
    }
}