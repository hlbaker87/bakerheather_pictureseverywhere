/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectRemovalPhoto;
import com.bakerheather.hb.pictureseverywhere.views.SquareGridImageView;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.concurrent.RejectedExecutionException;

//SelectRemovalGridAdapter - adapter for use in displaying database photos for multi selection
public class SelectRemovalGridAdapter extends BaseAdapter {

    //int for creating item ids
    private static final int HEX_ID_CONSTANT = 0x0011;

    //array list for data model
    private final ArrayList<SelectRemovalPhoto> mSelectRemovalPhotos;

    //reference for context
    private final Context mContext;


    //public constructor sets data model and context references
    public SelectRemovalGridAdapter(Context _context, ArrayList<SelectRemovalPhoto> _selectRemovalPhotos) {

        //store reference to context and data model
        this.mContext = _context;
        this.mSelectRemovalPhotos = _selectRemovalPhotos;
    }


    //getCount - return count of data model items
    @Override
    public int getCount() {

        //verify data model is available
        if (mSelectRemovalPhotos != null) {

            //return the size of the data model
            return mSelectRemovalPhotos.size();

            //if data model is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the adapters current position
    @Override
    public SelectRemovalPhoto getItem(int position) {

        //verify the position is within bounds of data model
        if (mSelectRemovalPhotos != null && position >= 0 && position < mSelectRemovalPhotos.size()) {

            //return the item for the current position
            return mSelectRemovalPhotos.get(position);

            //if item is not within bounds
        } else {

            //return new SelectRemovalPhoto
            return new SelectRemovalPhoto();
        }
    }


    //getItemId - return id for the current item
    @Override
    public long getItemId(int position) {

        //verify position is within data model bounds
        if (mSelectRemovalPhotos != null && position >= 0 && position < mSelectRemovalPhotos.size()) {

            //return sum of id constant and position
            return HEX_ID_CONSTANT + position;

            //if not within bounds
        } else {

            //return 0
            return 0;
        }
    }


    //getView - return configured view for the current item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //reference for the view holder
        SelectionPhotoViewHolder selectionPhotoViewHolder;

        //check if the convertView is unavailable
        if(convertView == null) {

            //inflate and store view
            convertView = LayoutInflater.from(mContext).inflate(R.layout.selection_grid_image_item, parent, false);

            //create new view holder and set as the tag
            selectionPhotoViewHolder = new SelectionPhotoViewHolder(convertView);
            convertView.setTag(selectionPhotoViewHolder);

            //if convertView is available
        } else {

            //retrieve the view holder
            selectionPhotoViewHolder = (SelectionPhotoViewHolder) convertView.getTag();
        }

        //get the item for the current position
        SelectRemovalPhoto photoAtPosition = getItem(position);

        //store final reference to view holder
        final SelectionPhotoViewHolder photoHolder = selectionPhotoViewHolder;

        //create OnSuccessListener
        OnSuccessListener onSuccessListener = new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {

                //try
                try {

                    //load the photo into the image view with glide
                    Glide.with(mContext).load(uri).into(photoHolder.mSelectionImageView);

                    //catch exceptions and print the stack trace
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        //try
        try {

            //get the download url for the image
            /*
            * Warning suppressed due to intentional and tested implementation.
            * */
            //noinspection unchecked
            photoAtPosition.getImageReference().getDownloadUrl().addOnSuccessListener(onSuccessListener);

        //catch any RejectedExecutionException and print the stack trace
        } catch (RejectedExecutionException e) {
            e.printStackTrace();
        }

        //check if the photo is selected
        if (photoAtPosition.getIsSelected()) {

            //set the visibility of the overlay to visible
            selectionPhotoViewHolder.mIsSelectedOverlay.setVisibility(View.VISIBLE);

            //if photo is not selected
        } else {

            //set the overlay to invisible
            selectionPhotoViewHolder.mIsSelectedOverlay.setVisibility(View.INVISIBLE);
        }

        //return the configured view
        return convertView;
    }


    //SelectionPhotoViewHolder - view holder for SelectRemovalGridAdapter
    private static class SelectionPhotoViewHolder {

        //references for SquareGridImageView and overlay layout
        final SquareGridImageView mSelectionImageView;
        final FrameLayout mIsSelectedOverlay;

        //constructor
        SelectionPhotoViewHolder(View _view) {

            //retrieve and store references to views
            this.mSelectionImageView = _view.findViewById(R.id.selection_image_view_grid_item);
            this.mIsSelectedOverlay = _view.findViewById(R.id.selection_grid_item_overlay);
        }
    }
}