/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectRemovalPhoto;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.fragments.SelectRemovalPhotosFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.SelectRemovalPhotosListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;


//SelectRemovalPhotosActivity - displays all photos in a shared folder, allows for multi select and
// removal of photos from the folder
public class SelectRemovalPhotosActivity extends AppCompatActivity implements SelectRemovalPhotosListener {


    //ArrayList for data model
    private final ArrayList<SelectRemovalPhoto> mFolderPhotos = new ArrayList<>();

    //reference to the current shared folder
    private SharedFolder mSelectedFolder;

    //boolean to determine if done has been clicked
    private boolean mDoneClicked = false;


    //onCreate - set up data and fragment for UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_removal_photos);

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set the orientation to portrait
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve the intent and verify available
        Intent intent = getIntent();

        if (intent != null) {

            //retrieve the extras and verify available
            Bundle extras = intent.getExtras();

            if (extras != null) {

                //retrieve the current shared folder
                mSelectedFolder = (SharedFolder) extras.getSerializable(SharedFoldersActivity.EXTRA_SHARED_FOLDER);

                /*
                * Suppressed unchecked warning due to intentional ArrayList cast
                * */
                @SuppressWarnings("unchecked")
                ArrayList<SelectRemovalPhoto> selectionPhotos = (ArrayList<SelectRemovalPhoto>) extras.getSerializable(SingleSharedFolderActivity.EXTRA_SELECTION_PHOTOS);
                mFolderPhotos.addAll(selectionPhotos);

                //add fragment to view with fragment manager
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.activity_select_removal_photos_fragment_container, SelectRemovalPhotosFragment.newInstance(mSelectedFolder.getTitle(), mFolderPhotos), SelectRemovalPhotosFragment.TAG)
                        .commit();
            }
        }
    }


    //cancelClicked - finish the activity
    @Override
    public void cancelClicked() {

        //finish the activity
        finish();
    }


    //doneClicked - disable the done button and add remove the selected photos from the folder
    @Override
    public void doneClicked(ArrayList<SelectRemovalPhoto> _selectedPhotos) {

        //if done has not been clicked
        if (!mDoneClicked) {

            //set that done has been clicked
            mDoneClicked = true;

            //check for internet
            if (InternetUtils.getIsNetworkAvailable(SelectRemovalPhotosActivity.this)) {

                //show the progress bar
                showProgressBar();

                //retrieve instance of the database and verify available
                FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                if (firebaseDatabase != null) {

                    //retrieve reference to the database and verify available
                    DatabaseReference databaseReference = firebaseDatabase.getReference();

                    if (databaseReference != null) {

                        //loop over the selected photos
                        for (SelectRemovalPhoto photo : _selectedPhotos) {

                            //create a reference to the photos of the shared folder
                            final DatabaseReference photoReference = databaseReference.child(SharedFoldersActivity.KEY_SHARED_FOLDERS).child(mSelectedFolder.getFolderIdentifier())
                                    .child(SharedFoldersActivity.KEY_PHOTOS);

                            //retrieve the path of the photo
                            String photoPath = photo.getStoragePath().substring(photo.getStoragePath().lastIndexOf("/")).trim();

                            //create query for the selected path of the photo
                            Query query = photoReference.orderByValue().equalTo(photoPath.substring(1, photoPath.length()));

                            //add single event listener to the query
                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    /*
                                    * Warning suppressed due to intentional and tested implementation.
                                    * */
                                    @SuppressWarnings("unchecked")
                                    HashMap<String, String> valueMap = (HashMap<String, String>) dataSnapshot.getValue();

                                    //verify the photo information was retrieved
                                    if (valueMap != null) {

                                        //loop over the received values
                                        for (String key : valueMap.keySet()) {

                                            //delete the value in the database
                                            photoReference.child(key).removeValue();
                                        }
                                    }
                                }

                                //required override
                                @Override
                                public void onCancelled(DatabaseError databaseError) {}
                            });

                            //delete the photo from storage
                            photo.getImageReference().delete();
                        }
                    }
                }

                //display toast to inform user the selected photos have been removed
                Toast.makeText(SelectRemovalPhotosActivity.this, R.string.photos_removed, Toast.LENGTH_SHORT).show();

                //hid the progress bar
                hideProgressBar();

                //set the result of the activity as ok and finish
                setResult(RESULT_OK);
                finish();

                //if no internet
            } else {

                //display toast to inform user internet is required to remove photos
                Toast.makeText(SelectRemovalPhotosActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
            }
        }
    }


    //getDoneClicked - returns if done has been clicked
    @Override
    public boolean getDoneClicked() {

        //return boolean for if done has been clicked
        return mDoneClicked;
    }


    //showProgressBar - display the progress bar
    private void showProgressBar() {

        //retrieve the fragment
        SelectRemovalPhotosFragment selectRemovalPhotosFragment = (SelectRemovalPhotosFragment) getFragmentManager().findFragmentByTag(SelectRemovalPhotosFragment.TAG);

        //verify fragment is available
        if (selectRemovalPhotosFragment != null) {

            //show the progress bar
            selectRemovalPhotosFragment.showProgressBar();
        }
    }


    //hideProgressBar - hide the progress bar
    private void hideProgressBar() {

        //retrieve the fragment
        SelectRemovalPhotosFragment selectRemovalPhotosFragment = (SelectRemovalPhotosFragment) getFragmentManager().findFragmentByTag(SelectRemovalPhotosFragment.TAG);

        //verify fragment is available
        if (selectRemovalPhotosFragment != null) {

            //hide the progress bar
            selectRemovalPhotosFragment.hideProgressBar();
        }
    }
}