/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.MainActivity;
import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.utilities.MediaStoreUtils;
import com.bakerheather.hb.pictureseverywhere.interfaces.EffectAppliedListener;
import com.bakerheather.hb.pictureseverywhere.utilities.EffectUtils;
import com.bakerheather.hb.pictureseverywhere.views.SquareFrameLayout;

import java.io.File;


//SinglePhotoFragment - fragment to display a single selected photo and allows for editing and sharing to social media
public class SinglePhotoFragment extends Fragment implements EffectAppliedListener {


    //fragment tag
    public static final String TAG = "FRAGMENT_SINGLE_PHOTO";

    //strings for effects
    private static final String GAMMA = "GAMMA";
    private static final String SEPIA = "SEPIA";
    private static final String GREYSCALE = "GREYSCALE";
    private static final String INVERTED = "INVERTED";
    private static final String BLACKFILTER = "BLACKFILTER";
    private static final String BRIGHTEN = "BRIGHTEN";
    private static final String COLORFILTER = "COLORFILTER";
    private static final String CONTRAST = "CONTRAST";
    private static final String DEPTH = "DEPTH";
    private static final String EMBOSS = "EMBOSS";
    private static final String ENGRAVE = "ENGRAVE";
    private static final String HORIZONTAL = "HORIZONTAL";
    private static final String VERTICAL = "VERTICAL";
    private static final String MEAN = "MEAN";
    private static final String REFLECTION = "REFLECTION";
    private static final String ROTATE = "ROTATE";
    private static final String SNOW = "SNOW";
    private static final String TINT = "TINT";

    //files for photo and photo with effects during changes
    private File mPhotoFile;
    public File mTempEffectFile;

    //references for image view and photo
    private ImageView mPhotoImageView;
    public Bitmap mPhoto;

    //seek bar references
    private SeekBar mSeekBarRed;
    private SeekBar mSeekBarGreen;
    private SeekBar mSeekBarBlue;
    private SeekBar mSeekBarValue;

    //progress bar ref
    private ProgressBar mEffectsProgressBar;

    //layout refs
    private SquareFrameLayout mColorPreviewLayout;
    private HorizontalScrollView mEffectSelectionView;
    private LinearLayout mEffectConfigureBar;
    private LinearLayout mEffectAppliedBar;
    private LinearLayout mEffectColorConfigureBar;
    private TextView mEffectColorConfigureTitle;
    private LinearLayout mEffectValueConfigureBar;
    private TextView mEffectValueConfigureTitle;

    //selected effect
    private String mSelectedEffect = "";


    //public constructor
    public SinglePhotoFragment() {}


    //newInstance - create and return new instance of fragment
    public static SinglePhotoFragment newInstance(File _photoFile) {

        //create new bundle and add file
        Bundle args = new Bundle();
        args.putSerializable(MainActivity.EXTRA_PHOTO_FILE, _photoFile);

        //create new fragment, set arguments
        SinglePhotoFragment fragment = new SinglePhotoFragment();
        fragment.setArguments(args);

        //return fragment
        return fragment;
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_single_photo, container, false);
    }


    //onActivityCreated - set up UI
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available
        if (getView() != null) {

            //get args and verify
            Bundle args = getArguments();

            if (args != null) {

                //get file extra and verify available
                mPhotoFile = (File) args.getSerializable(MainActivity.EXTRA_PHOTO_FILE);

                if (mPhotoFile != null) {

                    //get photo and image view, set bitmap
                    mPhoto = MediaStoreUtils.getEditingBitmapFromFile(mPhotoFile);

                    //rotate the photo based on EXIF data
                    mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mPhotoFile));
                    mPhotoImageView = getView().findViewById(R.id.fragment_single_photo_image_view);
                    mPhotoImageView.setImageBitmap(mPhoto);
                }

                //get seek bars, set listeners and progress
                mSeekBarRed = getView().findViewById(R.id.effect_seek_bar_red);
                mSeekBarGreen = getView().findViewById(R.id.effect_seek_bar_green);
                mSeekBarBlue = getView().findViewById(R.id.effect_seek_bar_blue);
                mSeekBarValue = getView().findViewById(R.id.effect_seek_bar_value);
                mSeekBarRed.setOnSeekBarChangeListener(mRedSeekBarChangeListener);
                mSeekBarGreen.setOnSeekBarChangeListener(mGreenSeekBarChangeListener);
                mSeekBarBlue.setOnSeekBarChangeListener(mBlueSeekBarChangeListener);
                mSeekBarRed.setProgress(50);
                mSeekBarGreen.setProgress(50);
                mSeekBarBlue.setProgress(50);

                //set preview color and display
                mColorPreviewLayout = getView().findViewById(R.id.effect_color_preview);
                displaySelectedColor();

                //get effect views and configure
                mEffectSelectionView = getView().findViewById(R.id.effects_select_effect_scroll);
                mEffectColorConfigureBar = getView().findViewById(R.id.effect_color_chooser);
                mEffectColorConfigureTitle = getView().findViewById(R.id.effect_color_chooser_title);
                mEffectValueConfigureBar = getView().findViewById(R.id.effect_value_chooser);
                mEffectValueConfigureTitle = getView().findViewById(R.id.effect_value_chooser_title);
                mEffectConfigureBar = getView().findViewById(R.id.effect_configure_bar);
                mEffectAppliedBar = getView().findViewById(R.id.effect_applied_bar);

                //get image views and set listeners
                ImageView imageView = getView().findViewById(R.id.effect_cancel_applied);
                imageView.setOnClickListener(mEffectAppliedBarCancelClickedListener);

                imageView = getView().findViewById(R.id.effect_save_applied);
                imageView.setOnClickListener(mEffectAppliedBarAcceptClickedListener);

                imageView = getView().findViewById(R.id.effect_apply_configured);
                imageView.setOnClickListener(mEffectConfigureBarApplyClickedListener);

                imageView = getView().findViewById(R.id.effect_cancel_configure);
                imageView.setOnClickListener(mEffectConfigureBarCancelClickedListener);

                //get progress bar
                mEffectsProgressBar = getView().findViewById(R.id.effects_progress_bar);

                //get effects views and set click listeners
                LinearLayout effect = getView().findViewById(R.id.effect_gamma);
                effect.setOnClickListener(mGammaClickListener);

                effect = getView().findViewById(R.id.effect_sepia);
                effect.setOnClickListener(mSepiaClickListener);

                effect = getView().findViewById(R.id.effect_greyscale);
                effect.setOnClickListener(mGreyscaleClickListener);

                effect = getView().findViewById(R.id.effect_inverted);
                effect.setOnClickListener(mInvertedClickListener);

                effect = getView().findViewById(R.id.effect_blackfilter);
                effect.setOnClickListener(mBlackfilterClickListener);

                effect = getView().findViewById(R.id.effect_brighten);
                effect.setOnClickListener(mBrightenClickListener);

                effect = getView().findViewById(R.id.effect_colorfilter);
                effect.setOnClickListener(mColorfilterClickListener);

                effect = getView().findViewById(R.id.effect_contrast);
                effect.setOnClickListener(mContrastClickListener);

                effect = getView().findViewById(R.id.effect_depth);
                effect.setOnClickListener(mDepthClickListener);

                effect = getView().findViewById(R.id.effect_emboss);
                effect.setOnClickListener(mEmbossClickListener);

                effect = getView().findViewById(R.id.effect_engrave);
                effect.setOnClickListener(mEngraveClickListener);

                effect = getView().findViewById(R.id.effect_fliphori);
                effect.setOnClickListener(mHorizontalClickListener);

                effect = getView().findViewById(R.id.effect_flipvert);
                effect.setOnClickListener(mVerticalClickListener);

                effect = getView().findViewById(R.id.effect_mean);
                effect.setOnClickListener(mMeanClickListener);

                effect = getView().findViewById(R.id.effect_reflection);
                effect.setOnClickListener(mReflectionClickListener);

                effect = getView().findViewById(R.id.effect_rotate);
                effect.setOnClickListener(mRotateClickListener);

                effect = getView().findViewById(R.id.effect_snow);
                effect.setOnClickListener(mSnowClickListener);

                effect = getView().findViewById(R.id.effect_tint);
                effect.setOnClickListener(mTintClickListener);
            }
        }
    }


    //hideEffectSelection - hide the effect selection
    private void hideEffectSelection () {

        //verify view available
        if (mEffectSelectionView != null) {

            //set visibility to gone
            mEffectSelectionView.setVisibility(View.GONE);
        }
    }


    //showEffectSelection - show the effect selection
    private void showEffectSelection() {

        //verify view available
        if (mEffectSelectionView != null) {

            //make visible
            mEffectSelectionView.setVisibility(View.VISIBLE);
        }
    }


    //effectsMode - show effects
    public void effectsMode() {

        //display the effects selection
        showEffectSelection();
    }


    //cancelEffects - refresh image view and cancel effects
    public void cancelEffects() {

        //hide effects views
        hideEffectSelection();
        hideEffectConfiguration();
        hideAppliedBar();

        //remove temp file
        mTempEffectFile = null;

        //get original photo
        mPhoto = MediaStoreUtils.getEditingBitmapFromFile(mPhotoFile);

        //rotate the photo based on EXIF data
        mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mPhotoFile));

        //and set in bitmap
        mPhotoImageView.setImageBitmap(mPhoto);
    }


    //saveEffects - save the applied effects as a new photo
    public void saveEffects() {

        //hide effect views
        hideEffectSelection();
        hideEffectConfiguration();
        hideAppliedBar();

        //verify file was created
        if (mTempEffectFile == null) {

            //get photo and
            mPhoto = MediaStoreUtils.getEditingBitmapFromFile(mPhotoFile);

            //rotate the photo based on EXIF data
            mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mPhotoFile));

            //set in image view
            mPhotoImageView.setImageBitmap(mPhoto);

            //if file saved
        } else {

            //get edited photo
            mPhoto = MediaStoreUtils.getEditingBitmapFromFile(mTempEffectFile);

            //rotate the photo based on EXIF data
            mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mTempEffectFile));

            //set in image view
            mPhotoImageView.setImageBitmap(mPhoto);
        }

        //erase temp file
        mTempEffectFile = null;
    }


    //hideAppliedBar - hide the effects apply bar
    private void hideAppliedBar() {

        //verify bar is available
        if (mEffectAppliedBar != null) {

            //make gone
            mEffectAppliedBar.setVisibility(View.GONE);
        }
    }


    //showAppliedBar - show the effects apply bar
    private void showAppliedBar() {

        //verify bar is available
        if (mEffectAppliedBar != null) {

            //make visible
            mEffectAppliedBar.setVisibility(View.VISIBLE);
        }
    }


    //showColorChooser - display color selection
    private void showColorChooser(String _effectTitle) {

        //verify color views are available
        if (mEffectColorConfigureBar != null && mEffectColorConfigureTitle != null && mEffectConfigureBar != null) {

            //set views for color selection
            mEffectColorConfigureBar.setVisibility(View.VISIBLE);
            mEffectColorConfigureTitle.setText(_effectTitle);
            mEffectConfigureBar.setVisibility(View.VISIBLE);
        }
    }


    //showValueChooser - display single value chooser
    private void showValueChooser(String _effectTitle) {

        //verify value views are available
        if (mEffectValueConfigureBar != null && mEffectColorConfigureTitle != null && mEffectConfigureBar != null) {

            //set views for value selection
            mEffectValueConfigureBar.setVisibility(View.VISIBLE);
            mEffectValueConfigureTitle.setText(_effectTitle);
            mEffectConfigureBar.setVisibility(View.VISIBLE);
        }
    }

    //hideEffectConfiguration - hide all effect configuration views
    private void hideEffectConfiguration () {

        //verify views are available
        if (mEffectColorConfigureBar != null && mEffectValueConfigureBar != null) {

            //hide effect config views
            mEffectColorConfigureBar.setVisibility(View.GONE);
            mEffectValueConfigureBar.setVisibility(View.GONE);
            mEffectConfigureBar.setVisibility(View.GONE);
        }
    }


    //mEffectConfigureBarApplyClickedListener - click listener for apply effects
    private final View.OnClickListener mEffectConfigureBarApplyClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //apply the current effect
            applyEffect();

        }
    };


    //applyEffect - add selected effect
    private void applyEffect() {

        //switch on
        switch (mSelectedEffect) {

            //Gamma
            case GAMMA:

                //start gamma effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyGamma(mPhoto, mSeekBarRed.getProgress(), mSeekBarGreen.getProgress(), mSeekBarBlue.getProgress(), SinglePhotoFragment.this);

                break;

                //sepia
            case SEPIA:

                //start sepia effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applySepia(mPhoto, mSeekBarRed.getProgress(), mSeekBarGreen.getProgress(), mSeekBarBlue.getProgress(), SinglePhotoFragment.this);

                break;

            //Greyscale
            case GREYSCALE:

                //start greyscale effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyGreyscale(mPhoto, SinglePhotoFragment.this);

                break;

            //Inverted
            case INVERTED:

                //start inverted effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyInverted(mPhoto, SinglePhotoFragment.this);

                break;

            //Black Filter
            case BLACKFILTER:

                //start black filter effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyBlackfilter(mPhoto, SinglePhotoFragment.this);

                break;

            //Brighten
            case BRIGHTEN:

                //start brighten effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyBrighten(mPhoto, mSeekBarValue.getProgress(),SinglePhotoFragment.this);

                break;

            //Color filter
            case COLORFILTER:

                //start color filter effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyColorFilter(mPhoto, mSeekBarRed.getProgress(), mSeekBarGreen.getProgress(), mSeekBarBlue.getProgress(), SinglePhotoFragment.this);

                break;

            //Contrast
            case CONTRAST:

                //start contrast effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyContrast(mPhoto, mSeekBarValue.getProgress(),SinglePhotoFragment.this);

                break;

            //Depth
            case DEPTH:

                //start depth effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyDepth(mPhoto, mSeekBarValue.getProgress(),SinglePhotoFragment.this);

                break;

            //Emboss
            case EMBOSS:

                //start emboss effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyEmboss(mPhoto, SinglePhotoFragment.this);

                break;

            //Engrave
            case ENGRAVE:

                //start engrave effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyEngrave(mPhoto, SinglePhotoFragment.this);

                break;

            //Horizontal
            case HORIZONTAL:

                //start horizontal effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyHorizontal(mPhoto, SinglePhotoFragment.this);

                break;

            //Vertical
            case VERTICAL:

                //start vertical effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyVertical(mPhoto, SinglePhotoFragment.this);

                break;

            //Mean
            case MEAN:

                //start mean effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyMean(mPhoto, SinglePhotoFragment.this);

                break;

            //Reflection
            case REFLECTION:

                //start reflection effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyReflection(mPhoto, SinglePhotoFragment.this);

                break;

            //Rotate
            case ROTATE:

                //start rotate effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyRotate(mPhoto, SinglePhotoFragment.this);

                break;

            //Snow
            case SNOW:

                //start snow effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applySnow(mPhoto, SinglePhotoFragment.this);

                break;

            //Snow
            case TINT:

                //start snow effect
                hideEffectConfiguration();
                animateProgressBar();
                EffectUtils.applyTint(mPhoto, mSeekBarValue.getProgress(),SinglePhotoFragment.this);

                break;
        }
    }


    //mEffectConfigureBarCancelClickedListener - cancel click listener
    private final View.OnClickListener mEffectConfigureBarCancelClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //hide config and show selection
            hideEffectConfiguration();
            showEffectSelection();
        }
    };


    //mEffectAppliedBarAcceptClickedListener - apply click listener
    private final View.OnClickListener mEffectAppliedBarAcceptClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //get temp file
            mTempEffectFile = MediaStoreUtils.storePhotoToTempFile(getActivity(), mPhoto);

            //hide apply and show selection
            hideAppliedBar();
            showEffectSelection();
        }
    };


    //mEffectAppliedBarCancelClickedListener - click listener for cancel click
    private final View.OnClickListener mEffectAppliedBarCancelClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //check if effect file is not available
            if (mTempEffectFile == null) {

                //get original photo
                mPhoto = MediaStoreUtils.getEditingBitmapFromFile(mPhotoFile);

                //rotate the photo based on EXIF data
                mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mPhotoFile));

                //set in view
                mPhotoImageView.setImageBitmap(mPhoto);

                //if file is available
            } else {

                //display effect file photo
                mPhoto = MediaStoreUtils.getPhotoBitmapFromFile(mTempEffectFile);

                //rotate the photo based on EXIF data
                mPhoto = MediaStoreUtils.rotatePhoto(mPhoto, MediaStoreUtils.getRotateDegrees(mTempEffectFile));

                //set in view
                mPhotoImageView.setImageBitmap(mPhoto);
            }

            //hide effect views
            hideAppliedBar();
            showEffectSelection();
        }
    };


    //mRedSeekBarChangeListener - listener for red seek bar change
    private final SeekBar.OnSeekBarChangeListener mRedSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            //display the currently selected color
            displaySelectedColor();
        }

        //required overrides
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };


    //mGreenSeekBarChangeListener - listener for green seek bar change
    private final SeekBar.OnSeekBarChangeListener mGreenSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            //display the currently selected color
            displaySelectedColor();
        }

        //required overrides
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };


    //mBlueSeekBarChangeListener - listener for blue seek bar change
    private final SeekBar.OnSeekBarChangeListener mBlueSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            //display the currently selected color
            displaySelectedColor();
        }

        //required overrides
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };


    //displaySelectedColor - display the currently selected color
    private void displaySelectedColor() {


        //verify seek bars and display views are available
        if (mSeekBarRed != null && mSeekBarGreen != null && mSeekBarBlue != null && mColorPreviewLayout != null) {

            //create color for seek bar values
            int mSelectedColor = Color.argb(255, mSeekBarRed.getProgress(), mSeekBarGreen.getProgress(), mSeekBarBlue.getProgress());

            //set the preview color
            mColorPreviewLayout.setBackgroundColor(mSelectedColor);
        }
    }


    //mGammaClickListener - click listener for gamma
    private final View.OnClickListener mGammaClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show color chooser
            hideEffectSelection();
            showColorChooser(getActivity().getString(R.string.gamma));
            mSelectedEffect = GAMMA;
        }
    };


    //mSepiaClickListener - click listener for sepia
    private final View.OnClickListener mSepiaClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show color chooser
            hideEffectSelection();
            showColorChooser(getActivity().getString(R.string.sepia));
            mSelectedEffect = SEPIA;
        }
    };


    //mGreyscaleClickListener - click listener for grey scale
    private final View.OnClickListener mGreyscaleClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = GREYSCALE;
            applyEffect();
        }
    };


    //mInvertedClickListener - click inverted for gamma
    private final View.OnClickListener mInvertedClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = INVERTED;
            applyEffect();
        }
    };


    //mBlackfilterClickListener - click black filter for gamma
    private final View.OnClickListener mBlackfilterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = BLACKFILTER;
            applyEffect();
        }
    };


    //mBrightenClickListener - click listener for brighten
    private final View.OnClickListener mBrightenClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show value chooser
            hideEffectSelection();
            showValueChooser(getActivity().getString(R.string.brighten));
            mSeekBarValue.setMax(50);
            mSelectedEffect = BRIGHTEN;
        }
    };


    //mColorfilterClickListener - click listener for color filter
    private final View.OnClickListener mColorfilterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show color chooser
            hideEffectSelection();
            hideEffectSelection();
            showColorChooser(getActivity().getString(R.string.color_filter));
            mSelectedEffect = COLORFILTER;
        }
    };


    //mContrastClickListener - click listener for contrast
    private final View.OnClickListener mContrastClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show value chooser
            hideEffectSelection();
            showValueChooser(getActivity().getString(R.string.contrast));
            mSeekBarValue.setMax(50);
            mSelectedEffect = CONTRAST;
        }
    };


    //mDepthClickListener - click listener for depth
    private final View.OnClickListener mDepthClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show value chooser
            hideEffectSelection();
            showValueChooser(getActivity().getString(R.string.depth));
            mSeekBarValue.setMax(50);
            mSelectedEffect = DEPTH;
        }
    };


    //mEmbossClickListener - click listener for emboss
    private final View.OnClickListener mEmbossClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = EMBOSS;
            applyEffect();
        }
    };


    //mEngraveClickListener - click listener for engrave
    private final View.OnClickListener mEngraveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = ENGRAVE;
            applyEffect();
        }
    };


    //mHorizontalClickListener - click listener for horizontal
    private final View.OnClickListener mHorizontalClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = HORIZONTAL;
            applyEffect();
        }
    };


    //mVerticalClickListener - click listener for vertical
    private final View.OnClickListener mVerticalClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = VERTICAL;
            applyEffect();
        }
    };


    //mMeanClickListener - click listener for mean
    private final View.OnClickListener mMeanClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = MEAN;
            applyEffect();
        }
    };


    //mReflectionClickListener - click listener for reflection
    private final View.OnClickListener mReflectionClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = REFLECTION;
            applyEffect();
        }
    };


    //mRotateClickListener - click listener for rotate
    private final View.OnClickListener mRotateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = ROTATE;
            applyEffect();
        }
    };


    //mSnowClickListener - click listener for snow
    private final View.OnClickListener mSnowClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and apply
            hideEffectSelection();
            mSelectedEffect = SNOW;
            applyEffect();
        }
    };


    //mTintClickListener - click listener for tint
    private final View.OnClickListener mTintClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //set selected effect and show value chooser
            hideEffectSelection();
            showValueChooser(getActivity().getString(R.string.tint));
            mSeekBarValue.setMax(500);
            mSelectedEffect = TINT;
        }
    };


    //effectApplied - display photo with effect
    @Override
    public void effectApplied(Bitmap _bitmap) {

        //verify photo and image view are available
        if (mPhotoImageView != null && _bitmap != null) {

            //show the applied bar and display the new image
            showAppliedBar();
            hideProgressBar();
            mPhoto = _bitmap;
            mPhotoImageView.setImageBitmap(_bitmap);

            //if effect failed
        } else {

            //display toast to inform user the effect failed for memory
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.effect_failed_free_memory), Toast.LENGTH_SHORT).show();

            //hide progress bar and dispaly effects
            hideProgressBar();
            showEffectSelection();
        }
    }


    //animateProgressBar - display and animate the progress bar
    private void animateProgressBar() {

        //verify progress bar is available
        if (mEffectsProgressBar != null) {

            //show and animate the bar
            mEffectsProgressBar.setVisibility(View.VISIBLE);
            mEffectsProgressBar.animate();
        }
    }


    //hideProgressBar - hide the progress bar
    private void hideProgressBar() {

        //verify progress bar is available
        if (mEffectsProgressBar != null) {

            //set progress bar visibility to gone
            mEffectsProgressBar.setVisibility(View.GONE);
        }
    }
}