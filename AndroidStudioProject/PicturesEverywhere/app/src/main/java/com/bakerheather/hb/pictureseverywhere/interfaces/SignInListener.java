/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import com.facebook.login.LoginResult;


//SignInListener - interface to interact between sign in fragment and activity
public interface SignInListener {

    //methods to pass information for sign up, sign in, forgot password, and facebook signed in to
    // activity
    void signUpClicked(String _email, String _password);
    void signInClicked(String _email, String _password);
    void forgotPasswordClicked(String _email);
    void facebookSignedIn(LoginResult _loginResult);
}
