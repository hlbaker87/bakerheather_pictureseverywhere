/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.fragments.ProfileFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.ProfileListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.ProfileUtils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthRecentLoginRequiredException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


//ProfileActivity - displays profile information for the currently signed in user and allows the
// change of email and password
public class ProfileActivity extends AppCompatActivity implements ProfileListener {


    //string key for extra
    public static final String EXTRA_EMAIL = "EXTRA_EMAIL";

    //reference to Firebase auth
    private FirebaseAuth mAuth;


    //onCreate - retrieve user email and setup fragment for UI
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set the orientation to portrait
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_profile);
        setSupportActionBar(myToolbar);

        //retrieve instance of firebase auth and verify available
        mAuth = FirebaseAuth.getInstance();

        if (mAuth != null) {

            //verify user is signed in and retrieve email address
            if (mAuth.getCurrentUser() != null) {

                String email = mAuth.getCurrentUser().getEmail();

                //add fragment to view with fragment manager
                getFragmentManager().beginTransaction().add(R.id.activity_profile_fragment_container, ProfileFragment.newInstance(email), ProfileFragment.TAG).commit();
            }
        }
    }


    //onCreateOptionsMenu - inflate menu from resources, load fragment, and hide options items accordingly
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_profile, menu);

        //return true
        return true;
    }


    //onOptionsItemSelected - verify options item selected and take action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the id of the selected menu item
        switch (item.getItemId()) {

            //sign out
            case R.id.action_sign_out:

                //sign out of current account
                signOut();
                break;
        }

        //return super implementation
        return super.onOptionsItemSelected(item);
    }


    //signOut - set result intent to sign out and finish the activity
    private void signOut() {

        //create new intent and add extra for sign out
        Intent resultIntent = new Intent();
        resultIntent.putExtra(MainActivity.EXTRA_SIGNED_OUT, true);

        //set the result intent and finish the activity
        setResult(MainActivity.RESULT_SIGNED_OUT, resultIntent);
        finish();
    }


    //changePassword - display alert allowing user to change their password
    @Override
    public void changePassword() {

        //verify Firebase auth is available
        if (mAuth != null) {

            //retrieve current user and verify available
            final FirebaseUser user = mAuth.getCurrentUser();

            if (user != null) {

                //create new alert builder and set the title
                AlertDialog.Builder changePasswordAlertBuilder = new AlertDialog.Builder(ProfileActivity.this);
                changePasswordAlertBuilder.setTitle(R.string.change_password_title);

                //inflate layout and set view
                /*
                * Warning suppressed due to intentional passing of null as root view
                * */
                @SuppressLint("InflateParams")
                View changePasswordAlertLayout = ProfileActivity.this.getLayoutInflater().inflate(R.layout.alert_change_password, null);
                changePasswordAlertBuilder.setView(changePasswordAlertLayout);

                //retrieve reference to new password EditText
                final EditText passwordInput = changePasswordAlertLayout.findViewById(R.id.change_password_edit_text);

                //cancel button
                changePasswordAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });

                //change password button
                changePasswordAlertBuilder.setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //check if password is not valid
                        if (!ProfileUtils.getPasswordValidity(ProfileActivity.this, passwordInput.getText().toString().trim()).equals("")) {

                            //display toast to inform user the password is not valid
                            Toast.makeText(ProfileActivity.this, ProfileUtils.getPasswordValidity(ProfileActivity.this, passwordInput.getText().toString().trim()), Toast.LENGTH_SHORT).show();

                            //if password is valid
                        } else {

                            //check if internet is available
                            if (InternetUtils.getIsNetworkAvailable(ProfileActivity.this)) {

                                //update the password for the user
                                user.updatePassword(passwordInput.getText().toString())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {

                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                //verify the task is successful
                                                if (task.isSuccessful()) {

                                                    //display toast to inform user update was successful
                                                    Toast.makeText(ProfileActivity.this, R.string.password_is_updated, Toast.LENGTH_SHORT).show();

                                                    //if update unsuccessful
                                                } else {

                                                    //try
                                                    try {

                                                        //check if there was an exception
                                                        if (task.getException() != null) {

                                                            //throw the exception
                                                            throw task.getException();
                                                        }

                                                        //catch recent login exceptions and print the stack trace
                                                    }  catch (FirebaseAuthRecentLoginRequiredException e) {
                                                        e.printStackTrace();

                                                        //display toast informing user recent sign in is necessary
                                                        Toast.makeText(ProfileActivity.this, R.string.please_sgn_in_again, Toast.LENGTH_SHORT).show();

                                                        //catch any exceptions and print the stack trace
                                                    } catch (Exception e) {
                                                        e.printStackTrace();

                                                        //display toast informing user the update was not successful
                                                        Toast.makeText(ProfileActivity.this, R.string.password_unsuccessful, Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            }
                                        });

                                //if no internet connection
                            } else {

                                //display toast to inform user network connection is required
                                Toast.makeText(ProfileActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });

                //display the alert
                changePasswordAlertBuilder.create().show();
            }
        }
    }


    //changeEmail - create alert and display for user to input a new email address, update firebase
    // if confirmed by user with valid email
    @Override
    public void changeEmail() {

        //verify firebase auth is available
        if (mAuth != null) {

            //retrieve current authorized user
            final FirebaseUser user = mAuth.getCurrentUser();

            //verify user is available
            if (user != null) {

                //create alert builder for email change
                AlertDialog.Builder changeEmailAlertBuilder = new AlertDialog.Builder(ProfileActivity.this);

                //set title of alert
                changeEmailAlertBuilder.setTitle(R.string.change_email);

                //set layout for email change and set as view for alert
                @SuppressLint("InflateParams")
                View changeEmailAlertLayout = ProfileActivity.this.getLayoutInflater().inflate(R.layout.alert_change_email,null);
                changeEmailAlertBuilder.setView(changeEmailAlertLayout);

                //retrieve the EditText from the view
                final EditText emailInput = changeEmailAlertLayout.findViewById(R.id.change_email_edit_text);

                //cancel button
                changeEmailAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });

                //change email button
                changeEmailAlertBuilder.setPositiveButton(R.string.change, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //check if entry was empty or blank space
                        if (emailInput.getText().toString().trim().equals("")) {

                            //display toast to inform user an email must be entered
                            Toast.makeText(ProfileActivity.this, R.string.blank_email_update, Toast.LENGTH_SHORT).show();

                            //check if entry was not valid email
                        } else if (!ProfileUtils.isValidEmail(emailInput.getText().toString().trim())) {

                            //display toast to inform user a valid email is required
                            Toast.makeText(ProfileActivity.this, R.string.valid_email_required_update, Toast.LENGTH_SHORT).show();

                            //check if entry is valid email address
                        } else if (ProfileUtils.isValidEmail(emailInput.getText().toString().trim())) {

                            //verify internet is available
                            if (InternetUtils.getIsNetworkAvailable(ProfileActivity.this)) {

                                //update the user's email in firebase
                                user.updateEmail(emailInput.getText().toString().trim())
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {

                                                //verify the update was successful
                                                if (task.isSuccessful()) {

                                                    //display toast to inform user their email address has been successfully updated
                                                    Toast.makeText(ProfileActivity.this, R.string.email_is_updated, Toast.LENGTH_SHORT).show();

                                                    //retrieve fragment
                                                    ProfileFragment profileFragment = (ProfileFragment) getFragmentManager().findFragmentByTag(ProfileFragment.TAG);

                                                    //verify fragment is available
                                                    if (profileFragment != null) {

                                                        //update fragment UI with new email
                                                        profileFragment.emailChanged(emailInput.getText().toString().trim());
                                                    }

                                                    //retrieve database instance and verify available
                                                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

                                                    if (firebaseDatabase != null) {

                                                        //retrieve reference to database
                                                        DatabaseReference databaseReference = firebaseDatabase.getReference();

                                                        //verify database, auth, and current user are available
                                                        if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                                                            //set the user's email in the database
                                                            databaseReference.child(SignInActivity.KEY_USERS).child(mAuth.getCurrentUser().getUid()).setValue(emailInput.getText().toString().trim());
                                                        }
                                                    }

                                                    //if update was not successful
                                                } else {

                                                    //display toast to inform user the update was not successful
                                                    Toast.makeText(ProfileActivity.this, R.string.email_update_unsuccessful, Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });

                                //if no internet connection
                            } else {

                                //display toast to inform user network connection is required
                                Toast.makeText(ProfileActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
                            }

                            //alternate scenario
                        } else {

                            //display toast to inform user the email they entered was invalid
                            Toast.makeText(ProfileActivity.this, R.string.invalid_email, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                //build and show the alert
                changeEmailAlertBuilder.create().show();
            }
        }
    }
}