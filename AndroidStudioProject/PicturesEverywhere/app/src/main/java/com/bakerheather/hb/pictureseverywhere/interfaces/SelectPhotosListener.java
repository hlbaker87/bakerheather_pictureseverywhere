/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import java.io.File;
import java.util.ArrayList;

//SelectPhotosListener - interface for selecting multiple photos
public interface SelectPhotosListener {

    //methods for canceling selection and completing selection to send the ArrayList of selected
    // Files as parameters
    void cancelClicked();
    void doneClicked(ArrayList<File> _selectedPhotos);

    /*
    * Suppressed due to clarity of code with inverted boolean
    * */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    boolean getDoneClicked();
}
