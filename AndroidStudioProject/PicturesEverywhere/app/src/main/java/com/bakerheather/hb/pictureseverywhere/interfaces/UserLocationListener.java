/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import android.location.Location;

//UserLocationListener - interface to retrieve the user's current location
public interface UserLocationListener {

    //method to return the users current location
    Location getCurrentLocation();
}
