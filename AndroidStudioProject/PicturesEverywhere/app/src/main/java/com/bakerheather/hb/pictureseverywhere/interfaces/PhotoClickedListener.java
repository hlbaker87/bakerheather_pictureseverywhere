/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import java.io.File;


//PhotoClickedListener - interface for when a photo is clicked to open the photo in the SinglePhotoActivity
public interface PhotoClickedListener {

    //method to open the photo from the file received as a parameter
    void photoClicked(File _photoFile);
}
