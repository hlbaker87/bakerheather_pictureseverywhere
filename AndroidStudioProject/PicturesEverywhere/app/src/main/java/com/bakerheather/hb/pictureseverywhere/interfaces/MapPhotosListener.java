/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import com.bakerheather.hb.pictureseverywhere.dataclasses.MapPhotoFile;


//MapPhotosListener - interface to add map markers for photos with location data
public interface MapPhotosListener {

    //method to add a single marker for the MapPhotoFile received as a parameter
    void addMapMarker(MapPhotoFile _mapPhotoFile);
}
