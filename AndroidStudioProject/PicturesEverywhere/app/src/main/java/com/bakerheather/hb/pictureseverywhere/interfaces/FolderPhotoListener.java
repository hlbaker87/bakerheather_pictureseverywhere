/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//FolderPhotoListener - interface for reloading shared folder photos
public interface FolderPhotoListener {

    //method to get the photos for a shared folder
    void getFolderPhotos();
}
