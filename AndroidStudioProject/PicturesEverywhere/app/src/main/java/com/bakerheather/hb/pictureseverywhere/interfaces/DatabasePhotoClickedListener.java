/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//DatabasePhotoClickedListener - interface for when a photo loaded from the database is clicked
public interface DatabasePhotoClickedListener {

    //method to display a photo clicked in a shared folder
    void photoClicked(int _clickedIndex);
}
