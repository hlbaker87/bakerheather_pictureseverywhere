/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.views;

import android.content.Context;
import android.util.AttributeSet;


//SquareGridImageView - square image view to display in grids
public class SquareGridImageView extends android.support.v7.widget.AppCompatImageView {

    //constructors
    public SquareGridImageView(Context context) {
        super(context);
    }

    public SquareGridImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareGridImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /*
    * Warning suppressed due to intentional passing of width to height measure to create square view
    * */
    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        //pass width as higth and width to super to create square image view
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}