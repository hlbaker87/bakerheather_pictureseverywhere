/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//DatePhotoSelectedListener - interface for displaying a photo clicked in the single date display
public interface DatePhotoSelectedListener {

    //method to display a photo clicked in the single date activity
    void datePhotoSelected(int _index);
}
