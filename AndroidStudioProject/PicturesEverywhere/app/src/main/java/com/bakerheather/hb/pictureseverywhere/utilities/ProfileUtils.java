/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.content.Context;

import com.bakerheather.hb.pictureseverywhere.R;

import java.util.regex.Pattern;


//ProfileUtils - utility class for dealing with profile information
public class ProfileUtils {


    //getPasswordValidity - return string of error if password is not valid
    public static String getPasswordValidity(Context _context, String _password) {

        //verify length of password is between 8 and 15 characters
        if (_password.trim().length() < 8 || _password.trim().length() > 15) {

            //return error string
            return _context.getString(R.string.error_password_length);
        }

        //return empty string
        return "";
    }


    //isValidEmail - verify if received string is a valid email address and return boolean
    public static boolean isValidEmail(String _email) {

        //create pattern
        Pattern pattern = Pattern.compile("^[\\w.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

        //return if the received string is an email address
        return pattern.matcher(_email).matches();
    }
}