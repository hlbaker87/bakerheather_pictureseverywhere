/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//ProfileListener - interface to communicate between activity and profile fragment
public interface ProfileListener {

    //methods to initiate changing user's password and email address
    void changePassword();
    void changeEmail();
}
