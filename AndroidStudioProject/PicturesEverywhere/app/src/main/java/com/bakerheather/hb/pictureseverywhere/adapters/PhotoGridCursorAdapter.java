/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;
import android.widget.ResourceCursorAdapter;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bumptech.glide.Glide;

import java.io.File;


//PhotoGridCursorAdapter - adapter for the grid gallery to display media store photos from cursor
public class PhotoGridCursorAdapter extends ResourceCursorAdapter {


    //public constructor passes context and cursor to super
    public PhotoGridCursorAdapter(Context _context, android.database.Cursor _c) {

        //pass context and cursor to super
        super(_context, R.layout.gallery_grid_image_item, _c, 0);
    }


    //bindView - binds the current view
    @Override
    public void bindView(View view, Context context, android.database.Cursor cursor) {

        //try
        try {

            //retrieve the image view
            ImageView imageView = view.findViewById(R.id.image_view_grid_item);

            //retrieve the path of the current photo
            String imagePath = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));

            //create a File for the current photo
            File imageFile = new File(imagePath);

            //load the photo into the image view
            Glide.with(context)
                    .load(imageFile).into(imageView);

            //catch any exceptions and print the stack trace
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
}