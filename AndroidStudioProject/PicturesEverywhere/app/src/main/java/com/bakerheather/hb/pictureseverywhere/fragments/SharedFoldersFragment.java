/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.SharedFoldersAdapter;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedFoldersListener;

import java.util.ArrayList;

//SharedFoldersFragment - fragment to display and allow selection from all shared folder current
// user is authorized to access
public class SharedFoldersFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_SHARED_FOLDERS";

    //references for listener and adapter
    private SharedFoldersListener mSharedFoldersListener;
    private SharedFoldersAdapter mSharedFoldersAdapter;

    //data model
    private final ArrayList<SharedFolder> mSharedFolders = new ArrayList<>();


    //public constructor
    public SharedFoldersFragment() {}


    //newInstance - create and return new instance of fragment
    public static SharedFoldersFragment newInstance() {

        //return new fragment
        return new SharedFoldersFragment();
    }


    //onAttach - verify implementation of interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify implementation
        if (context instanceof SharedFoldersListener) {

            //cast and store reference
            mSharedFoldersListener = (SharedFoldersListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_shared_folders, container, false);
    }


    //onActivityCreated - set up UI
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available
        if (getView() != null) {

            //get grid and set empty view
            GridView sharedFoldersGrid = getView().findViewById(R.id.shared_folders_grid_view);
            sharedFoldersGrid.setEmptyView(getView().findViewById(R.id.fragment_shared_folders_empty_list_state));

            //create and set adapter and click listener
            mSharedFoldersAdapter = new SharedFoldersAdapter(getActivity(), mSharedFolders);
            sharedFoldersGrid.setAdapter(mSharedFoldersAdapter);
            sharedFoldersGrid.setOnItemClickListener(mSharedFolderClickedListener);

            //verify listener is available
            if (mSharedFoldersListener != null) {

                //retrieve shared folders
                mSharedFoldersListener.getSharedFolders();
            }
        }
    }


    //mSharedFolderClickedListener - notify activity of click
    private final GridView.OnItemClickListener mSharedFolderClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify listener is available
            if (mSharedFoldersListener != null) {

                //notify of click
                mSharedFoldersListener.sharedFolderClicked(position);
            }
        }
    };


    //sharedFoldersChanged - update data model and UI
    public void sharedFoldersChanged(ArrayList<SharedFolder> _sharedFolders) {

        //reset the data model with new data
        mSharedFolders.clear();
        mSharedFolders.addAll(_sharedFolders);

        //verify adapter is available
        if (mSharedFoldersAdapter != null) {

            //notify the adapter of data set change
            mSharedFoldersAdapter.notifyDataSetChanged();
        }
    }
}