/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


import java.io.File;


//SharedPhotoDownloadListener - interface for opening a photo downloaded from a shared folder
public interface SharedPhotoDownloadListener {

    //method to display retrieved photo with temporary file created after download as parameter
    void displaySharedPhoto(File _tempPhotoFile);
}
