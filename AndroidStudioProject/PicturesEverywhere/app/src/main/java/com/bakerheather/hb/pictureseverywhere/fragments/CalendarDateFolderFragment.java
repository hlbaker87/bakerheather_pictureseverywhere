/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.MainActivity;
import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.CalendarDateAdapter;
import com.bakerheather.hb.pictureseverywhere.interfaces.DatePhotoSelectedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoClickedListener;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


//CalendarDateFolderFragment - fragment to display all photos captured on the selected day
public class CalendarDateFolderFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_CALENDAR_DATE_FOLDER";

    //references to interfaces
    private PhotoClickedListener mPhotoClickedListener;
    private DatePhotoSelectedListener mDatePhotoSelectedListener;

    //reference to the adapter
    private CalendarDateAdapter mCalendarDateAdapter;

    //ArrayList of Files for data model
    private final ArrayList<File> mDatePhotos = new ArrayList<>();


    //public constructor
    public CalendarDateFolderFragment() {}


    //newInstance - create and return new instance of fragment
    public static CalendarDateFolderFragment newInstance(ArrayList<File> _datePhotos, Date _date) {

        //create a new bundle
        Bundle args = new Bundle();

        //add extras
        args.putSerializable(MainActivity.EXTRA_CALENDAR_DATE_FOLDER, _datePhotos);
        args.putSerializable(MainActivity.EXTRA_DATE, _date);

        //create a new instance of the fragment and set the arguments
        CalendarDateFolderFragment fragment = new CalendarDateFolderFragment();
        fragment.setArguments(args);

        //return the fragment
        return fragment;
    }


    //onAttach - verify context implements interfaces, cast and store reference
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify context implements interface
        if (context instanceof PhotoClickedListener) {

            //cast context and store reference in member variable
            mPhotoClickedListener = (PhotoClickedListener) context;
        }

        //verify context implements interface
        if (context instanceof DatePhotoSelectedListener) {

            //cast context and store reference in member variable
            mDatePhotoSelectedListener = (DatePhotoSelectedListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar_date_folder, container, false);
    }


    //onActivityCreated - set up the grid view and title
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify the view is available
        if (getView() != null) {

            //retrieve the arguments and verify available
            Bundle args = getArguments();

            if (args != null) {

                //retrieve the data model and date from arguments
                /*
                * Suppressed unchecked warning due to intentional ArrayList cast
                * */
                @SuppressWarnings("unchecked")
                ArrayList<File> datePhotos = (ArrayList<File>) args.getSerializable(MainActivity.EXTRA_CALENDAR_DATE_FOLDER);
                Date date = (Date) args.getSerializable(MainActivity.EXTRA_DATE);

                //verify data model and date are available
                if (datePhotos != null && date != null) {

                    //create date formatter, retrieve text view and set the date as title
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM d, yyyy", Locale.US);
                    TextView dateTitle = getView().findViewById(R.id.calendar_date_folder_title_text_view);
                    dateTitle.setText(simpleDateFormat.format(date));

                    //add all photo files to the data model
                    mDatePhotos.addAll(datePhotos);

                    //retrieve the grid view and set the empty view
                    GridView gridView = getView().findViewById(R.id.calendar_date_grid_grid_view);
                    gridView.setEmptyView(getView().findViewById(R.id.fragment_calendar_date_folder_empty_list_state));

                    //create and store reference to adapter
                    mCalendarDateAdapter = new CalendarDateAdapter(getActivity(), mDatePhotos);

                    //set the adapter and click listener
                    gridView.setAdapter(mCalendarDateAdapter);
                    gridView.setOnItemClickListener(mGridItemClickListener);
                }
            }
        }
    }


    //mGridItemClickListener - on click listener for grid items to open clicked photo in single photo view
    private final GridView.OnItemClickListener mGridItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify listener is available
            if (mPhotoClickedListener != null) {

                //open clicked photo
                mDatePhotoSelectedListener.datePhotoSelected(position);
                mPhotoClickedListener.photoClicked(mDatePhotos.get(position));
            }
        }
    };


    //photoDeleted - notify adapter of data model change
    public void photoDeleted(int _index) {

        //remove the photo from the data model
        mDatePhotos.remove(_index);

        //verify the adapter is available
        if (mCalendarDateAdapter != null) {

            //notify the adapter of data set change
            mCalendarDateAdapter.notifyDataSetChanged();
        }
    }
}