/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */


package com.bakerheather.hb.pictureseverywhere.interfaces;


//SharedFoldersListener - interface for shared folders to open clicked folders and reload folders
public interface SharedFoldersListener {

    //methods for opening a clicked shared folder and reloading shared folders
    void sharedFolderClicked(int _clickedIndex);
    void getSharedFolders();
}
