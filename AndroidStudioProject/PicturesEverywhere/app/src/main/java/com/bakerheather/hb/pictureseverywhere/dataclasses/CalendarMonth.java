/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

//CalendarMonth - class to create objects for storing the information of a month with photos
public class CalendarMonth implements Serializable {

    //arrayList of dates
    private final ArrayList<Date> mMonthDates;

    //getter for ArrayList of dates
    public ArrayList<Date> getMonthDates() {
        return mMonthDates;
    }

    //public constructor setting received ArrayList as member variable
    public CalendarMonth(ArrayList<Date> mMonthDates) {
        this.mMonthDates = mMonthDates;
    }
}
