/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.dataclasses.CalendarMonth;
import com.bakerheather.hb.pictureseverywhere.interfaces.CalendarDateListener;
import com.bakerheather.hb.pictureseverywhere.views.DateView;
import com.bakerheather.hb.pictureseverywhere.views.SquareFrameLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

//CalendarAdapter - adapter for displaying the calendar gallery with dates highlighted for existing photos
public class CalendarAdapter extends BaseAdapter {

    //int for use in creating ids
    private static final int HEX_ID_CONSTANT = 0x0111;

    //ArrayList of CalendarMonths for data model
    private final ArrayList<CalendarMonth> mCalendarMonths;

    //context reference
    private final Context mContext;

    //reference to calendar date listener
    private final CalendarDateListener mCalendarDateListener;


    //public constructor referencing parameters in member variables
    public CalendarAdapter(Context _context, ArrayList<CalendarMonth> _calendarMonths, CalendarDateListener _calendarDateListener) {

        //store all parameters in member variables
        this.mContext = _context;
        this.mCalendarMonths = _calendarMonths;
        mCalendarDateListener = _calendarDateListener;
    }


    //getCount - return the count of data model objects
    @Override
    public int getCount() {

        //verify data model is available
        if (mCalendarMonths != null) {

            //return the size of the data model
            return mCalendarMonths.size();

            //if data model is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getItem - return the item for the position received as parameter
    @Override
    public CalendarMonth getItem(int position) {

        //verify data model is available and index is within bounds
        if (mCalendarMonths != null && position >= 0 && position < mCalendarMonths.size()) {

            //return the object at the position received
            return mCalendarMonths.get(position);

            //if data model or position is unavailable
        } else {

            //return a new calendar month
            return new CalendarMonth(new ArrayList<Date>());
        }
    }


    //getItemId - return a unique id for the item
    @Override
    public long getItemId(int position) {

        //verify data model is available and index is within bounds
        if (mCalendarMonths != null && position >= 0 && position < mCalendarMonths.size()) {

            //return constant added to the current position
            return HEX_ID_CONSTANT + position;

            //if position is unavailable
        } else {

            //return 0
            return 0;
        }
    }


    //getView - create and return the view for the current item
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //variable for the view holder
        CalendarMonthViewHolder calendarMonthViewHolder;

        //check if the convertView is null
        if(convertView == null) {

            //inflate the layout and store as convertView
            convertView = LayoutInflater.from(mContext).inflate(R.layout.calendar_grid_month_view, parent, false);

            //create a new view holder for the view and set as the tag of the convertView
            calendarMonthViewHolder = new CalendarMonthViewHolder(convertView);
            convertView.setTag(calendarMonthViewHolder);

            //if convertView is available
        } else {

            //store the tag of the convertView
            calendarMonthViewHolder = (CalendarMonthViewHolder) convertView.getTag();
        }

        //create date formatter and get an instance of the Calendar
        SimpleDateFormat titleFormat = new SimpleDateFormat("MMMM yyyy", Locale.US);
        Calendar calendar = Calendar.getInstance();

        //retrieve the CalendarMonth for the current position
        CalendarMonth monthAtPosition = getItem(position);

        //format the date as title and store in variable
        String monthTitle = titleFormat.format(monthAtPosition.getMonthDates().get(0));

        //set the text of the month title
        calendarMonthViewHolder.mMonthTitleTextView.setText(monthTitle.toUpperCase());

        //set set the calendar time for the current CalendarMonth and go to the first day of the month
        calendar.setTime(monthAtPosition.getMonthDates().get(0));
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        //retrieve the day of the week the month starts
        int startDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

        //retreive the number of days in the month
        int daysInMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

        //check if only 5 weeks are needed for the month
        if (daysInMonth + (startDayOfWeek - 1) <= 35) {

            //set the visibility of the sixth week to gone
            calendarMonthViewHolder.mWeekSix.setVisibility(View.GONE);

            //if more than 5 weeks are needed
        } else {

            //set the visibility of the sixth week to visible
            calendarMonthViewHolder.mWeekSix.setVisibility(View.VISIBLE);
        }

        //loop over the date views in the viewHolder
        for (DateView dateView: calendarMonthViewHolder.mDateViews) {

            //set the text to empty string and color to white
            dateView.getDateNumberTextView().setText("");
            dateView.getDateBackground().setBackgroundColor(Color.WHITE);
        }

        //loop over the dateViews
        for (int i = 1; i <= calendarMonthViewHolder.mDateViews.size(); i++) {

            //verify the current calendar square is after the start of the month and before the
            // end for the correct day of the week
            if (i >= startDayOfWeek && i <= daysInMonth + (startDayOfWeek - 1)) {

                //set the text of the calendar square to the day of the month it represents
                calendarMonthViewHolder.mDateViews.get(i - 1).getDateNumberTextView().setText(String.valueOf(i - startDayOfWeek + 1));
            }
        }

        //loop over the dates with photos
        for (final Date dateWithPhotos: monthAtPosition.getMonthDates()) {

            //set the time of the calendar to the date of loop
            calendar.setTime(dateWithPhotos);

            //retreive the day of the month
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            //set the background color of the calendar square with photos to highlighted
            calendarMonthViewHolder.mDateViews.get(day - 2 + startDayOfWeek).getDateBackground().setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));

            //set on click listener for the date with photos to use listener method for displaying
            // the photos for the clicked date
            calendarMonthViewHolder.mDateViews.get(day - 2 + startDayOfWeek).getDateBackground().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    //verify the listener is available
                     if (mCalendarDateListener != null) {

                         //pass the clicked date to listener method
                         mCalendarDateListener.calendarDateClicked(dateWithPhotos);
                     }
                }
            });
        }

        //return the configured view
        return convertView;
    }


    //CalendarMonthViewHolder - view holder for a calendar month
    private static class CalendarMonthViewHolder {

        //TextView for the month title
        final TextView mMonthTitleTextView;

        //ArrayList of DateViews
        final ArrayList<DateView> mDateViews;

        //layout for WeekSix
        final LinearLayout mWeekSix;

        //constructor with view as parameter
        CalendarMonthViewHolder(View _view) {

            //create new ArrayList to hold the date views
            mDateViews = new ArrayList<>();

            //store the title text view
            mMonthTitleTextView = _view.findViewById(R.id.calendar_month_text_view);

            //retrieve the date squares and text views and add as date views to the ArrayList
            SquareFrameLayout bg = _view.findViewById(R.id.calendar_date_background_1);
            TextView tv = _view.findViewById(R.id.calendar_date_text_view_1);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_2);
            tv = _view.findViewById(R.id.calendar_date_text_view_2);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_3);
            tv = _view.findViewById(R.id.calendar_date_text_view_3);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_4);
            tv = _view.findViewById(R.id.calendar_date_text_view_4);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_5);
            tv = _view.findViewById(R.id.calendar_date_text_view_5);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_6);
            tv = _view.findViewById(R.id.calendar_date_text_view_6);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_7);
            tv = _view.findViewById(R.id.calendar_date_text_view_7);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_8);
            tv = _view.findViewById(R.id.calendar_date_text_view_8);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_9);
            tv = _view.findViewById(R.id.calendar_date_text_view_9);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_10);
            tv = _view.findViewById(R.id.calendar_date_text_view_10);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_11);
            tv = _view.findViewById(R.id.calendar_date_text_view_11);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_12);
            tv = _view.findViewById(R.id.calendar_date_text_view_12);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_13);
            tv = _view.findViewById(R.id.calendar_date_text_view_13);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_14);
            tv = _view.findViewById(R.id.calendar_date_text_view_14);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_15);
            tv = _view.findViewById(R.id.calendar_date_text_view_15);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_16);
            tv = _view.findViewById(R.id.calendar_date_text_view_16);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_17);
            tv = _view.findViewById(R.id.calendar_date_text_view_17);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_18);
            tv = _view.findViewById(R.id.calendar_date_text_view_18);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_19);
            tv = _view.findViewById(R.id.calendar_date_text_view_19);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_20);
            tv = _view.findViewById(R.id.calendar_date_text_view_20);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_21);
            tv = _view.findViewById(R.id.calendar_date_text_view_21);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_22);
            tv = _view.findViewById(R.id.calendar_date_text_view_22);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_23);
            tv = _view.findViewById(R.id.calendar_date_text_view_23);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_24);
            tv = _view.findViewById(R.id.calendar_date_text_view_24);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_25);
            tv = _view.findViewById(R.id.calendar_date_text_view_25);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_26);
            tv = _view.findViewById(R.id.calendar_date_text_view_26);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_27);
            tv = _view.findViewById(R.id.calendar_date_text_view_27);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_28);
            tv = _view.findViewById(R.id.calendar_date_text_view_28);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_29);
            tv = _view.findViewById(R.id.calendar_date_text_view_29);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_30);
            tv = _view.findViewById(R.id.calendar_date_text_view_30);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_31);
            tv = _view.findViewById(R.id.calendar_date_text_view_31);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_32);
            tv = _view.findViewById(R.id.calendar_date_text_view_32);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_33);
            tv = _view.findViewById(R.id.calendar_date_text_view_33);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_34);
            tv = _view.findViewById(R.id.calendar_date_text_view_34);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_35);
            tv = _view.findViewById(R.id.calendar_date_text_view_35);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_36);
            tv = _view.findViewById(R.id.calendar_date_text_view_36);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_37);
            tv = _view.findViewById(R.id.calendar_date_text_view_37);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_38);
            tv = _view.findViewById(R.id.calendar_date_text_view_38);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_39);
            tv = _view.findViewById(R.id.calendar_date_text_view_39);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_40);
            tv = _view.findViewById(R.id.calendar_date_text_view_40);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_41);
            tv = _view.findViewById(R.id.calendar_date_text_view_41);
            mDateViews.add(new DateView(bg, tv));

            bg = _view.findViewById(R.id.calendar_date_background_42);
            tv = _view.findViewById(R.id.calendar_date_text_view_42);
            mDateViews.add(new DateView(bg, tv));

            //retrieve and store the layout for week six
            mWeekSix = _view.findViewById(R.id.calendar_week_6);
        }
    }
}