/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.vending.billing.IInAppBillingService;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SharedFolder;
import com.bakerheather.hb.pictureseverywhere.fragments.SharedFoldersFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.SharedFoldersListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;


//SharedFoldersActivity - display all shared folders the user owns and is given access to, also
// allows creation of new shared folders for subscribed users
public class SharedFoldersActivity extends AppCompatActivity implements SharedFoldersListener, PurchasesUpdatedListener, ServiceConnection {


    //strings for use as extra keys
    public static final String EXTRA_SHARED_FOLDER_TITLE = "EXTRA_SHARED_FOLDER_TITLE";
    public static final String EXTRA_SHARED_FOLDER = "EXTRA_SHARED_FOLDER";
    public static final String EXTRA_SHARED_FOLDER_OWNER = "EXTRA_SHARED_FOLDER_OWNER";

    //strings for use as database keys
    public static final String KEY_SHARED_FOLDERS = "SHARED_FOLDERS";
    public static final String KEY_OWNER = "OWNER";
    public static final String KEY_TITLE = "TITLE";
    public static final String KEY_CONTRIBUTORS = "CONTRIBUTORS";
    public static final String KEY_PHOTOS = "PHOTOS";

    //array list to store all shared folders
    private final ArrayList<SharedFolder> mSharedFolders = new ArrayList<>();

    //references to Firebase objects
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    //variables for billing with GooglePlay
    private IInAppBillingService mBillingService;
    private BillingClient mBillingClient;
    private boolean mBillingConnected = false;

    //boolean to determine if service is bound
    private boolean mIsBound = false;


    //boolean to determine if user is subscribed
    private boolean mUserIsSubscribed;


    //onCreate - set up necessary references and fragment
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_folders);

        //create billing client and start connection
        mBillingClient = BillingClient.newBuilder(SharedFoldersActivity.this).setListener(this).build();
        mBillingClient.startConnection(mBillingClientStateListener);

        //retrieve references to database and storage
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();

        //verify database is available and get reference
        if (mDatabase != null) {

            DatabaseReference databaseReference = mDatabase.getReference();

            //verify reference is available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference to shared folders and add listener
                DatabaseReference sharedFoldersReference = databaseReference.child(KEY_SHARED_FOLDERS);
                sharedFoldersReference.addValueEventListener(mSharedFoldersEventListener);
            }
        }

        //load the size of the device
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set orientation as portrait only
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_shared_folders);
        setSupportActionBar(myToolbar);

        //retrieve the intent and verify available
        Intent intent = getIntent();

        if (intent != null) {

            //retrieve the extras and verify available
            Bundle extras = intent.getExtras();

            if (extras != null) {

                //retrieve boolean to determine if the user is subscribed
                mUserIsSubscribed = extras.getBoolean(MainActivity.EXTRA_USER_IS_SUBSCRIBED, false);
            }
        }

        //add fragment to view with fragment manager
        getFragmentManager()
                .beginTransaction()
                .add(R.id.activity_shared_folders_fragment_container, SharedFoldersFragment.newInstance(), SharedFoldersFragment.TAG)
                .commit();
    }


    //onDestroy - remove event listener for shared folders and unbind services
    @Override
    protected void onDestroy() {
        super.onDestroy();

        //verify database is available and get reference
        if (mDatabase != null) {

            DatabaseReference databaseReference = mDatabase.getReference();

            //verify reference is available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference for shared folders
                DatabaseReference sharedFoldersReference = databaseReference.child(KEY_SHARED_FOLDERS);

                //remove the event listener
                sharedFoldersReference.removeEventListener(mSharedFoldersEventListener);
            }
        }

        //check if the service is bound
        if (mIsBound) {

            //set boolean to false and unbind
            mIsBound = false;
            unbindService(this);
        }

        //check if the billing service is available
        if (mBillingService != null) {

            //unbind the service and remove the service
            unbindService(mBillingServiceConnection);
            mBillingService = null;
        }
    }


    //onResume - verify subscription
    @Override
    protected void onResume() {
        super.onResume();

        //check if the billing client is available
        if (mBillingClient != null) {

            //if the client is not connected
            if (!mBillingConnected) {

                //start the connection
                mBillingClient.startConnection(mBillingClientStateListener);

                //if connected
            } else {

                //query the user's purchase history
                mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                    @Override
                    public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                        //set user subscribed to false
                        mUserIsSubscribed = false;

                        //verify the purchase list is available
                        if (purchasesList != null) {

                            //loop over the purchase list
                            for (Purchase purchase : purchasesList) {

                                //check if the SKU is for a month subscription
                                if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                    //set boolean for if the user is subscribed
                                    mUserIsSubscribed = true;
                                }
                            }
                        }
                    }
                });
            }
        }
    }



    //onActivityResult - verify received codes and take action
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //check if result code is to capture a new photo
        if (resultCode == MainActivity.RESULT_CAPTURE_NEW_PHOTO) {

            //set the result for the main activity to capture a new photo and finisht he activity
            setResult(MainActivity.RESULT_CAPTURE_NEW_PHOTO);
            finish();
        }

        //check if the request code is to delete a folder
        if (requestCode == SingleSharedFolderActivity.RC_FOLDER_DELETED) {

            //verify the result is ok
            if (resultCode == RESULT_OK) {

                //check if intent is available
                if (data != null) {

                    //retrieve the extras from the intent and verify available
                    Bundle extras = data.getExtras();

                    if (extras != null) {

                        //retrieve title of folder from extras
                        String title = extras.getString(EXTRA_SHARED_FOLDER_TITLE);

                        //create null folder variable
                        SharedFolder sharedFolder = null;

                        //loop over shared folders
                        for (SharedFolder folder: mSharedFolders) {

                            //verify title is available
                            if (title != null) {

                                //check if folder title received matches the folder title at index of loop
                                if (title.equals(folder.getTitle())) {

                                    //set folder at loop as shared folder
                                    sharedFolder = folder;

                                    break;
                                }
                            }
                        }

                        //check if matching folder was found during loop
                        if (sharedFolder != null) {

                            //remove the matching folder from the data model
                            mSharedFolders.remove(sharedFolder);

                            //reload shared folders
                            getSharedFolders();
                        }
                    }
                }
            }
        }

        //check if the request code was to get a subscription
        if (requestCode == MainActivity.RC_BUY_SUBSCRIPTION) {

            //check if the billing client is available
            if (mBillingClient != null) {

                //if the client is not connected
                if (!mBillingConnected) {

                    //start the connection
                    mBillingClient.startConnection(mBillingClientStateListener);

                    //if connected
                } else {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserIsSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserIsSubscribed = true;
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }
    }


    //onCreateOptionsMenu - inflate menu from resources
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_shared_folders_activity, menu);

        //return true
        return true;
    }


    //onOptionsItemSelected - verify selected options item and take action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the id of the selected menu item
        switch (item.getItemId()) {

            //shared folders
            case R.id.action_new_shared_folder:

                //check for internet
                if (InternetUtils.getIsNetworkAvailable(SharedFoldersActivity.this)) {

                    //display alert to create new shared folder
                    createSharedFolder();

                    //if no internet
                } else {

                    //display toast to inform user internet is required for creating folder
                    Toast.makeText(SharedFoldersActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
                }

                break;

            //capture photo
            case R.id.action_capture_photo:

                //set result to capture a new photo and finish the activity
                setResult(MainActivity.RESULT_CAPTURE_NEW_PHOTO);
                finish();

                break;

            //sign out
            case R.id.action_sign_out:

                //sign out
                signOut();

                break;
        }

        //return super implementation
        return super.onOptionsItemSelected(item);
    }


    //createSharedFolder - create and display alert to create shared folder
    private void createSharedFolder() {

        //check if user is currently subscribed
        if (mUserIsSubscribed) {

            //create dialog builder for creating a shared folder
            AlertDialog.Builder createSharedFolderAlertBuilder = new AlertDialog.Builder(SharedFoldersActivity.this);

            //set the title
            createSharedFolderAlertBuilder.setTitle(R.string.create_a_folder);

            //inflate layout and set view for input from user
            /*
            * Suppressed due to intentional passing of null as root view
            * */
            @SuppressLint("InflateParams")
            View createSharedFolderLayout = SharedFoldersActivity.this.getLayoutInflater().inflate(R.layout.alert_create_shared_folder, null);
            createSharedFolderAlertBuilder.setView(createSharedFolderLayout);

            //retrieve reference to edit text
            final EditText titleInput = createSharedFolderLayout.findViewById(R.id.create_shared_folder_edit_text);

            //set negative button as cancel with no action
            createSharedFolderAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });

            //set positive button as Add to add folder with title input by user
            createSharedFolderAlertBuilder.setPositiveButton(getString(R.string.add), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //retrieve input title and verify input is not empty
                    String inputTitle = titleInput.getText().toString();

                    if (!inputTitle.trim().equals("")) {

                        //verify auth and database are available
                        if (mAuth != null && mDatabase != null) {

                            //retrieve current user and verify available
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (user != null) {

                                //retrieve reference to database and verify available
                                DatabaseReference databaseReference = mDatabase.getReference();

                                if (databaseReference != null) {

                                    //create reference for new folder, set title, owner, and member
                                    DatabaseReference sharedFoldersRef = databaseReference.child(KEY_SHARED_FOLDERS).push();
                                    sharedFoldersRef.child(KEY_OWNER).setValue(user.getUid());
                                    sharedFoldersRef.child(KEY_TITLE).setValue(inputTitle);
                                    sharedFoldersRef.child(KEY_CONTRIBUTORS).push().setValue(user.getUid());
                                }
                            }
                        }

                        //reload shared folders and inform user with toast that the folder was created
                        getSharedFolders();
                        Toast.makeText(SharedFoldersActivity.this, inputTitle + " " + getString(R.string.suffix_folder_created), Toast.LENGTH_SHORT).show();

                        //if user did not input a title
                    } else {

                        //display toast to inform user a title is required
                        Toast.makeText(SharedFoldersActivity.this, R.string.title_required, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            //build and show the alert
            createSharedFolderAlertBuilder.create().show();

            //if not subscribed
        } else {

            //start the subscription process
            startSubscription();
        }
    }


    //signOut - sign out of firebase and facebook, notify the user, and finish the activity
    private void signOut() {

        //retrieve instances of FirebaseAuth and the Facebook LoginManager
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        LoginManager loginManager = LoginManager.getInstance();

        //verify auths are available
        if (firebaseAuth != null && loginManager != null) {

            //sign out of Firebase and log out of Facebook
            firebaseAuth.signOut();
            loginManager.logOut();

            //display toast to inform user they have signed out
            Toast.makeText(SharedFoldersActivity.this, R.string.signed_out, Toast.LENGTH_SHORT).show();

            //finish the activity
            finish();
        }
    }


    //sharedFolderClicked - create new intent and open the clicked folder in the SingleSharedFolder
    // activity
    @Override
    public void sharedFolderClicked(int _clickedIndex) {

        //check for internet
        if (InternetUtils.getIsNetworkAvailable(SharedFoldersActivity.this)) {

            //retrieve the clicked folder
            SharedFolder sharedFolder = mSharedFolders.get(_clickedIndex);

            //create a new intent and set folder as extra
            Intent singleSharedFolderIntent = new Intent(SharedFoldersActivity.this, SingleSharedFolderActivity.class);
            singleSharedFolderIntent.putExtra(EXTRA_SHARED_FOLDER, sharedFolder);
            singleSharedFolderIntent.putExtra(MainActivity.EXTRA_USER_IS_SUBSCRIBED, mUserIsSubscribed);

            //start the activity to view the folder
            startActivityForResult(singleSharedFolderIntent, SingleSharedFolderActivity.RC_FOLDER_DELETED);

            //if no internet
        } else {

            //display toast to inform user internet is required for folder
            Toast.makeText(SharedFoldersActivity.this, getString(R.string.no_network_connection), Toast.LENGTH_SHORT).show();
        }
    }


    //getSharedFolders - sort and update the fragment with current shared folders
    @Override
    public void getSharedFolders() {

        //sort the folders alphabetically
        Collections.sort(mSharedFolders, new Comparator<SharedFolder>() {
            @Override
            public int compare(SharedFolder o1, SharedFolder o2) {
                return o1.getTitle().toLowerCase().compareTo(o2.getTitle().toLowerCase());
            }
        });

        //retrieve fragment and verify available
        SharedFoldersFragment sharedFoldersFragment = (SharedFoldersFragment) getFragmentManager().findFragmentByTag(SharedFoldersFragment.TAG);

        if (sharedFoldersFragment != null) {

            //update the fragment with the new data model
            sharedFoldersFragment.sharedFoldersChanged(mSharedFolders);
        }
    }


    //startSubscription - create alert to inform user of subscription benefits and allow user to start the subscription process
    private void startSubscription() {

        //create alert
        AlertDialog.Builder subscriptionAlertBuilder = new AlertDialog.Builder(SharedFoldersActivity.this);

        //set title, message, buttons, and prevent cancellation
        subscriptionAlertBuilder.setTitle(R.string.title_start_subscription);
        subscriptionAlertBuilder.setCancelable(false);
        subscriptionAlertBuilder.setMessage(R.string.message_start_subscription);

        //set positive button to start the subscription process
        subscriptionAlertBuilder.setPositiveButton(R.string.subscribe, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //verify billing client is available
                if (mBillingClient != null) {

                    //try
                    try {

                        //create BillingFlowParams for a monthly subscription
                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                .setSku(MainActivity.SUBSCRIPTION_SKU)
                                .setType(BillingClient.SkuType.SUBS)
                                .build();

                        //set the request code and launch the subscription process
                        MainActivity.RC_BUY_SUBSCRIPTION = mBillingClient.launchBillingFlow(SharedFoldersActivity.this, billingFlowParams);

                        //catch any exceptions and print the stack trace
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        //set the negative button to cancel and complete no action
        subscriptionAlertBuilder.setNegativeButton(getString(R.string.maybe_later), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        //display the alert
        subscriptionAlertBuilder.create().show();
    }


    //mSharedFoldersEventListener - event listener for all shared folders
    private final ValueEventListener mSharedFoldersEventListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            //verify snapshot is available
            if (dataSnapshot != null) {

                //clear the current data model
                mSharedFolders.clear();

                //verify authed user is available
                if ( mAuth != null && mAuth.getCurrentUser() != null) {

                    //retrieve shared folders
                    if (dataSnapshot.getChildren() != null) {

                        //loop over shared folders
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                            //retrieve the folder key
                            String folderId = snapshot.getKey();

                            /*
                            * Warning suppressed due to intentional and tested implementation.
                            * */
                            @SuppressWarnings("unchecked")
                            HashMap<String, Object> folderMap = (HashMap<String, Object>) snapshot.getValue();

                            //verify the folder map is available
                            if (folderMap != null) {

                                //retrieve the owner and title of the folder
                                String owner = (String) folderMap.get(KEY_OWNER);
                                String title = (String) folderMap.get(KEY_TITLE);

                                /*
                                * Warning suppressed due to intentional and tested implementation.
                                * */
                                @SuppressWarnings("unchecked")
                                HashMap<String, String> contributorMap = (HashMap<String, String>) folderMap.get(KEY_CONTRIBUTORS);

                                //create boolean to determine if user is authorized for folder
                                boolean userIsContributor = false;

                                //verify contributor map is available
                                if (contributorMap != null) {

                                    //create ArrayList of contributors
                                    ArrayList<String> contributors = new ArrayList<>();

                                    //loop over contributors
                                    for (String key: contributorMap.keySet()) {

                                        //add to list of contributors
                                        contributors.add(contributorMap.get(key));

                                        //verify the user is a contributor
                                        if (contributorMap.get(key).equals(mAuth.getCurrentUser().getUid())) {

                                            //set boolean to true
                                            userIsContributor = true;
                                        }
                                    }

                                    //verify the user is a contributor
                                    if (userIsContributor) {

                                        /*
                                        * Warning suppressed due to intentional and tested implementation.
                                        * */
                                        @SuppressWarnings("unchecked")
                                        HashMap<String, String> photoPathMap = (HashMap<String, String>) folderMap.get(KEY_PHOTOS);

                                        //create new ArrayList for paths
                                        ArrayList<String> photoPaths = new ArrayList<>();

                                        //verify the path map is available
                                        if (photoPathMap != null) {

                                            //loop over the paths
                                            for (String photoKey : photoPathMap.keySet()) {

                                                //add the current path to the list of paths
                                                photoPaths.add(photoPathMap.get(photoKey));
                                            }
                                        }

                                        //create a new shared folder
                                        SharedFolder sharedFolder = new SharedFolder(folderId, owner, title, photoPaths);

                                        //loop over contributors and add to shared folder
                                        for (String contributor: contributors) {

                                            sharedFolder.addContributorID(contributor);
                                        }

                                        //add the new folder to the shared folders
                                        mSharedFolders.add(sharedFolder);
                                    }
                                }
                            }
                        }
                    }
                }

                //reload shared folders into fragment
                getSharedFolders();
            }
        }

        //required override
        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };


    //mBillingClientStateListener - state listener for billing client
    private final BillingClientStateListener mBillingClientStateListener = new BillingClientStateListener() {

        //onBillingSetupFinished - called when billing setup is finished - gets the purchase history
        @Override
        public void onBillingSetupFinished(@BillingClient.BillingResponse int billingResponseCode) {
            if (billingResponseCode == BillingClient.BillingResponse.OK) {

                //set billing boolean to true
                mBillingConnected = true;

                //verify the client is available
                if (mBillingClient != null) {

                    //query the user's purchase history
                    mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
                        @Override
                        public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {

                            //set user subscribed to false
                            mUserIsSubscribed = false;

                            //verify the purchase list is available
                            if (purchasesList != null) {

                                //loop over the purchase list
                                for (Purchase purchase : purchasesList) {

                                    //check if the SKU is for a month subscription
                                    if (purchase.getSku().equals(MainActivity.SUBSCRIPTION_SKU)) {

                                        //set boolean for if the user is subscribed
                                        mUserIsSubscribed = true;
                                    }
                                }
                            }
                        }
                    });
                }
            }
        }

        //onBillingServiceDisconnected - called when billing is disconnected
        @Override
        public void onBillingServiceDisconnected() {

            //set connected boolean to false
            mBillingConnected = false;
        }
    };


    @Override
    public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {

    }


    //onServiceConnected - called when the service is connected
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {

        //set bound boolean to true
        mIsBound = true;
    }


    //onServiceDisconnected - called when the service is disconnected
    @Override
    public void onServiceDisconnected(ComponentName componentName) {

        //set bound boolean to false
        mIsBound = false;
    }


    //mBillingServiceConnection - service connection for in app subscriptions
    private final ServiceConnection mBillingServiceConnection = new ServiceConnection() {

        //onServiceDisconnected - called when the service is disconnected
        @Override
        public void onServiceDisconnected(ComponentName name) {

            //set billing service to null
            mBillingService = null;
        }

        //onServiceConnected - called when the service is connected
        @Override
        public void onServiceConnected(ComponentName name,
                                       IBinder service) {

            //store the billing service
            mBillingService = IInAppBillingService.Stub.asInterface(service);
        }
    };
}