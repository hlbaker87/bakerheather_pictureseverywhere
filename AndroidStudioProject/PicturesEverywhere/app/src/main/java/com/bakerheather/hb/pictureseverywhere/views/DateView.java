/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.views;


import android.widget.TextView;


//DateView - class for storing views for a calendar date for display in the calendar gallery
public class DateView {

    //member variables for text view and background SquareFrameLayout
    private final SquareFrameLayout mDateBackground;
    private final TextView mDateNumberTextView;


    //getters for the text view and layout
    public SquareFrameLayout getDateBackground() {
        return mDateBackground;
    }

    public TextView getDateNumberTextView() {
        return mDateNumberTextView;
    }


    //public constructor setting received layout and text view as values of member variables
    public DateView(SquareFrameLayout _dateBackground, TextView _dateNumberTextView) {

        //store values in member variables
        this.mDateBackground = _dateBackground;
        this.mDateNumberTextView = _dateNumberTextView;
    }
}