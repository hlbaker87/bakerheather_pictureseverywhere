/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.fragments.SignInFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.SignInListener;
import com.bakerheather.hb.pictureseverywhere.utilities.InternetUtils;
import com.bakerheather.hb.pictureseverywhere.utilities.ProfileUtils;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


//SignInActivity - allows user to sign in and sign up using email/password or Facebook
public class SignInActivity extends AppCompatActivity implements SignInListener{


    //database key for users
    public static final String KEY_USERS = "USERS";

    //reference for the progress bar
    private ProgressBar mProgressBar;

    //reference for FirebaseAuth
    private FirebaseAuth mAuth;


    //onCreate - setup Auth and the fragment
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        //retrieve the device size
        String deviceSize = getString(R.string.layout_size);

        //check if the device is small
        if (deviceSize.equals("small")) {

            //set the orientation to portrait only
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve instance of firebase auth
        mAuth = FirebaseAuth.getInstance();

        //add fragment to view with fragment manager
        getFragmentManager()
                .beginTransaction()
                .add(R.id.activity_sign_in_fragment_container, SignInFragment.newInstance(), SignInFragment.TAG)
                .commit();

        //retrieve reference to the progress bar
        mProgressBar = findViewById(R.id.sign_in_progress_bar);
    }


    //signUpClicked - start sign up process and show alert for user to verify password
    @Override
    public void signUpClicked(final String _email, final String _password) {

        //verify email and password have been entered
        if (_email.trim().length() == 0 || _password.trim().length() == 0) {

            //display toast to inform user email and password are required
            Toast.makeText(SignInActivity.this, R.string.email_and_password_required_create_account, Toast.LENGTH_SHORT).show();

            //check if email is not valid
        } else if (!ProfileUtils.isValidEmail(_email.trim())) {

            //display toast to inform user that a valid email is required for an account
            Toast.makeText(SignInActivity.this, R.string.valid_email_create_account, Toast.LENGTH_SHORT).show();

            //if password is not valid
        } else if (!ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()).equals("")) {

            //retrieve fragment
            SignInFragment signInFragment = (SignInFragment) getFragmentManager().findFragmentByTag(SignInFragment.TAG);

            //verify fragment is available
            if (signInFragment != null) {

                //clear password input
                signInFragment.clearInput(false);
            }

            //display toast to inform user the password is not valid
            Toast.makeText(SignInActivity.this, ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()), Toast.LENGTH_SHORT).show();

            //if email and password are both valid
        } else if (ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()).equals("") && ProfileUtils.isValidEmail(_email.trim())) {

            //create new alert builder for sign up to confirm the password
            AlertDialog.Builder signUpAlertBuilder = new AlertDialog.Builder(SignInActivity.this);

            //set the alert title
            signUpAlertBuilder.setTitle(R.string.confirm_password);

            //inflate view for the alert and set the view
            /*
            * Suppressed due to intentional passing of null as root view
            * */
            @SuppressLint("InflateParams")
            View confirmPasswordLayout = SignInActivity.this.getLayoutInflater().inflate(R.layout.alert_confirm_password, null);
            signUpAlertBuilder.setView(confirmPasswordLayout);

            //retrieve the EditText for confirming the password
            final EditText confirmPasswordInput = confirmPasswordLayout.findViewById(R.id.confirm_password_edit_text);

            //set the negative button to cancel
            signUpAlertBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });

            //set the positive button to confirm and confirm passwords match
            signUpAlertBuilder.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    //retrieve the entered password
                    String confirmedPassword = confirmPasswordInput.getText().toString().trim();

                    //check if the passwords match
                    if (confirmedPassword.equals(_password.trim())) {

                        //check network availability
                        if (InternetUtils.getIsNetworkAvailable(SignInActivity.this)) {

                            //display progress bar and create a new user
                            showProgressBar();
                            mAuth.createUserWithEmailAndPassword(_email.trim(), confirmedPassword).addOnCompleteListener(SignInActivity.this, mFirebaseCompleteListener);

                            //if no internet connection
                        } else {

                            //display toast to inform user network connection is required
                            Toast.makeText(SignInActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
                        }

                        //if passwords do not match
                    } else {

                        //display toast to inform user the passwords do not match
                        Toast.makeText(SignInActivity.this, R.string.please_match_passwords, Toast.LENGTH_SHORT).show();
                    }
                }
            });

            //retrieve fragment
            SignInFragment signInFragment = (SignInFragment) getFragmentManager().findFragmentByTag(SignInFragment.TAG);

            //verify fragment is available
            if (signInFragment != null) {

                //clear password input
                signInFragment.clearInput(false);
            }

            //display the alert
            signUpAlertBuilder.create().show();
        }
    }


    //mFirebaseCompleteListener - listener for sign in completion with firebase
   private final OnCompleteListener<AuthResult> mFirebaseCompleteListener = new OnCompleteListener<AuthResult>() {
        @Override
        public void onComplete(@NonNull Task task) {

            //hide the progress bar
            hideProgressBar();

            //check if task was successful
            if (task.isSuccessful()) {

                //retrieve current user
                FirebaseUser user = mAuth.getCurrentUser();

                //verify user is available
                if (user != null) {

                    //verify the user email is available
                    if (user.getEmail() != null) {

                        //update UI for sign in with user email
                        signedIn(user.getEmail());
                    }
                }

                //if task was not successful
            } else {

                //try
                try {

                    //check if exception was received
                    if (task.getException() != null) {

                        //throw the exception
                        throw task.getException();
                    }

                    //catch invalid credentials exception, print stack trace and display toast for affordance
                } catch (FirebaseAuthInvalidCredentialsException e){
                    e.printStackTrace();

                    //display toast to inform user of failed sign in
                    Toast.makeText(SignInActivity.this, R.string.sign_in_failed_bad_password, Toast.LENGTH_SHORT).show();

                    //catch any other exceptions, print stack trace and display affordance
                } catch (Exception e){
                    e.printStackTrace();

                    //display toast to inform user of failed sign in
                    Toast.makeText(SignInActivity.this, e.getLocalizedMessage(),
                            Toast.LENGTH_SHORT).show();
                }
            }
        }
    };


    //signInClicked - verify the users information received from fragment input and sign in
    @Override
    public void signInClicked(String _email, String _password) {

        //verify email and password have been entered
        if (_email.trim().length() == 0 || _password.trim().length() == 0) {

            //display toast to inform user email and password are required
            Toast.makeText(SignInActivity.this, R.string.email_password_required_sign_in, Toast.LENGTH_SHORT).show();

            //check if email is not valid
        } else if (!ProfileUtils.isValidEmail(_email.trim())) {

            //display toast to inform user valid email is required
            Toast.makeText(SignInActivity.this, R.string.valid_email_required_sign_in, Toast.LENGTH_SHORT).show();

            //if password is not valid
        } else if (!ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()).equals("")) {

            //display toast to inform user of password requirements
            Toast.makeText(SignInActivity.this, ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()), Toast.LENGTH_SHORT).show();

            //if email and password are both valid
        } else if (ProfileUtils.getPasswordValidity(SignInActivity.this, _password.trim()).equals("") && ProfileUtils.isValidEmail(_email.trim())) {

            //check for internet
            if (InternetUtils.getIsNetworkAvailable(SignInActivity.this)) {

                //show the progress bar
                showProgressBar();

                //attempt to sign in with provided email and password
                mAuth.signInWithEmailAndPassword(_email.trim(), _password.trim())
                        .addOnCompleteListener(SignInActivity.this, mFirebaseCompleteListener);

                //if no internet connection
            } else {

                //display toast to inform user network connection is required
                Toast.makeText(SignInActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
            }
        }

        //retrieve fragment
        SignInFragment signInFragment = (SignInFragment) getFragmentManager().findFragmentByTag(SignInFragment.TAG);

        //verify fragment is available
        if (signInFragment != null) {

            //clear password input
            signInFragment.clearInput(false);
        }
    }


    //forgotPasswordClicked - verify email input and send a password reset email
    @Override
    public void forgotPasswordClicked(final String _email) {

        //retrieve fragment
        SignInFragment signInFragment = (SignInFragment) getFragmentManager().findFragmentByTag(SignInFragment.TAG);

        //verify fragment is available
        if (signInFragment != null) {

            //clear password input
            signInFragment.clearInput(false);
        }

        //verify input is valid email
        if (ProfileUtils.isValidEmail(_email.trim())) {

            //verify internet is available
            if (InternetUtils.getIsNetworkAvailable(SignInActivity.this)) {

                //set password reset email with firebase auth
                mAuth.sendPasswordResetEmail(_email.trim())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                //verify the task was successful
                                if (task.isSuccessful()) {

                                    //display toast to inform user emailw as sent
                                    Toast.makeText(SignInActivity.this, getString(R.string.recovery_email_sent), Toast.LENGTH_SHORT).show();

                                    //if unsuccessful
                                } else {

                                    //display toast to inform user the email is not registered
                                    Toast.makeText(SignInActivity.this, _email + " " + getString(R.string.email_not_registers_pass_reco), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                //if no internet connection
            } else {

                //display toast to inform user network connection is required
                Toast.makeText(SignInActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
            }

            //if invalid email
        } else {

            //display toast to inform user valid email is required
            Toast.makeText(SignInActivity.this, R.string.enter_valid_email_to_recover, Toast.LENGTH_SHORT).show();
        }
    }


    //facebookSignedIn - handle the access token received from facebook sign in to sign in to firebase
    @Override
    public void facebookSignedIn(LoginResult _loginResult) {

        //start sign in to firebase
        handleFacebookAccessToken(_loginResult.getAccessToken());
    }


    //handleFacebookAccessToken - start sign in with firebase using received access token
    private void handleFacebookAccessToken(AccessToken _token) {

        //verify internet is available
        if (InternetUtils.getIsNetworkAvailable(SignInActivity.this)) {

            //show the progress bar
            showProgressBar();

            //create auth credential with the received token
            AuthCredential credential = FacebookAuthProvider.getCredential(_token.getToken());

            //sign in to firebase with the credential
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            //hide the progress bar
                            hideProgressBar();

                            //check if sign in was successful
                            if (task.isSuccessful()) {

                                //retrieve the current user and verify available
                                FirebaseUser user = mAuth.getCurrentUser();

                                if (user != null) {

                                    //retrieve the email of the user
                                    if (user.getEmail() != null) {

                                        //display information for user and navigate back to the galleries
                                        signedIn(user.getEmail());
                                    }
                                }

                                //if sign in was not successful
                            } else {

                                //try
                                try {

                                    //check if exception was received
                                    if (task.getException() != null) {

                                        //throw the exception
                                        throw task.getException();
                                    }

                                    //catch invalid credentials exception, print stack trace and display toast for affordance
                                } catch (FirebaseAuthInvalidCredentialsException e){
                                    e.printStackTrace();

                                    //display toast to inform user of failed sign in
                                    Toast.makeText(SignInActivity.this, R.string.sign_in_failed_bad_password, Toast.LENGTH_SHORT).show();

                                    //catch any other exceptions, print stack trace and display affordance
                                } catch (Exception e){
                                    e.printStackTrace();

                                    //display toast to inform user of failed sign in
                                    Toast.makeText(SignInActivity.this, e.getLocalizedMessage(),
                                            Toast.LENGTH_LONG).show();
                                }

                                //sign out of facebook
                                LoginManager.getInstance().logOut();
                            }
                        }
                    });

            //if no internet connection
        } else {

            //display toast to inform user network connection is required
            Toast.makeText(SignInActivity.this, R.string.check_network, Toast.LENGTH_SHORT).show();
        }
    }


    //signedIn - display toast to inform user they are signed in, set result intent and finish the
    // activity to return to the main gallery
    private void signedIn(String _email) {

        //retrieve instance of database and verify available
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();

        if (firebaseDatabase != null) {

            //retrieve reference for database
            DatabaseReference databaseReference = firebaseDatabase.getReference();

            //verify reference, auth, and current user are available
            if (databaseReference != null && mAuth != null && mAuth.getCurrentUser() != null) {

                //create reference to the user in the database and set the user's current email
                databaseReference.child(KEY_USERS).child(mAuth.getCurrentUser().getUid()).setValue(_email);
            }
        }

        //hide the progress bar
        hideProgressBar();

        //retrieve fragment
        SignInFragment signInFragment = (SignInFragment) getFragmentManager().findFragmentByTag(SignInFragment.TAG);

        //verify fragment is available
        if (signInFragment != null) {

            //clear password and email input
            signInFragment.clearInput(true);
        }

        //display toast to inform user they are signed in
        Toast.makeText(SignInActivity.this, getString(R.string.signed_in_as_prefix) + " " + _email, Toast.LENGTH_SHORT).show();

        //create intent and set result of activity as signed in
        Intent resultIntent = new Intent();
        resultIntent.putExtra(MainActivity.EXTRA_SIGNED_IN, true);
        setResult(MainActivity.RESULT_SIGNED_IN, resultIntent);

        //finish the activity
        finish();
    }


    //showProgressBar - display and animate the progress bar
    private void showProgressBar() {

        //verify the progress bar is available
        if (mProgressBar != null) {

            //set to visible and animate
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.animate();
        }
    }


    //hideProgressBar - hide the progress bar
    private void hideProgressBar() {

        //verify the progress bar is available
        if (mProgressBar != null) {

            //set the progress bar visibility to gone
            mProgressBar.setVisibility(View.GONE);
        }
    }
}