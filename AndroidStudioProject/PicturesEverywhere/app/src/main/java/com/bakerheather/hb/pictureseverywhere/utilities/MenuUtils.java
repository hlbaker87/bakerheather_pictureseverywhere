/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.utilities;


import android.view.Menu;
import android.view.MenuItem;


//MenuUtils - utility class for hiding and showing options menu items
public class MenuUtils {


    //hideMenuItem - hides toolbar item
    public static void hideMenuItem(Menu _menu, int _itemId) {

        //verify menu is available
        if (_menu != null) {

            //retrieve the menu item and set visible to false
            MenuItem item = _menu.findItem(_itemId);
            item.setVisible(false);
        }
    }


    //showMenuItem - shows toolbar item
    public static void showMenuItem(Menu _menu, int _itemId){

        //verify menu is available
        if (_menu != null) {

            //retrieve the menu item and set visible to true
            MenuItem item = _menu.findItem(_itemId);
            item.setVisible(true);
        }
    }
}