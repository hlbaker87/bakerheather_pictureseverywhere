/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//DownloadBackupListener - interface for notifying activity when upload or download is complete
public interface DownloadBackupListener {

    //method to notify the activity a photo has been downloaded
    void downloadComplete();

    //methods to notify the activity when uploading or downloading has finished
    void fullDownloadComplete();
    void fullUploadComplete();
}
