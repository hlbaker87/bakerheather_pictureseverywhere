/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


import com.bakerheather.hb.pictureseverywhere.SharedFoldersActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.Serializable;
import java.util.ArrayList;


//SharedFolder - class for creating objects to store the information of a shared folder
public class SharedFolder implements Serializable {

    //member variables
    private final String mTitle;
    private final ArrayList<String> mPhotoPaths;
    private final ArrayList<String> mContributorUserIds = new ArrayList<>();
    private final String mOwner;
    private final String mFolderIdentifier;


    //getters
    public ArrayList<String> getPhotoPaths() {
        return mPhotoPaths;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getOwner() {
        return mOwner;
    }

    public String getFolderIdentifier() {
        return mFolderIdentifier;
    }

    public ArrayList<String> getContributorUserIds() {
        return mContributorUserIds;
    }


    //public constructor setting all received parameters as values of member variables
    public SharedFolder(String _folderID, String _owner, String _title, ArrayList<String> _photoPaths) {

        //set values of member variables
        this.mFolderIdentifier = _folderID;
        this.mOwner = _owner;
        this.mTitle = _title;
        this.mPhotoPaths = _photoPaths;
    }


    //addContributorID - add the received id to the contributor ids
    public void addContributorID(String _uid) {

        //verify the id is not already stored
        if (!mContributorUserIds.contains(_uid)) {

            //add the id to the ArrayList
            mContributorUserIds.add(_uid);
        }
    }


    //removeContributorID - remove the received id from the list of contributors
    public void removeContributorID(String _uid) {

        //remove the received id
        mContributorUserIds.remove(_uid);
    }


    //getCoverPhotoStorageReference - return a reference for
    public StorageReference getCoverPhotoStorageReference() {

        //retrieve references for firebase auth and storage
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        FirebaseStorage firebaseStorage = FirebaseStorage.getInstance();

        //verify firebase auth is available
        if (firebaseAuth != null) {

            //create and return a reference to the cover photo
            return firebaseStorage.getReference()
                    .child(SharedFoldersActivity.KEY_SHARED_FOLDERS)
                    .child(mFolderIdentifier)
                    .child(mPhotoPaths.get(0));
        }

        //return null
        return null;
    }
}