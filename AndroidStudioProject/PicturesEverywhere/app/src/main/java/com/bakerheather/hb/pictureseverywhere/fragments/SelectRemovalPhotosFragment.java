/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.SharedFoldersActivity;
import com.bakerheather.hb.pictureseverywhere.SingleSharedFolderActivity;
import com.bakerheather.hb.pictureseverywhere.adapters.SelectRemovalGridAdapter;
import com.bakerheather.hb.pictureseverywhere.dataclasses.SelectRemovalPhoto;
import com.bakerheather.hb.pictureseverywhere.interfaces.SelectRemovalPhotosListener;

import java.util.ArrayList;


//SelectRemovalPhotosFragment - fragment to display all photos in a shared folder and allow multi
// select for removal from the folder
public class SelectRemovalPhotosFragment extends Fragment {

    //fragment tag
    public static final String TAG = "FRAGMENT_SELECT_REMOVAL_PHOTOS";

    //reference to interface
    private SelectRemovalPhotosListener mSelectRemovalPhotosListener;

    //data model
    private final ArrayList<SelectRemovalPhoto> mSelectRemovalPhotos = new ArrayList<>();

    //reference for adapter
    private SelectRemovalGridAdapter mSelectRemovalGridAdapter;

    //reference for progress bar
    private ProgressBar mProgressBar;


    //public constructor
    public SelectRemovalPhotosFragment() {}


    //newInstance - create and return a new instance of the fragment
    public static SelectRemovalPhotosFragment newInstance(String _title, ArrayList<SelectRemovalPhoto> _selectRemovalPhotos) {

        //create new bundle
        Bundle args = new Bundle();

        //add arguments
        args.putString(SharedFoldersActivity.EXTRA_SHARED_FOLDER_TITLE, _title);
        args.putSerializable(SingleSharedFolderActivity.EXTRA_SELECTION_PHOTOS, _selectRemovalPhotos);

        //create new fragment and set arguments
        SelectRemovalPhotosFragment fragment = new SelectRemovalPhotosFragment();
        fragment.setArguments(args);

        //return the fragment
        return fragment;
    }


    //onAttach - verify implementation of required references
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify implementation
        if (context instanceof SelectRemovalPhotosListener) {

            //cast and store reference to interface
            mSelectRemovalPhotosListener = (SelectRemovalPhotosListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_removal_photos, container, false);
    }


    //onActivityCreated - set up UI
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available and get arguments
        if (getView() != null) {

            Bundle args = getArguments();

            if (args != null) {

                //get the progress bar
                mProgressBar = getView().findViewById(R.id.select_removal_photos_progress_bar);

                //get the title of the folder
                String title = args.getString(SharedFoldersActivity.EXTRA_SHARED_FOLDER_TITLE, "");

                /*
                * Suppressed unchecked warning due to intentional ArrayList cast
                * */
                @SuppressWarnings("unchecked")
                ArrayList<SelectRemovalPhoto> selectionPhotos = (ArrayList<SelectRemovalPhoto>) args.getSerializable(SingleSharedFolderActivity.EXTRA_SELECTION_PHOTOS);

                //get the grid and set empty view
                GridView photoGrid = getView().findViewById(R.id.select_removal_photos_grid_view);
                photoGrid.setEmptyView(getView().findViewById(R.id.fragment_select_removal_photos_empty_list_state));

                //get the title view and set text
                TextView titleTextView = getView().findViewById(R.id.select_removal_photos_title_text_view);
                titleTextView.setText(title);

                //set up the data model
                mSelectRemovalPhotos.addAll(selectionPhotos);

                //create and set adapter, set click listener
                mSelectRemovalGridAdapter = new SelectRemovalGridAdapter(getActivity(), mSelectRemovalPhotos);
                photoGrid.setAdapter(mSelectRemovalGridAdapter);
                photoGrid.setOnItemClickListener(mGridViewItemClickedListener);

                //get image views and set listeners
                ImageView done = getView().findViewById(R.id.select_photos_done);
                ImageView cancel = getView().findViewById(R.id.select_photos_cancel);
                done.setOnClickListener(mDoneClickedListener);
                cancel.setOnClickListener(mCancelClickedListener);
            }
        }
    }


    //mGridViewItemClickedListener - click listener for grid items
    private final GridView.OnItemClickListener mGridViewItemClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify listener is available and done has not been clicked
            if (mSelectRemovalPhotosListener != null) {

                if (!mSelectRemovalPhotosListener.getDoneClicked()) {

                    //get the clicked photo
                    SelectRemovalPhoto clickedPhoto = mSelectRemovalPhotos.get(position);

                    //change is selected boolean
                    clickedPhoto.setIsSelected(!clickedPhoto.getIsSelected());

                    //verify adapter is available
                    if (mSelectRemovalGridAdapter != null) {

                        //notify adapter of data change
                        mSelectRemovalGridAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    };


    //mCancelClickedListener - notify activity of cancel click
    private final View.OnClickListener mCancelClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //verify listener is available
            if (mSelectRemovalPhotosListener != null) {

                //notify listener of cancel click
                mSelectRemovalPhotosListener.cancelClicked();
            }
        }
    };


    //mDoneClickedListener - notify activity of done and remove photos
    private final View.OnClickListener mDoneClickedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            //create array list for selected photos
            ArrayList<SelectRemovalPhoto> selectedPhotos = new ArrayList<>();

            //loop over photos
            for (SelectRemovalPhoto photo: mSelectRemovalPhotos) {

                //check if selected and add to list
                if (photo.getIsSelected()) {

                    selectedPhotos.add(photo);
                }
            }

            //verify listener is available
            if (mSelectRemovalPhotosListener != null) {

                //notify of done click
                mSelectRemovalPhotosListener.doneClicked(selectedPhotos);
            }
        }
    };


    //showProgressBar - show and animate the progress bar
    public void showProgressBar() {

        //verify the bar is available
        if (mProgressBar != null) {

            //set visibility and animate
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.animate();
        }
    }


    //hideProgressBar - hide the progress bar
    public void hideProgressBar() {

        //verify the bar is available
        if (mProgressBar != null) {

            //set visibility
            mProgressBar.setVisibility(View.GONE);
        }
    }
}