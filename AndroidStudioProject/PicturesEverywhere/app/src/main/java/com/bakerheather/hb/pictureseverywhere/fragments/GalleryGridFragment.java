/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.adapters.PhotoGridCursorAdapter;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoCursorListener;

import java.io.File;

//GalleryGridFragment - displays all photos available on the device in GridView
public class GalleryGridFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_GALLERY_GRID";

    //reference to PhotoClickedListener
    private PhotoClickedListener mPhotoClickedListener;
    private PhotoCursorListener mPhotoCursorListener;

    //data model
    private android.database.Cursor mPhotoCursor;


    //public constructor
    public GalleryGridFragment() {}


    //newInstance - create and return new instance of the fragment
    public static GalleryGridFragment newInstance() {

        //return a new instance of the fragment
        return new GalleryGridFragment();
    }


    //onAttach - verify context implements interfaces, cast and store reference
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify context implements interface
        if (context instanceof PhotoClickedListener) {

            //cast context and store reference in member variable
            mPhotoClickedListener = (PhotoClickedListener) context;
        }

        //verify context implements interface
        if (context instanceof PhotoCursorListener) {

            //cast context and store reference in member variable
            mPhotoCursorListener = (PhotoCursorListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery_grid, container, false);
    }


    //onActivityCreated - set up the ui and listeners
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify view is available
        if (getView() != null) {

            //get the grid and set empty view
            GridView photoGrid = getView().findViewById(R.id.gallery_grid_grid_view);
            photoGrid.setEmptyView(getView().findViewById(R.id.fragment_gallery_grid_empty_list_state));

            //verify listener is available
            if (mPhotoCursorListener != null) {

                //get the cursor
                mPhotoCursor = mPhotoCursorListener.getPhotoCursor();

                //verify cursor is available
                if (mPhotoCursor != null) {

                    //create and set adapter and grid item click listener
                    PhotoGridCursorAdapter photoAdapter = new PhotoGridCursorAdapter(getActivity(), mPhotoCursor);
                    photoGrid.setAdapter(photoAdapter);
                    photoGrid.setOnItemClickListener(mGridViewItemClickedListener);
                }
            }
        }
    }


    //mGridViewItemClickedListener - click listener for grid items open photo in single photo view
    private final GridView.OnItemClickListener mGridViewItemClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //move cursor to the position of click
            mPhotoCursor.moveToPosition(position);

            //get the path and new file
            String imagePath = mPhotoCursor.getString(mPhotoCursor.getColumnIndex(MediaStore.Images.Media.DATA));
            File imageFile = new File(imagePath);

            //verify listener is available
            if (mPhotoClickedListener != null) {

                //open the clicked photo
                mPhotoClickedListener.photoClicked(imageFile);
            }
        }
    };
}