/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//SharedPhotosAddedListener - interface for notification that aphoto has been added to shared folder
public interface SharedPhotosAddedListener {

    //method to notify user when photo has been added to shared folder
    void photoAdded();
}
