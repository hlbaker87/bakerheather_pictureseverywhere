/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.bakerheather.hb.pictureseverywhere.fragments.CalendarDateFolderFragment;
import com.bakerheather.hb.pictureseverywhere.interfaces.DatePhotoSelectedListener;
import com.bakerheather.hb.pictureseverywhere.interfaces.PhotoClickedListener;
import com.bakerheather.hb.pictureseverywhere.utilities.MenuUtils;
import com.facebook.login.LoginManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;


//CalendarDateFolderActivity - displays the photos for a clicked date in the calendar gallery
public class CalendarDateFolderActivity extends AppCompatActivity implements PhotoClickedListener, DatePhotoSelectedListener {


    //static ints for result and request codes
    public static final int RESULT_PHOTO_DELETED = 0x010001;
    private static final int RC_DELETE = 0x010;

    //variable to store a selected index
    private int mSelectedIndex = 0;

    //reference to Toolbar Menu
    private Menu mToolbarMenu;

    //boolean to determine if the user is subscribed
    private boolean mUserIsSubscribed = false;


    //onCreate - set up menu, device orientation, and fragment
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_date_folder);

        //retrieve toolbar from resources and set as support action
        Toolbar myToolbar = findViewById(R.id.toolbar_calendar_date);
        setSupportActionBar(myToolbar);

        //retrieve the size of the device
        String deviceSize = getString(R.string.layout_size);

        //verify the device is small
        if (deviceSize.equals("small")) {

            //set the orientation to portrait only
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        //retrieve opening intent and verify available
        Intent intent = getIntent();

        if (intent != null) {

            //retrieve extras and verify available
            Bundle extras = intent.getExtras();

            if (extras != null) {

                /*
                * Suppressed unchecked warning due to intentional ArrayList cast
                * */
                @SuppressWarnings("unchecked")
                ArrayList<File> datePhotos = (ArrayList<File>) extras.getSerializable(MainActivity.EXTRA_CALENDAR_DATE_FOLDER);

                //retrieve date extra and boolean for subscribed user
                Date date = (Date) extras.getSerializable(MainActivity.EXTRA_DATE);
                mUserIsSubscribed = extras.getBoolean(MainActivity.EXTRA_USER_IS_SUBSCRIBED, false);

                //verify photos and date are available
                if (datePhotos != null && date != null) {

                    //add fragment to view with fragment manager
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.activity_calendar_date_fragment_container, CalendarDateFolderFragment.newInstance(datePhotos, date), CalendarDateFolderFragment.TAG)
                            .commit();
                }
            }
        }
    }


    //onActivityResult - verify the result code and pass the selected index to the fragment
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //verify the result code is for delete
        if (resultCode == RESULT_PHOTO_DELETED) {

            //retrieve the fragment and verify available
            CalendarDateFolderFragment calendarDateFolderFragment = (CalendarDateFolderFragment) getFragmentManager().findFragmentByTag(CalendarDateFolderFragment.TAG);

            if (calendarDateFolderFragment != null) {

                //pass the selected index to the fragment to notify of deletion
                calendarDateFolderFragment.photoDeleted(mSelectedIndex);
            }
        }
    }


    //onCreateOptionsMenu - inflate menu from resources, load fragment, and hide options items accordingly
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //store reference to toolbar menu
        mToolbarMenu = menu;

        //inflate menu from resources
        getMenuInflater().inflate(R.menu.menu_camera_sign_out, menu);

        //retrieve instance of FirebaseAuth
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        //verify Firebase auth has been referenced
        if (firebaseAuth != null) {

            //retrieve current Firebase user and verify the user is available
            FirebaseUser user = firebaseAuth.getCurrentUser();

            if (user == null) {

                //hide and show menu items for signed out user
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
                MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_in);

                //if user is unavailable
            } else {

                //hide and show menu items for signed in user
                MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_out);
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_in);
            }
        }

        //return true
        return true;
    }


    //onOptionsItemSelected - verify the selected options item by index and take action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //switch on the id of the selected menu item
        switch (item.getItemId()) {

            //capture photo
            case R.id.action_capture_photo:

                //start photo capture
                capturePhoto();

                break;

                //sign out
            case R.id.action_sign_out:

                //sign out the user
                signOut();

                break;

                //sign in
            case R.id.action_sign_in:

                //start sign in process
                signIn();
                break;
        }

        //return result of super implementation
        return super.onOptionsItemSelected(item);
    }


    //capturePhoto - set the result to start photo capture and finish the activity
    private void capturePhoto () {

        //set the result to capture a new photo
        setResult(MainActivity.RESULT_CAPTURE_NEW_PHOTO);

        //finish the activity
        finish();
    }


    //signOut - sign out the user
    private void signOut() {

        //retrieve instances of FirebaseAuth and the Facebook LoginManager
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        LoginManager loginManager = LoginManager.getInstance();

        //verify auths are available
        if (firebaseAuth != null && loginManager != null) {

            //sign out of Firebase and log out of Facebook
            firebaseAuth.signOut();
            loginManager.logOut();

            //verify toolbar menu is available
            if (mToolbarMenu != null) {

                //hide and show menu items for signed out user
                MenuUtils.hideMenuItem(mToolbarMenu, R.id.action_sign_out);
                MenuUtils.showMenuItem(mToolbarMenu, R.id.action_sign_in);
            }
        }

        //display toast to inform user they have signed out
        Toast.makeText(CalendarDateFolderActivity.this, R.string.signed_out, Toast.LENGTH_SHORT).show();
    }


    //signIn - set the result to start the sign in process and finish the activity
    private void signIn() {

        //set the result to start sign in and finish the activity
        setResult(MainActivity.RESULT_SIGN_IN);
        finish();
    }


    //photoClicked - open clicked photo in Single Photo Activity
    @Override
    public void photoClicked(File _photoFile) {

        //create a new intent and set the extras
        Intent singlePhotoIntent = new Intent(CalendarDateFolderActivity.this, SinglePhotoActivity.class);
        singlePhotoIntent.putExtra(MainActivity.EXTRA_PHOTO_FILE, _photoFile);
        singlePhotoIntent.putExtra(MainActivity.EXTRA_USER_IS_SUBSCRIBED, mUserIsSubscribed);

        //start the activity for result to update in the case of delete by user
        startActivityForResult(singlePhotoIntent, RC_DELETE);
    }


    //datePhotoSelected - set member variable for selected index as the index received as a parameter
    @Override
    public void datePhotoSelected(int _index) {

        //set index received as selected index
        mSelectedIndex = _index;
    }
}