/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.interfaces;


//PhotoCursorListener - interface to retrieve Cursor of all media store photos
public interface PhotoCursorListener {

    //method to return Cursor of all media store photos
    android.database.Cursor getPhotoCursor();
}
