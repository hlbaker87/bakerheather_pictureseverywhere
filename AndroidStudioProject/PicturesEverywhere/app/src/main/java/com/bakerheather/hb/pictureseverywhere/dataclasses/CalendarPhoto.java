/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.dataclasses;


import java.io.File;
import java.io.Serializable;
import java.util.Date;


//CalendarPhoto - class for creating objects to store the information of phtos with date data
public class CalendarPhoto implements Serializable {

    //member variables
    private final File mImageFile;
    private final Date mDate;

    //getters
    public File getImageFile() {
        return mImageFile;
    }

    public Date getDate() {
        return mDate;
    }

    //public constructor setting all parameters as member variables
    public CalendarPhoto(File _imageFile, Date _date) {

        //store file and date as member variables
        this.mImageFile = _imageFile;
        this.mDate = _date;
    }
}
