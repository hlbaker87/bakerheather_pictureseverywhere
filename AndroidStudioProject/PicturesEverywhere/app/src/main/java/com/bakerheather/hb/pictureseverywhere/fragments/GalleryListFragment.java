/*
* Heather Baker
* PicturesEverywhere
* Final Project 1712 - 1801
* Mobile Development Bachelor of Science
* Full Sail University
* */

package com.bakerheather.hb.pictureseverywhere.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bakerheather.hb.pictureseverywhere.R;
import com.bakerheather.hb.pictureseverywhere.interfaces.GalleryListListener;

import java.util.ArrayList;

//GalleryListFragment - fragment to display list of galleries for tablet layout
public class GalleryListFragment extends Fragment {


    //fragment tag
    public static final String TAG = "FRAGMENT_GALLERY_LIST";

    //reference to interface
    private GalleryListListener mGalleryListListener;


    //public constructor
    public GalleryListFragment() {}


    //newInstance - create and return a new instance of the fragment
    public static GalleryListFragment newInstance() {

        //return a enw instance of the fragment
        return new GalleryListFragment();
    }


    //onAttach - verify implementation of interfaces
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        //verify interface is implemented
        if (context instanceof GalleryListListener) {

            //cast and store reference
            mGalleryListListener = (GalleryListListener) context;
        }
    }


    //onCreateView - inflate and return layout from resources
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery_list, container, false);
    }


    //onActivityCreated - setup list view, adapter and click listeners for gallery list
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //verify the view is available
        if (getView() != null) {

            //create ArrayList of the gallery titles
            ArrayList<String> galleryTypes = new ArrayList<>();
            galleryTypes.add("GRID");
            galleryTypes.add("MAP");
            galleryTypes.add("CALENDAR");

            //retrieve the list view
            ListView listView = getView().findViewById(R.id.gallery_list);

            //create adapter for the ListView
            /*
            * Suppressed unchecked warning due to intentional parameter
            * */
            @SuppressWarnings("unchecked")
            ArrayAdapter listAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, galleryTypes);

            //set the adapter and click listener
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(mListItemClickedListener);
        }
    }


    //mListItemClickedListener - click listener for photos in ListView
    private final AdapterView.OnItemClickListener mListItemClickedListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            //verify listener is available
            if (mGalleryListListener != null) {

                //display the clicked gallery with interface
                mGalleryListListener.displayGallery(position);
            }
        }
    };
}